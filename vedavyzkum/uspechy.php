<?php
include '../header.php';


$succeses = [
    ['day' => '1','month' => 'leden', 'year' => '2017', 'heading' => 'Velký úspěch našeho předního vědce' , 'title' => 'Velký úspěch našeho předního vědce', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""],
    ['day' => '2','month' => 'březen', 'year' => '2018', 'heading' => 'Velký úspěch našeho předního vědce' , 'title' => 'Velký úspěch našeho předního vědce', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""],
    ['day' => '7','month' => 'říjen', 'year' => '2018', 'heading' => 'Velký úspěch našeho předního vědce' , 'title' => 'Velký úspěch našeho předního vědce', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""],
    ['day' => '9','month' => 'únor', 'year' => '2018', 'heading' => 'Velký úspěch našeho předního vědce' , 'title' => 'Velký úspěch našeho předního vědce', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""],
];
?>

    <div class="b-intro border-b holder holder--lg">
        <h1 class="b-intro__title" style="position:relative; top: 5px;">Úspěchy našich vědců</h1>

        <form action="?" class="f-subjects">
            <div class="f-subjects__filter">
                <p class="inp inp--multiple">
                    <span class="inp__fix minw440">
                        <label for="faculty" class="inp__label inp__label--inside">Výzkumná skupina</label>
                        <select name="faculty" id="faculty" class="select js-select">
                            <option selected disabled placeholder>Výzkumná skupina</option>
                            <option>Skupina A</option>
                            <option>Skupina B</option>
                        </select>
                    </span>
                </p>
            </div>
        </form>
    </div>

    <div class="c-subjects holder holder--lg">
        <div class="b-detail">
            <div class="grid grid--0">

                <div class="grid__cell size--t-12-12">
                    <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                        <div class="c-articles-lg ">
                            <div class="c-articles-lg__wrap border-b">
                                <ul class="c-articles-lg__list grid grid--0">
                                    <?php foreach ($succeses as $success){
                                        echo "<li class=\"c-articles-lg__item grid__cell size--t-6-12\">
                                        <article class=\"b-article-lg holder holder--lg\" role=\"article\">
                                            <a href=\"../vedavyzkum/uspechy_detail.php\" class=\"b-article-lg__link\">
                                                <div class=\"b-article-lg__img\">
                                                    <div class=\"b-article-lg__img-bg\" style=\"background-image: url({$success['image']});\"></div>
                                                </div>
                                                <div class=\"b-article-lg__head\">
                                                    <time class=\"b-article-lg__date date font-secondary\" datetime=\"2017-02-01\">
                                                        <span class=\"date__day\">{$success['day']}</span>
                                                        <span class=\"date__month\">{$success['month']}</span>
                                                        <span class=\"date__year\">{$success['year']}</span>
                                                    </time>
                                                    <h3 class=\"b-article-lg__title\">{$success['title']}</h3>
                                                </div>
                                                <div class=\"b-article-lg__content\">
                                                    <p>
                                                        {$success['short-text']}
                                                    </p>
                                                </div>
                                            </a>
                                        </article>
                                    </li>";} ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class="pagination " aria-label="Stránkování" role="navigation">
            <ul class="pagination__list">
                <li class="pagination__item">
                    <a href="#" class="pagination__link" aria-current="page">1</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">2</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">3</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="btn btn--secondary btn--wide btn--icon-only--m">
				<span class="btn__text">
					<span class="hide--m">Další</span>
					<span class="icon-svg icon-svg--angle-r btn__icon hide--t hide--d">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

				</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
<?php
include '../footer.php'
?>