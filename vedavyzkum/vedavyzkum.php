<?php
include '../header.php';

$successes = [
    ['title' => 'Plasty v Antarktidě stárnou jinak. Unikátní výzkum zjišťuje proč', 'text' => 'text akutality', 'day' => 4, 'month' => 'leden', 'year' => 2017],
    ['title' => 'Plasty v Antarktidě stárnou jinak. Unikátní výzkum zjišťuje proč', 'text' => 'text akutality 2', 'day' => 11, 'month' => 'březen', 'year' => 2017],
];

$scienceGroups = [
    ['name' => 'Výzkumná skupina 1', 'text' => 'O výzkumné skupině 1'],
    ['name' => 'Výzkumná skupina 2', 'text' => 'O výzkumné skupině 2'],
    ['name' => 'Výzkumná skupina 3', 'text' => 'O výzkumné skupině 3'],
];

$conferences = [
    ['title' => 'Excel FIT','day' => 4, 'month' => 'leden', 'year' => 2017],
    ['title' => 'OpenALT', 'day' => 11, 'month' => 'březen', 'year' => 2017],
    ['title' => 'Konference FIT', 'day' => 11, 'month' => 'březen', 'year' => 2017],
];
?>

<main id="main" class="main" role="main">
    <div class="b-hero-header ">
        <div class="b-hero-header__img " style="background-image: url('/img/illust/b-hero-header--10.jpg');"></div>
        <div class="b-hero-header__content holder holder--lg">
            <h1 class="title b-hero-header__title">
                <span class="title__item">Věda</span><span class="title__item">a</span><span class="title__item">výzkum</span><br><span class="title__item">na</span><span class="title__item">FIT</span>		</h1>

                <a href="#content" data-slide="#content" class="b-hero-header__next">
                <span class="icon-svg icon-svg--angle-d ">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
                    </svg>
                </span>

                <span class="vhide">Další</span>
            </a>
        </div>
    </div>

    <div id="content"></div>

    <div class="c-articles-lg border-b">
        <div class="holder holder--lg">
            <h2 class="c-articles-lg__title">Úspěchy našich vědců</h2>
        </div>
        <div class="c-articles-lg__wrap border-b">
            <ul class="c-articles-lg__list grid grid--0">
                <?php foreach($successes as $success){
                    echo "<li class=\"c-articles-lg__item grid__cell size--t-6-12\">
                    <article class=\"b-article-lg holder holder--lg\" role=\"article\">
                        <a href=\"#\" class=\"b-article-lg__link\">
                            <div class=\"b-article-lg__img\">
                                <div class=\"b-article-lg__img-bg\" style=\"background-image: url('/img/illust/b-article-lg--05.jpg');\"></div>
                            </div>
                            <div class=\"b-article-lg__head\">
                                <time class=\"b-article-lg__date date font-secondary\">
                                    <span class=\"date__day\">{$success['day']}</span>
                                    <span class=\"date__month\">{$success['month']}</span>
                                    <span class=\"date__year\">{$success['year']}</span>
                                </time>
                                <h3 class=\"b-article-lg__title\">{$success['title']}</h3>
                            </div>
                            <div class=\"b-article-lg__content\">
                                <p>
                                    {$success['text']}
                                </p>
                            </div>
                        </a>
                    </article>
                </li>";
                }?>

            </ul>
        </div>
        <div class="holder holder--lg pt40 pb40">
            <p class="text-center mb0">
                <a href="../vedavyzkum/uspechy.php" class="btn btn--sm btn--secondary btn--outline">
                    <span class="btn__text">Více úspěchů</span>
                </a>
            </p>
        </div>
    </div>

    <div class="sg-box__item">
        <div class="sg-box__item-code sg-box__item-code--bleed">
            <div class="c-programs holder holder--lg pt105--d pb85--d">
                <div class="row-main row-main--md">
                    <h2 class="c-programs__title">Výzkumné skupiny</h2>

                    <div class="c-programs__wrap">
                        <ul class="c-programs__list grid grid--t-40 grid--80">
                            <?php
                                foreach($scienceGroups as $scienceGroup){
                                    echo "<li class=\"c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
                                        <div class=\"b-program b-program--center\">
                                            <h3 class=\"b-program__title\">{$scienceGroup['name']}</h3>
                                            <div class=\"b-program__content\">
                                                <p>{$scienceGroup['text']}</p>
                                            </div>
                                            <p class=\"b-program__btn\">
                                                <a href=\"../vedavyzkum/vyzkumneskupiny_detail.php\" class=\"btn btn--outline btn--secondary btn--sm\">
                                                    <span class=\"btn__text\">Zjistit více</span>
                                                </a>
                                            </p>
                                        </div>
                                    </li>";
                                }
                            ?>

                        </ul>

                        <p class="text-center mt40--m mt40--t mt50 mb0">
                            <a href="../vedavyzkum/vyzkumneskupiny.php" class="btn">
                                <span class="btn__text">Všechny výzkumné skupiny</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="c-events border-t pt40 pb40">
        <div class="holder holder--lg">
            <h2 class="c-events__title">Nejbližší konference</h2>
        </div>

        <ul class="c-events__list grid grid--bd border-t mb40">
            <?php foreach ($conferences as $conference){
                echo "<li class=\"c-events__item grid__cell grid__cell--grow border-b holder size--t-6-12 size--4-12 c-events__item--sm holder--md\">
                <a href=\"#\" class=\"b-term
								b-term--sm
								b-term--img
						c-events__term\">
                    <div class=\"b-term__img\">
                        <img src=\"/img/illust/b-term--sm--01.jpg\" width=\"400\" height=\"280\" alt=\"\">
                    </div>
                    <div class=\"b-term__wrap\">
                        <time class=\"b-term__date date font-secondary\">
                            <span class=\"date__day\">{$conference['day']}</span>
                            <span class=\"date__month\">{$conference['month']}</span>
                            <span class=\"date__year\">{$conference['year']}</span>
                        </time>
                        <h3 class=\"b-term__title\">{$conference['title']}</h3>
                    </div>
                </a>
            </li>";
            }?>
        </ul>

        <div class="holder holder--lg">
            <p class="text-center mb0">
                <a href="#" class="btn btn--sm btn--secondary btn--outline">
                    <span class="btn__text">Více konferencí</span>
                </a>
            </p>
        </div>
    </div>

    <a href="#" class="b-cta b-cta--img">
        <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--05.jpg');">
            <img src="/img/illust/b-cta--xl--05.jpg" width="1440" height="540" alt="">
        </div>
        <div class="b-cta__content holder holder--lg">
            <h2 class="b-cta__title h1 mb10">Firemní spolupráce</h2>
            <p class="b-cta__subtitle">Řešíte problém, s kterým si nevíte rady?<br> Obraťte se na VUT. Pomůžeme vám!</p>
            <p class="mb0">
            <span class="btn btn--sm btn--outline btn--white">
                <span class="btn__text">Zjistit více o firemní spolupráci</span>
            </span>
            </p>
        </div>
    </a>
</main>

<?php
include '../footer.php'
?>
