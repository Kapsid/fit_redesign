<?php
include '../header.php';

$scienceGroups = [
    ["name" => "NES@FIT - Počítačové sítě a vestavěné systémy"],
    ["name" => "NES@FIT - Počítačové sítě a vestavěné systémy"],
    ["name" => "NES@FIT - Počítačové sítě a vestavěné systémy"],
    ["name" => "NES@FIT - Počítačové sítě a vestavěné systémy"],
    ["name" => "NES@FIT - Počítačové sítě a vestavěné systémy"],
    ["name" => "NES@FIT - Počítačové sítě a vestavěné systémy"],
]
?>

    <div class="b-intro border-b holder holder--lg">
        <h1 class="b-intro__title" style="position:relative; top: 20px;">Výzkumné skupiny</h1>
    </div>

    <div class="c-subjects holder holder--lg">
        <ul class="c-subjects__list">
            <?php foreach($scienceGroups as $scienceGroup){
                echo "<li class=\"c-subjects__item\">
            <div class=\"b-subject\">
                <h2 class=\"b-subject__title font-primary h4\">
                    <a href=\"../vedavyzkum/vyzkumneskupiny_detail.php#detail\" class=\"b-subject__link\">{$scienceGroup["name"]}</a>
                </h2>
            </div>
        </li>";
            }?>

        </ul>

        <nav class="pagination " aria-label="Stránkování" role="navigation">
            <ul class="pagination__list">
                <li class="pagination__item">
                    <a href="#" class="pagination__link" aria-current="page">1</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">2</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">3</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="btn btn--secondary btn--wide btn--icon-only--m">
				<span class="btn__text">
					<span class="hide--m">Další</span>
					<span class="icon-svg icon-svg--angle-r btn__icon hide--t hide--d">
                        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
                        </svg>
                    </span>
				</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
<?php
include '../footer.php'
?>