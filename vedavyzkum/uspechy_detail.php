<?php
include '../header.php';

$success = [
  'heading' => 'Plasty v Antarktidě stárnou jinak. Unikátní výzkum zjišťuje proč',
  'text' => 'Na systému podlah pro provozy s vysokou zátěží pracuje tým, jehož součástí je i Vít Černý z Fakulty stavební VUT. Unikátní řešení pro sklady či továrny má vyplnit díru na trhu a nabídnout komplexní podlahový systém s vysokou mechanickou a 
            chemickou odolností. Během příštího roku plánují výzkumníci testovat systém v reálných podmínkách.
            Pokud uspěje, mohl by se do roka dostat k běžným zákazníkům. Nosu nešťastná, divoké brání špičky vystřídalo šetrnost ty u věc ta druhy úřadu měla boží nízké odsouzeni s dochází nejznámějším
            signálem první. Péče i světu zprávy pohodlí tanec množství — vážil oteplování řadu avšak. Zesílilo prohlubování zastupujete
            čemu průmyslově něm tím k velkým ráj ohřívání přispěly internetová. Dvou chemical tu liška jednoho vyděšených úplně
            velice nepoužil z nejen z ty. 
            Podlaha sestává z několika vrstev, které ve finále vytvoří pevný a nepropustný systém. „Spodní vrstva se nazývá adhezní můstek
            a může to být jemná polymercementová malta nebo epoxidová pryskyřice. Na to přidáváme posyp. Kromě klasického křemičitého
            písku experimentujeme například s využitím škváry,“ uvedl Černý. Následuje hlavní část, což je takzvaný podlahový
            potěr tvořený betonem s jemnými kamínky a dalšími složkami. Ty zaručují rychlé tuhnutí a následně vysokou pevnost.
            „Nakonec sypeme a do podlahy zahlazujeme hmotu, která přispěje k extrémní odolnosti,“ dodal Černý.
    ',
    'parentProject' => 'Výzkum a vývoj pokročilých materiálů pro průmyslové podlahy',
    'createdPublications' => ['Vliv teploty výpalu na strukturu kameniva ze slinutého popílku'],
];

$connectedSuccesses = [
    ['heading' => 'Absolvent ÚSI vymyslel, jak zlevnit figuríny pro crash testy', 'text' => 'Na systému podlah pro provozy s vysokou zátěží pracuje tým, jehož součástí je i Vít Černý z Fakulty stavební VUT. Unikátní
                    řešení pro sklady či továrny má vyplnit díru na trhu a nabídnout komplexní podlahový systém s vysokou mechanickou
                    a chemickou odolností. Během příštího roku plánují výzkumníci testovat systém v reálných podmínkách. Pokud uspěje,
                    mohl by se do roka dostat k běžným zákazníkům.', 'date' => ['day' => '01', 'month' => 'leden' , 'year' => '2010'], 'image' => 'b-article-lg--05.jpg'],
    ['heading' => 'Chytrá zařízení Easycon změří klima i energie. Budoucnost vidí v aluminiu', 'text' => 'Na systému podlah pro provozy s vysokou zátěží pracuje tým, jehož součástí je i Vít Černý z Fakulty stavební VUT. Unikátní
                    řešení pro sklady či továrny má vyplnit díru na trhu a nabídnout komplexní podlahový systém s vysokou mechanickou
                    a chemickou odolností. Během příštího roku plánují výzkumníci testovat systém v reálných podmínkách. Pokud uspěje,
                    mohl by se do roka dostat k běžným zákazníkům.', 'date' => ['day' => '01', 'month' => 'leden' , 'year' => '2010'], 'image' => 'b-article-lg--05.jpg'],
];

$people = [
    ['surname' => 'Hemannová', 'name' => 'Renáta', 'degree' => 'Ing.', 'number' => '+420 541 146 120', 'mail' => 'steffan@feec.vutbr.cz'],
    ['surname' => 'Kolčavová', 'name' => 'Radana', 'degree' => 'Ing.', 'number' => '+420 541 146 120', 'mail' => 'steffan@feec.vutbr.cz'],
];

?>

<div class="b-detail" style="min-height: 100%">
    <div class="grid grid--bd grid--0">
        <div class="grid__cell size--t-7-12 size--8-12 holder holder--lg fz-lg">
            <div id="detail-01" class="b-detail__body pt50--d js-gallery" role="tabpanel">
                <h1><?php echo"{$success['heading']}"; ?></h1>
                <p>
                    <?php echo"{$success['text']}"; ?>
                </p>

                <article class="b-detail-teaser" role="article">
                    <a href="#" class="b-detail-teaser__link">
                        <p class="b-detail-teaser__tag font-secondary">
                            Řešeno v rámci projektu
                        </p>
                        <h2 class="b-detail-teaser__title font-primary h4">
                            <?php echo"{$success['parentProject']}"; ?>
                        </h2>
                    </a>
                </article>

                <article class="b-detail-teaser" role="article">
                    <a href="#" class="b-detail-teaser__link">
                        <p class="b-detail-teaser__tag font-secondary">
                            Vzniklé publikace
                        </p>
                        <?php foreach($success['createdPublications'] as $publication){
                          echo " <h2 class=\"b-detail-teaser__title font-primary h4\">
                            {$publication}
                        </h2>";}
                        ?>

                    </a>
                </article>
            </div>
        </div>


        <div class="grid__cell size--t-5-12 size--4-12">
            <nav class="m-case-studies" role="navigation">
                <ul class="m-case-studies__list" role="tablist">
                    <?php foreach($connectedSuccesses as $connectedSuccess){
                    echo "<li class=\"m-case-studies__item holder holder--md pt30--m pb30--m pt40 pb40\">
                        <a href=\"#detail-01\" class=\"b-article-lg__link m-case-studies__link\" role=\"tab\" aria-controls=\"detail-01\" aria-selected=\"true\">
                            <article class=\"b-article-lg\" role=\"article\">
                                <div class=\"b-article-lg__img\">
                                    <div class=\"b-article-lg__img-bg\" style=\"background-image: url('/img/illust/{$connectedSuccess['image']}');\"></div>
                                </div>
                                <div class=\"b-article-lg__head\">
                                    <time class=\"b-article-lg__date date font-secondary\" datetime=\"2017-01-04\">
                                        <span class=\"date__day\">{$connectedSuccess['date']['day']}</span>
                                        <span class=\"date__month\">{$connectedSuccess['date']['month']}</span>
                                        <span class=\"date__year\">{$connectedSuccess['date']['year']}</span>
                                    </time>
                                    <h2 class=\"b-article-lg__title h4\">{$connectedSuccess['heading']}</h2>
                                </div>
                            </article>
                        </a>
                        <div class=\"m-case-studies__bd\"></div>
                    </li>";
                    }?>
                </ul>

                <p class="m-case-studies__btn">
                    <a href="../vedavyzkum/uspechy.php" class="btn btn--secondary btn--sm">
                        <span class="btn__text">Všechny úspěchy</span>
                    </a>
                </p>
            </nav>
        </div>
    </div>
</div>
<div class="holder holder--lg">
    <div class="sg-box">
        <div class="sg-box__item">
            <div class="sg-box__item-code sg-box__item-code--bleed">
                <div class="c-employees">
                    <div class="holder holder--lg">
                        <h2 class="c-employees__title c-employees__title--underlined">Řešitelé</h2>

                        <div class="c-employees__wrap">
                            <ul class="c-employees__list grid grid--60 pt30">
                                <?php foreach($people as $person){
                                    echo "<li class=\"c-employees__item grid__cell grid__cell--grow size--t-6-12 size--3-12\">
                                    <div class=\"b-employee\">
                                        <div class=\"b-employee__wrap\">
                                            <a href=\"../ofakulte/profil.php\" class=\"b-employee__link\">
                                                <div class=\"b-employee__img\">
                                                    <img src=\"/img/illust/b-employee--01.jpg\" width=\"100\" height=\"100\" alt=\"\">
                                                </div>
                                                <h3 class=\"b-employee__name\">{$person['surname']} {$person['name']}, {$person['degree']}</h3>
                                            </a>
                                            <div class=\"b-employee__footer\">
                                                <p>
                                                    <a href=\"tel:{$person['number']}\">{$person['number']}</a><br>
                                                    <a href=\"mailto:{$person['mail']}\">{$person['mail']}</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>";
                                }?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<a href="#" class="b-cta b-cta--img">
    <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--05.jpg');">
        <img src="/img/illust/b-cta--xl--05.jpg" width="1440" height="540" alt="">
    </div>
    <div class="b-cta__content holder holder--lg">
        <h2 class="b-cta__title h1 mb10">Firemní spolupráce</h2>
        <p class="b-cta__subtitle">Řešíte problém, s kterým si nevíte rady?<br> Obraťte se na VUT. Pomůžeme vám! </p>
        <p class="mb0">
						<span class="btn btn--sm btn--outline btn--white">
							<span class="btn__text">Zjistit více o firemní spolupráci</span>
						</span>
        </p>
    </div>
</a>

<?php
include '../footer.php'
?>