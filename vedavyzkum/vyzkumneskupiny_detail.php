<?php
include '../header.php';

$scienceGroup = [
    'name' => 'Výzkumná skupina'];

$partners = [
    ['name' => 'Logo1', 'image' => 'logo--01.jpg'],
    ['name' => 'Logo2', 'image' => 'logo--02.jpg'],
    ['name' => 'Logo3', 'image' => 'logo--03.jpg'],
    ['name' => 'Logo4', 'image' => 'logo--04.jpg'],
    ['name' => 'Logo5', 'image' => 'logo--05.jpg'],
    ['name' => 'Logo6', 'image' => 'logo--06.jpg']
];

$actualities = [
    ['day' => '1','month' => 'leden', 'year' => '2017', 'heading' => 'Velký úspěch našeho předního vědce' , 'title' => 'Velký úspěch našeho předního vědce', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""],
    ['day' => '2','month' => 'březen', 'year' => '2018', 'heading' => 'Velký úspěch našeho předního vědce' , 'title' => 'Velký úspěch našeho předního vědce', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""]
];

$stats = [
    ['desc' => 'Namodelovaných počítačových sítí', 'amount' => '1 700'],
    ['desc' => 'projektů od roku 2005', 'amount' => '100'],
    ['desc' => 'řádků kódu', 'amount' => '3 mil.'],
    ['desc' => 'patenty', 'amount' => '4']
];

$topics = [
    'Komunikace vestavěných zažízení, bezdrátové a mobilní sítě', 'Komunikace vestavěných zažízení, bezdrátové a mobilní sítě', 'Komunikace vestavěných zažízení, bezdrátové a mobilní sítě', 'Komunikace vestavěných zažízení, bezdrátové a mobilní sítě', 'Komunikace vestavěných zažízení, bezdrátové a mobilní sítě', 'Komunikace vestavěných zažízení, bezdrátové a mobilní sítě'
];

$equipments = [
    ['type' => 'Přístrojové vybavení', 'name' => 'Zkušebna letecké techniky',
        'text' => ' Zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. ',
        'image' => 'b-img--01.jpg'],
    ['type' => 'Přístrojové vybavení', 'name' => 'Zkušebna letecké techniky 2',
        'text' => ' Zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. ',
        'image' => 'b-img--02.jpg'],
    ['type' => 'Přístrojové vybavení', 'name' => 'Zkušebna letecké techniky',
        'text' => ' Zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. ',
        'image' => 'b-img--01.jpg'],
    ['type' => 'Přístrojové vybavení', 'name' => 'Zkušebna letecké techniky 2',
        'text' => ' Zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. zkušebna je oprávněna Úřadem pro civilní letectví č. L-3-040/9 k provádění průkazných a ověřovacích zkoušek konstrukce draku letounů podle předpisů CS a FAR. ',
        'image' => 'b-img--02.jpg'],
];

$team = [
  ['name' => 'Marek Novák, Ing', 'function' => 'vedoucí skupiny'],
    ['name' => 'Jan Vít, Ing', 'function' => 'tajemník'],
    ['name' => 'Petr Jícha, Ing', 'function' => 'člen'],
    ['name' => 'Miroslav Novák, Ing', 'function' => 'člen'],
];

$projects = [
    ["name" => "Projekt 1", "From" => "01. 01. 2015", "To" => "14. 02. 2018", "financed" => "Evropská Unie"],
    ["name" => "Projekt 2", "From" => "01. 04. 2016", "To" => "14. 02. 2019", "financed" => "Město Brno"],
    ["name" => "Projekt 3", "From" => "14. 03. 2018", "To" => "14. 04. 2019", "financed" => "Technický institut"],
    ["name" => "Projekt 1", "From" => "01. 01. 2015", "To" => "14. 02. 2018", "financed" => "Evropská Unie"],
    ["name" => "Projekt 2", "From" => "01. 04. 2016", "To" => "14. 02. 2019", "financed" => "Město Brno"],
    ["name" => "Projekt 3", "From" => "14. 03. 2018", "To" => "14. 04. 2019", "financed" => "Technický institut"],
    ["name" => "Projekt 1", "From" => "01. 01. 2015", "To" => "14. 02. 2018", "financed" => "Evropská Unie"],
    ["name" => "Projekt 2", "From" => "01. 04. 2016", "To" => "14. 02. 2019", "financed" => "Město Brno"],
    ["name" => "Projekt 3", "From" => "14. 03. 2018", "To" => "14. 04. 2019", "financed" => "Technický institut"],
];


$publications = [
    ["name" => "Testovací publikace", "author" => "Ing. Jiří Novák", "link" => "../ofakulte/profil.php", "coauthors" => "Ing. Jana Nová , Mgr. Petr Pavel"],
    ["name" => "Školní publikace", "author" => "Ing. Jiří Novág", "link" => "../ofakulte/profil.php", "coauthors" => "Ing. Jana Nová"],
    ["name" => "Vývoj techniky ve 21. století", "author" => "Ing. Zdeněk Bukal", "link" => "../ofakulte/profil.php", "coauthors" => "Ing. Jana Nová, Mgr. Petr Pavel, Mgr. František Kocáb"],

];

$products = [
    ["name" => "Produkt 1", "type" => "software", "year" => 2018, "authors" => "Ing. Jana Nová"],
    ["name" => "Produkt 2", "type" => "software", "year" => 2018, "authors" => "Ing. Jana Nová, Mgr. Adam Novák"],
    ["name" => "Produkt 3", "type" => "software", "year" => 2017, "authors" => "Ing. Jana Nová"],
    ["name" => "Produkt 4", "type" => "hardware", "year" => 2016, "authors" => "Ing. Jana Nová"],
    ["name" => "Produkt 5", "type" => "hardware", "year" => 2016, "authors" => "Ing. Jana Nová, Ing. František Vosáhlo"],
    ["name" => "Produkt 6", "type" => "software", "year" => 2016, "authors" => "Ing. Jana Nová"],
];

$patents = [
    ["name" => "Patent 1", "type" => "užitný vzor", "registration" => 2018, "acceptance" => 2020, "ending" => 2022, "authors" => "Ing. Jana Nová", "owner" => "VUT v Brně"],
    ["name" => "Patent 2", "type" => "užitný vzor", "registration" => 2018, "acceptance" => 2020, "ending" => 2022, "authors" => "Ing. Jana Nová, Mgr. Adam Novák", "owner" => "VUT v Brně"],
    ["name" => "Patent 3", "type" => "patent", "registration" => 2017, "acceptance" => 2020, "ending" => 2022, "authors" => "Ing. Michal Dvořák", "owner" => "VUT v Brně"],
    ["name" => "Patent 4", "type" => "patent", "registration" => 2016, "acceptance" => 2020, "ending" => 2022, "authors" => "Ing. Jana Nová", "owner" => "VUT v Brně"],
    ["name" => "Patent 5", "type" => "užitný vzor", "registration" => 2016, "acceptance" => 2020, "ending" => 2022, "authors" => "Ing. Jana Nová, Ing. František Vosáhlo", "owner" => "VUT v Brně"],
    ["name" => "Patent 6", "type" => "patent", "registration" => 2016, "acceptance" => 2020, "ending" => 2022, "authors" => "Ing. Jana Nová", "owner" => "VUT v Brně"],
];

$results = [
    ['name' => 'Výsledek výzkumu 1', 'people' => ['Jiří Novák, Ing', 'Adam Jiřík, Mgr.', 'Petr Galuška, Ing.']],
    ['name' => 'Výsledek výzkumu 2', 'people' => ['Jaromír Petřík, Ing', 'Adam Jiřík, Mgr.', 'Adam Novák, Ing.']],
    ['name' => 'Výsledek výzkumu 3', 'people' => ['Petr Pavlík, Ing', 'Adam Jiřík, Mgr.', 'Petr Galuška, Ing.']],
];

?>

<div class="b-hero-header b-hero-header--nav">
    <div class="b-hero-header__img" style="background-image: url('/img/illust/b-hero-header--08.jpg');"></div>
    <div class="b-hero-header__content b-hero-header__content--bottom holder holder--lg text-left">
        <p class="mb0">
        </p>
        <h1 class="title title--secondary b-hero-header__title">
            <span class="title__item"><?php echo "{$scienceGroup['name']}" ?></span>
        </h1>

        <ul class="m-main__list profile-tablist" role="tablist">
            <li class="m-main__item">
                <a href="#detail" class="m-main__link" role="tab" aria-controls="detail"  aria-selected="true">O skupině</a>
            </li>
            <li class="m-main__item">
                <a href="#tym" class="m-main__link" role="tab" aria-controls="tym" >Tým</a>
            </li>
            <li class="m-main__item">
                <a href="#projekty" class="m-main__link" role="tab" aria-controls="projekty" >Projekty</a>
            </li>
            <li class="m-main__item">
                <a href="#produkty" class="m-main__link" role="tab" aria-controls="produkty" >Produkty</a>
            </li>
            <li class="m-main__item">
                <a href="#publikace" class="m-main__link" role="tab" aria-controls="publikace" >Publikace</a>
            </li>
            <li class="m-main__item">
                <a href="#patenty" class="m-main__link" role="tab" aria-controls="patenty" >Patenty</a>
            </li>
        </ul>
    </div>

</div>
    <div id="detail" role="tabpanel">
    <div class="b-detail">
    <div class="grid grid--0">

        <div class="grid__cell size--t-12-12">
            <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                <div class="c-articles-lg ">
                    <div class="holder holder--lg">
                        <h2 class="c-articles-lg__title">Úspěchy skupiny</h2>
                    </div>
                    <div class="c-articles-lg__wrap border-b">
                        <ul class="c-articles-lg__list grid grid--0">
                            <?php foreach ($actualities as $actuality){
                                echo "<li class=\"c-articles-lg__item grid__cell size--t-6-12\">
						<article class=\"b-article-lg holder holder--lg\" role=\"article\">
							<a href=\"../vedavyzkum/uspechy_detail.php\" class=\"b-article-lg__link\">
								<div class=\"b-article-lg__img\">
									<div class=\"b-article-lg__img-bg\" style=\"background-image: url({$actuality['image']});\"></div>
								</div>
								<div class=\"b-article-lg__head\">
									<time class=\"b-article-lg__date date font-secondary\" datetime=\"2017-02-01\">
										<span class=\"date__day\">{$actuality['day']}</span>
										<span class=\"date__month\">{$actuality['month']}</span>
										<span class=\"date__year\">{$actuality['year']}</span>
									</time>
									<h3 class=\"b-article-lg__title\">{$actuality['title']}</h3>
								</div>
								<div class=\"b-article-lg__content\">
									<p>
										{$actuality['short-text']}
									</p>
								</div>
							</a>
						</article>
					</li>";} ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="c-highlights border-t border-b">
        <ul class="c-highlights__list grid grid--bd grid--0">
            <?php foreach($stats as $stat){
                echo "<li class=\"c-highlights__item grid__cell size--t-6-12 size--3-12\">
                <div class=\"b-highlight holder\">
                    <p class=\"b-highlight__title font-secondary\">
                        {$stat['amount']}</p>
                    <div class=\"b-highlight__text\">
                        <p>{$stat['desc']}</p>
                    </div>
                </div>
            </li>";}?>

        </ul>
    </div>

    <div class="holder holder--lg pt40">
        <div class="sg-box">
            <div class="sg-box__item">
                <div class="">
                    <h2 class="sg-box__item-title">Témata výzkumu</h2>
                </div>
                <div class="sg-box__item-code sg-box__item-code--bleed">
                    <div class="c-highlights holder holder--lg pt40--m pb40--m pt40--t pb40--t pt55 pb75">
                        <ul class="c-highlights__list c-highlights__list--bd grid grid--bd grid--0">
                            <?php foreach($topics as $topic){
                               echo "<li class=\"c-highlights__item grid__cell size--t-6-12 size--2-12\">
                                <div class=\"b-highlight b-highlight--sm holder\">
                                    <div class=\"b-highlight__text\">
                                        <p>{$topic}</p>
                                    </div>
                                </div>
                            </li>";}?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="sg-box__item">
                <div class="sg-box__item-code sg-box__item-code--bleed">
                    <div class="c-carousel c-carousel--fade border-t border-b">
                        <div class="c-carousel__wrap js-carousel-img">
                            <?php foreach($equipments as $equipment){
                               echo "<div class=\"b-img\">
                                <div class=\"b-img__content holder holder--lg holder--sm-r\">
                                    <div class=\"scroll h400\">
                                        <div class=\"scroll__content pt20--m pt20--t pt50\">
                                            <h2 class=\"b-img__title\">
                                                <span class=\"b-img__subtitle h3\">{$equipment['type']}</span>
                                                {$equipment['name']}
                                            </h2>
                                            <p class=\"fz-lg\">
                                                {$equipment['text']}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class=\"b-img__img\">
                                    <img src=\"/img/illust/{$equipment['image']}\" width=\"720\" height=\"540\" alt=\"\">
                                </div>
                            </div>";
                            } ?>
                        </div>

                        <div class="c-carousel__nav grid grid--bd border-t">
                            <div class="grid__cell size--s-6-12">
                                <p class="mb0 text-center">
                                    <a href="#" class="c-carousel__prev link-nav link-nav--prev font-secondary">
                                        <span class="link-nav__inner">
                                            <span class="icon-svg icon-svg--angle-l link-nav__icon">
                                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <use xlink:href="/img/bg/icons-svg.svg#icon-angle-l" x="0" y="0" width="100%" height="100%"></use>
                                            </svg>
                                        </span>

                                            Předchozí
                                        </span>
                                    </a>
                                </p>
                            </div>

                            <div class="grid__cell size--s-6-12">
                                <p class="mb0 text-center">
                                    <a href="#" class="c-carousel__next link-nav link-nav--next font-secondary">
                                        <span class="link-nav__inner">
                                            <span class="icon-svg icon-svg--angle-r link-nav__icon">
                                                <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
                                                </svg>
                                            </span>

                                            Další
                                        </span>
                                    </a>
                                </p>
                            </div>
                        </div>

                        <p class="c-carousel__pager c-carousel__pager--bottom font-secondary">1/8</p>
                    </div>
                </div>
            </div>
            <div class="sg-box">
                <h1 class="sg-box__title">Partneři skupiny</h1>

                <div class="sg-box__item">
                    <div class="sg-box__item-code sg-box__item-code--bleed">
                        <div class="c-logos holder holder--lg">
                            <ul class="c-logos__list">
                                <?php foreach($partners as $partner){
                                  echo "<li class=\"c-logos__item\">
                                    <img src=\"/img/illust/{$partner["image"]}\" width=\"100%\" height=\"56\" alt=\"\" class=\"c-logos__img\">
                                </li>";
                                }?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div id="tym" role="tabpanel">
        <div class="c-employees">
            <div class="holder holder--lg">
                <h2 class="c-employees__title">Tým</h2>
            </div>
            <div class="holder holder--lg border-t">
                <div class="c-employees__wrap">
                    <ul class="c-employees__list grid grid--60">
                        <?php
                            foreach($team as $man){
                                echo "<li class=\"c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
                                        <a href=\"../ofakulte/profil.php\" class=\"b-employee\">
                                            <div class=\"b-employee__wrap\">
                                                <div class=\"b-employee__img\">
                                                    <img src=\"/img/illust/b-employee--01.jpg\" width=\"100\" height=\"100\" alt=\"\">
                                                </div>
                                                <h3 class=\"b-employee__name\">{$man['name']}</h3>
                                                <div class=\"b-employee__footer\">
                                                    <p class=\"b-employee__position font-secondary\">{$man['function']}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>";
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="projekty" role="tabpanel">
        <div class="c-subjects holder holder--lg">
            <ul class="c-subjects__list">
                <?php foreach($projects as $project){
                    echo "<li class=\"c-subjects__item\">
                <div class=\"b-subject\">
                    <h2 class=\"b-subject__title font-primary h4\">
                        <a href=\"../vedavyzkum/projekty_detail.php\" class=\"b-subject__link\">{$project["name"]}</a>
                    </h2>
                    <p class=\"b-subject__annot\">
                        <span class=\"b-subject__annot-item\">Období řešení: {$project["From"]} — {$project["To"]}</span>
                        <span class=\"b-subject__annot-item\">Financování: {$project["financed"]}</span>
                    </p>
                </div>
            </li>";
                }?>

            </ul>
        </div>
    </div>
    <div id="publikace" role="tabpanel">
        <div class="c-subjects holder holder--lg">
            <ul class="c-subjects__list">
                <?php foreach($publications as $publication){
                    echo "<li class=\"c-subjects__item\">
                <div class=\"b-subject\">
                    <h2 class=\"b-subject__title font-primary h4\">
                        <a href=\"../vedavyzkum/publikace_detail.php\" class=\"b-subject__link\">{$publication["name"]}</a>
                    </h2>
                    <p class=\"b-subject__annot\">
                        <span class=\"b-subject__annot-item\">Autor: {$publication["author"]}</span>
                        <span class=\"b-subject__annot-item\">Spolupracovali: {$publication["coauthors"]}</span>
                    </p>
                </div>
            </li>";
                }?>

            </ul>
        </div>
    </div>

    <div id="produkty" role="tabpanel">
        <div class="c-subjects holder holder--lg">
            <ul class="c-subjects__list">
                <?php foreach($products as $product){
                    echo "<li class=\"c-subjects__item\">
                <div class=\"b-subject\">
                    <h2 class=\"b-subject__title font-primary h4\">
                        <a href=\"#\" class=\"b-subject__link\">{$product["name"]}</a>
                    </h2>
                    <p class=\"b-subject__annot\">
                        <span class=\"b-subject__annot-item\">Typ: {$product["type"]}</span>
                        <span class=\"b-subject__annot-item\">Rok: {$product["year"]}</span>
                        <span class=\"b-subject__annot-item\">Autoři: {$product["authors"]}</span>
                    </p>
                </div>
            </li>";
                }?>

            </ul>
        </div>
    </div>

    <div id="patenty" role="tabpanel">
        <div class="c-subjects holder holder--lg">
            <ul class="c-subjects__list">
                <?php foreach($patents as $patent){
                    echo "<li class=\"c-subjects__item\">
                <div class=\"b-subject\">
                    <h2 class=\"b-subject__title font-primary h4\">
                        <a href=\"#\" class=\"b-subject__link\">{$patent["name"]}</a>
                    </h2>
                    <p class=\"b-subject__annot\">
                        <span class=\"b-subject__annot-item\">Registrace: {$patent["registration"]}</span>
                        <span class=\"b-subject__annot-item\">Přijetí: {$patent["acceptance"]}</span>
                        <span class=\"b-subject__annot-item\">Vypršení: {$patent["ending"]}</span>
                        <span class=\"b-subject__annot-item\">Typ: {$patent["type"]}</span>
                        <span class=\"b-subject__annot-item\">Rok: {$patent["year"]}</span>
                        <span class=\"b-subject__annot-item\">Autoři: {$patent["authors"]}</span>
                        <span class=\"b-subject__annot-item\">Majitel: {$patent["owner"]}</span>
                    </p>
                </div>
            </li>";
                }?>

            </ul>
        </div>
    </div>


<?php
include '../footer.php'
?>