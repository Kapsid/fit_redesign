<?php
include '../header.php';

$publications = [
    ["name" => "Testovací publikace", "author" => "Ing. Jiří Novák", "link" => "../ofakulte/profil.php", "coauthors" => "Ing. Jana Nová , Mgr. Petr Pavel"],
    ["name" => "Školní publikace", "author" => "Ing. Jiří Novág", "link" => "../ofakulte/profil.php", "coauthors" => "Ing. Jana Nová"],
    ["name" => "Vývoj techniky ve 21. století", "author" => "Ing. Zdeněk Bukal", "link" => "../ofakulte/profil.php", "coauthors" => "Ing. Jana Nová, Mgr. Petr Pavel, Mgr. František Kocáb"],

]
?>

    <div class="b-intro border-b holder holder--lg">
        <h1 class="b-intro__title">Publikace</h1>

        <form action="?" class="f-subjects">
            <div class="f-subjects__filter">
                <p class="inp inp--multiple">
							<span class="inp__fix minw440">
								<label for="faculty" class="inp__label inp__label--inside">Fakulta/pracoviště</label>
								<select name="faculty" id="faculty" class="select js-select">
									<option selected disabled placeholder>Výzkumná skupina</option>
									<option>Skupina A</option>
									<option>Skupina B</option>
								</select>
							</span>
                    <span class="inp__fix minw180">
								<label for="year" class="inp__label inp__label--inside">Rok</label>
								<select name="year" id="year" class="select js-select">
									<option selected disabled placeholder>Rok</option>
									<option>2017</option>
									<option>2018</option>
								</select>
							</span>
                </p>
            </div>

            <div class="f-subjects__search">
                <p class="inp inp--group mb0">
							<span class="inp__fix">
								<label for="f-subjects__search" class="inp__label inp__label--inside">Název projektu, klíčová slova, popis&hellip;</label>
								<input type="text" class="inp__text" id="f-subjects__search" placeholder="Název projektu, klíčová slova, popis&hellip;">
							</span>
                    <span class="inp__btn">
								<button class="btn btn--secondary btn--block--m" type="submit">
									<span class="btn__text">Hledat</span>
								</button>
							</span>
                </p>
            </div>
        </form>
    </div>

    <div class="c-subjects holder holder--lg">
        <ul class="c-subjects__list">
            <?php foreach($publications as $publication){
                echo "<li class=\"c-subjects__item\">
            <div class=\"b-subject\">
                <h2 class=\"b-subject__title font-primary h4\">
                    <a href=\"../vedavyzkum/publikace_detail.php\" class=\"b-subject__link\">{$publication["name"]}</a>
                </h2>
                <p class=\"b-subject__annot\">
                    <span class=\"b-subject__annot-item\">Autor: {$publication["author"]}</span>
                    <span class=\"b-subject__annot-item\">Spolupracovali: {$publication["coauthors"]}</span>
                </p>
            </div>
        </li>";
            }?>

        </ul>

        <nav class="pagination " aria-label="Stránkování" role="navigation">
            <ul class="pagination__list">
                <li class="pagination__item">
                    <a href="#" class="pagination__link" aria-current="page">1</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">2</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">3</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="btn btn--secondary btn--wide btn--icon-only--m">
				<span class="btn__text">
					<span class="hide--m">Další</span>
					<span class="icon-svg icon-svg--angle-r btn__icon hide--t hide--d">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

				</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
<?php
include '../footer.php'
?>