<?php
include '../header.php';

$people = [
    ["name" => "Jana", "surname" => "Burdíková", "degree" => "Ing,"],
    ["name" => "Filip", "surname" => "Mravec", "degree" => "Ing,, Ph.D."],
    ["name" => "Jana", "surname" => "Burdíková", "degree" => "Ing,"],
    ["name" => "Jaromír", "surname" => "Wesserbauer", "degree" => "Ing,"],
];

$keywords = [
    "Fotonová korelační spektroskopie", "Fotonová kros-korelační spektroskopie", "Polysstyrenová částice", "Rozptyl světla", "Difuze"
];

$citation = [
    "authors" => "BURDÍKOVÁ, J.; MRAVEC, F.; WASSERBAUER, J.; PEKAŘ, M.",
    "name" => "A practical comparison of photon correlation and cross-correlation spectroscopy in nanoparticle and microparticle size evaluation",
    "year" => "2017",
    "cathegory" => "Colloid and Polymer Science",
    "issn" => "0303-402X"

];

$publication = [
    "name" => "Praktické srovnání fotonové korelační a kros-korelační spektroskopie ve studiu velikosti nano- a mikročástic",
    "people" => $people,
    "perex" => "A practical comparison of photon correlation and cross-correlation spectroscopy in nanoparticle and&nbsp;microparticle size evaluation",
    "abstract" => "Standardní polystyrenové částice byly použity pro&nbsp;stanovení rozdílu mezi dvěma metodami pro stanovení velikosti částic založenými na dynamickém rozptylu světla.",
    "keywords" => $keywords,
    "lang" => "Čeština",
    "type" => "článek v časopise",
    "citation" => $citation,
    "parentProject" => "Pokročilé fyzikálně-chemické procesy a jejich aplikace",
];
?>

<div class="b-detail">
    <div class="holder holder--lg b-detail__head">
        <p class="mb20">
            <span class="tag tag--sm">Detail publikace</span>
        </p>
        <h1><?php echo "{$publication['name']}"; ?></h1>
        <p class="b-detail__annot">
            <span class="b-detail__annot-item">Autoři:</span>
            <?php foreach($publication["people"] as $person){
                echo "<span class=\"b-detail__annot-item\"><a href=\"../ofakulte/profil.php\" class=\"font-bold\">{$person['surname']} {$person['name']}, {$person['degree']}</a></span>";
            }?>
        </p>
    </div>

    <div class="b-detail__body border-t">
        <div class="row-main row-main--md ml0">
            <div class="grid grid--0">
                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Označení
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php echo "{$publication['perex']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Abstrakt
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php echo "{$publication['abstract']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Klíčová slova
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php foreach($publication['keywords'] as $keyword){
                               echo "{$keyword},";
                            }?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Originální jazyk
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php echo "{$publication['lang']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Typ
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php echo "{$publication['type']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Citace
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php echo "{$publication['citation']['authors']}";?><br>
                            <?php echo "{$publication['citation']['name']}";?> <?php echo "{$publication['citation']['cathegory']}";?>, <?php echo "{$publication['citation']['year']}";?>, ISSN: <?php echo "{$publication['citation']['issn']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        V rámci projektu
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <a href="../vedavyzkum/projekty_detail.php"><?php echo "{$publication['parentProject']}";?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include '../footer.php';
?>