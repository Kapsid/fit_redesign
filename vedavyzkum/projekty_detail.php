<?php
include '../header.php';

$solvers = [
    ["name" => "Jana", "surname" => "Řezáčová", "degree" => "Ing."],
    ["name" => "Michael", "surname" => "Nylander", "degree" => "PhD."],
    ["name" => "Karel", "surname" => "Plíhal", "degree" => "Mgr."],
];

$subjects = [
    "Fakulta architektury","Fakulta strojního inženýrství","Fakulta stavební"
];

$project = [
    "title" => "Aby měli lidé k sobě blíž, Umění a architektura jako nástroje budování socialistické kultury pevnosti, pohlcuje ne městský, až mě oslabuje",
    "from" => "01. 01. 2017",
    "to" => "31. 12. 2017",
    "financed" => "Vnitřní projekty VUT",
    "about" => "Podstatou předkládaného projektu je výzkum architektury (specificky masové industrializované výstavby, ale i solitérních staveb jakožto exkluzivních režimních zakázek) a výtvarného umění (především v jeho užité formě výtvarné výzdoby – „zvýtvarnění“ architektury a veřejného, respektive životního prostoru) jako symptomů socialistické kultury. Témata byla zvolena s ohledem na výzkumné záměry zúčastněných řešitelů a v souladu se zaměřením doktorských prací zúčastněných studentů a a lze je proto chápat jako jejich prohloubení.",
    "signing" => "FA/FAVU-S-14-2340",
    "lang" => "Čeština",
    "solvers" => $solvers,
    "subjects" => $subjects,
    "publication" => "Praktické srovnání fotonové korelační a kros-korelační spektroskopie ve studiu velikosti nano-&nbsp;a&nbsp;mikročástic"
];
?>

<div class="b-detail">
    <div class="holder holder--lg b-detail__head pb40--m">
        <p class="mb20">
            <span class="tag tag--sm">Detail projektu</span>
        </p>
        <h1 class="b-detail__title"><?php echo "{$project['title']}";?></h1>
        <p class="b-detail__annot">
            <span class="b-detail__annot-item">Období řešení: <strong><?php echo "{$project['from']} - {$project['to']}";?></strong></span>
            <span class="b-detail__annot-item">Financování: <strong><?php echo "{$project['financed']}";?></strong></span>
        </p>
    </div>

    <div class="b-detail__body border-t">
        <div class="row-main row-main--md ml0">
            <div class="grid grid--0">
                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        O projektu
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content fz-lg">
                        <p>
                            <?php echo "{$project['about']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Označení
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php echo "{$project['signing']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Originální jazyk
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php echo "{$project['lang']}";?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Řešitelé
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php foreach($project['solvers'] as $solver){
                                echo "<a href=\"../ofakulte/profil.php\">{$solver['name']} {$solver['surname']}, {$solver['degree']}</a><br> ";
                            }?>
                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Útvary
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <?php foreach($project['subjects'] as $subject){
                                echo "<a href=\"#\" class=\"link-ext\">{$subject}</a><br>";
                            }?>

                        </p>
                    </div>
                </div>

                <div class="grid__cell size--t-3-12 holder holder--lg holder--0-r">
                    <p class="b-detail__subtitle font-secondary">
                        Publikace
                    </p>
                </div>
                <div class="grid__cell size--t-9-12 holder holder--lg">
                    <div class="b-detail__content">
                        <p>
                            <a href="#"><?php echo "{$project['publication']}";?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include '../footer.php';
?>