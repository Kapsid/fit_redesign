<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Linux na učebnách</h2>
            <p>
                <strong>Slovo úvodem</strong><br />
                Od zimního semestru 2002 jsou na všech učebnách CVT FIT nainstalovány duálně operační systémy Windows 7 a Linux. Pro Linux je použita distribuce CentOS verze 7 s postupnými přechody na novější verze programů.<br />

                Budu vděčný za veškeré dotazy, připomínky a návrhy, které se budou týkat linuxových instalací. Veskerou komunikaci je nejlépe směrovat na e-mail, případně osobně, pokud mne zastihnete.<br />


                e-mail: <a href="mailto:linux@fit.vutbr.cz">linux@fit.vutbr.cz</a>
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Základy práce s Linuxem</h2>
            <p>
                <strong>Spuštění systému</strong><br />
                Po zapnutí/restartování počítače se zobrazí menu pro výběr operačního systému. Zde je možno zvolit Windows 7 nebo Linux CentOS 7
                (poslední zvolená volba se zapamatuje pro příště). Po výběru linuxu dojde ke spuštění systému a je možno se přihlásit na váš unixový účet
                (jako na serveru eva nebo kazi). Stanice se spouští přímo do grafického prostředí. Vypnutí a restart je v menu dole.<br />

                Přepnutí z grafického do textového režimu se provede pomocí CTRL-ALT-Fn kde n je číslo textové konzole (1-5). Konzole 1 je grafické sezení, 2-5 jsou textová.<br />

                V textovém režimu vypadá přihlášení následovně - výzva k přihlášení:<br /><br />

                <i>CentOS release 7 (Core)<br />
                Kernel 4.4.147 on an x86_64<br />
                    Login:</i><br /><br />

                Na tuto výzvu zadejte svoje přihlašovací jméno. Objeví se následující výzva:

                <br />
                <i>Password:</i><br /><br />
                Zadejte svoje Unixové heslo a spustí se interpret příkazů - shell bash. Ten vypadá nějak takhle:

                <br />
                <i>xkaspa06@merlin ~></i><br /><br />
                Pokud potřebujete více informací o možnostech shellu, podívejte se do manuálových stránek příkazem man bash.<br /><br />

                <strong>Ukončení práce</strong>
                Pokud již nechcete na počítači dále pracovat, pouze se odhlaste (příkazy exit nebo logout) a stanici ponechte
                v linuxu (nebo ji restartujte pomocí CTRL-(levý)ALT-DEL). Pokud chcete počítač vypnout, použijte příkaz /usr/bin/poweroff. Ten povolí vypnutí pouze lokálně přihlášeným uživatelům.<br /><br />

                <strong>Práce v systému</strong>
                Po přihlášení je vám namapován váš domácí adresář z eva nebo kazi. Zde platí veškerá omezení (velikosti, počet souborů, atd.)
                jako na vlastních serverech. Mimo to je momentálně k dispozici jistá disková kapacita na linuxovém serveru merlin, která je na
                linuxových počítačích namountována pod adresář /pub . Zde se nachází adresář /pub/tmp s obvyklým účelem (jako /tmp ale sdílen po síti)
                a dále jednotlivé adresáře pro další zájemce a předměty (/pub/courses/...).<br /><br />


            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Linuxové servery</h2>
            <p>
                Pro účely správy, sdílení dat a podobně je zřízen linuxový server merlin.fit.vutbr.cz. Server merlin
                poskytuje do sítě sdílený disk připojený na /pub a SVN/GIT strom pro správu projektů. O zařazení
                libovolného projektu do systému SVN/GIT je možno požádat na výse zmíněné adrese, správce SVN/GIT stromu je možno zastihnout na linux@fit....
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Řešení problémů,připomínky a dotazy</h2>
            <p>
                Obecně je možno jakékoliv nejasnosti řesit emailem na linux@fit.vutbr.cz a to obzvláště:
                <ul>
                    <li>oznámení chyb: zaslete především jméno počítače a co nejpodrobnější popis chyby nebo neobvyklého chování</li>
                    <li>připomínky k programům: pokud vám chybí váš oblíbený program, pošlete mi jeho jméno, stručný popis a adresu nějakého jeho zdroje a pokud to bude možné, pokusím se vám vyhovět a zařadit jej na stanice.</li>
                </ul>
            </p>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
