<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Bezpečné přihlašování</h2>
            <p>
                Při přihlašování na servery Unix přes počítačovou síť programem <strong>telnet</strong> nebo <strong>ftp</strong> jde zadávané heslo v otevřené podobě a hrozí nebezpečí odposlechu hesla na síti.
                Toto nebezpečí není nijak hypotetické, na síti typu Ethernet se může napojit kdokoli kdekoli na segment sítě a odposlouchávat provoz (s jistými omezeními i na síti která je tvořená přepínači). Stejně tak, pokud se přihlašujete na dálku, z terminálových serverů,
                jiných sítí, jiných areálů, nemůžete si být nikdy jisti, že někde po cestě není napojena sonda, která snímá síťový provoz a zajímavé informace ukládá.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">ssh</h2>
            <p>
                Na všech fakultních serverech je podporováno přihlašování pomocí <strong>ssh</strong> (slogin). Program <strong>ssh</strong> nahrazuje program <strong>telnet</strong> a zajišťuje bezpečné šifrované spojení na server.
                Program <strong>ssh</strong> je dostupný pro systémy Unix na <a href="http://www.openssh.org/">http://www.openssh.org/</a>. V běžných distribucích systémů Linux a BSD je dnes standardně začleněn. Pokud se tedy přihlašujete na
                fakultní servery z jiných systémů Unix, použijte program <strong>ssh</strong> nebo <strong>slogin</strong>.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">ssh pro W7/W8/W10</h2>
            <h3>Putty</h3>
            <p>
                Viz <a href="http://www.chiark.greenend.org.uk/~sgtatham/putty/">http://www.chiark.greenend.org.uk/~sgtatham/putty/</a>. Obsahuje jak ssh, tak scp a sftp. Ve fakultní síti FIT tyto aplikace najdete na síťovém disku Q:\netapp\putty. Putty lze
                použít i pro vzdálený přístup na fakultní servery (port forwarding).<br /><br />
                <strong>Příklad:</strong> Některé NAT přeposílají pakety na různé Web servery pokaždé z jiné zdrojové IP adresy, což činí problém při autentizaci přes CAS FIT.
                Stačí přidat do Putty: Change Settings -> Connection - SSH - Tunnels -> Source Port: 1234, Destination: IP adresa:443, zaškrtnuto Local, Auto -> stisknout Add.<br />
                Pozor: je nutno zadat IP adresu, nelze použít jméno serveru.<br />
                Nyní můžete použít pro přístup na daný Web server lokální adresu <strong>localhost:1234</strong>. Pro další servery pak použijte jiné lokální porty (1235, 1236, atd.). Mapování lze doplnit i v průběhu spojení, ale nelze jej v této situaci uložit. Trvalé mapování nastavíte postupem: Load session -> nastavení viz výše -> Save session.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Android, iOS</h2>
            <p>
                Implementace ssh existují i pro mobilní operační systémy, stačí v aplikačním obchodě vyhledat SSH. Pro Android např. Connectbot, pro iOS např. Termius - ale na výběr je mnoho aplikací.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Další</h2>
            <p>
                Další implementace SSH pro Windows viz <a href="http://wiht.link/ssh-putty">http://wiht.link/ssh-putty</a>.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">WinSCP a SFTP</h2>
            <p>
                Pro kopírování souborů není příliš rozumné používat FTP (heslo jde taky po síti v otevřené podobě).
                Bezpečný kopírovací program je v Unixu součástí SSH (scp) nebo OpenSSH (scp a sftp). Do systémů Windows je k dispozici volně šířený WinSCP.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">IMAP4, POP3, Web, SMTP</h2>
            <p>
                Pro přístup k poštovním schránkám, chráněným Web stránkám a autentizované odesílání pošty je opět třeba zadávat uživatelské jméno a heslo,
                které putuje v otevřené podobě. Ke všem těmto službám dnes existují varianty, které pracují přes bezpečný šifrovaný kanál SSL.
                Jejich použití podporují všechny rozšířené aplikace. Takže pro vzdálený přístup k poštovní schránce používejte vždy protokol IMAP4 přes SSL
                (port 993), protokol POP3 přes SSL (port 995), protokol HTTPS (port 443) a SMTP přes SSL (buď na portu 465 nebo na port 587 s příkazem STARTTLS).
                V některých případech je použití SSL tvrdě vyžadováno, například Web stránky, které vyžadují zadání hesla, jsou přístupné pouze přes protokol HTTPS,
                IMAP4 a POP3 servery nedovolují otevřené spojení mimo lokální síť fakulty apod.
            </p>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
