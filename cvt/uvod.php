<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
            <h2 class="">Důležité informace a pokyny</h2>
        </div>
    </div>
    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Informace</h2>
            </div>
        </div>
    </div>
    <div class="c-faculties holder holder--lg c-faculties--pattern">

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="./elektronicka_posta.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Studentská elektronická pošta</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="./ucty_bezpecnost.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Účty a bezpečnost dat</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="./bezpecne_prihlasovani.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Bezpečné přihlašování</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="./linux_na_ucebnach.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Linux na učebnách</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Unix na FIT pro začínající studenty</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Jak bojovat proti SPAMU?</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Pomoc při řešení problémů</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">MS Active Directory</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Centrální autentizace Web serverů</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Kuchařka ke studentským průkazům</h2>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Pravidla provozu</h2>
            </div>
        </div>
    </div>
    <div class="c-faculties holder holder--lg c-faculties--pattern">

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Licence pro výukový software</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Provozní řád CVT FIT</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="http://www.fit.vutbr.cz/CVT/net/Smernice-22-2017.pdf" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Pravidla provozu počítačové sítě VUT [PDF]</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Zásady používání sítě CESNET</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="http://www.fit.vutbr.cz/info/predpisy/Provozni_rad_serveroven_FIT_VUT_11.4.2014.pdf" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Provozní řád serveroven FIT [PDF]</h2>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Počítačová síť</h2>
            </div>
        </div>
    </div>
    <div class="c-faculties holder holder--lg c-faculties--pattern">

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">IPv6 v síti FIT</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Bezdrátová (WiFi) síť FIT</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Fakultní síť</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Přehled správců FIT</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Síť CESNET</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Lokální služby</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Evropská síť GEANT a její vývoj</h2>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Co by měl znát každý uživatel internetu</h2>
            </div>
        </div>
    </div>
    <div class="c-faculties holder holder--lg c-faculties--pattern">

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://tools.ietf.org/html/rfc1462" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Co je Internet?</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://www.internetsociety.org/internet/history-of-the-internet-in-africa/" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Historie Internetu</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://tools.ietf.org/html/rfc2664" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Odpovědi na často kladené otázky začátečníků</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://tools.ietf.org/html/rfc2664" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Slabikář internetových nástrojů a pomůcek</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://tools.ietf.org/html/rfc1855" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Netiquette</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://tools.ietf.org/html/rfc1087" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Etika a Internet</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://tools.ietf.org/html/rfc2635" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Co je to SPAM a jak proti němu bojovat</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="https://tools.ietf.org/html/rfc2504" target="_blank" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Bezpečnostní příručka uživatele</h2>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
