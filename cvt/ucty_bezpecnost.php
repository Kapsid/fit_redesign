<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Účty a bezpečnost dat</h2>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Studentské účty</h2>
            <p>
                Všem studentům je na počátku studia vytvořeno několik počítačových účtů, které jsou identifikovány přihlašovacím jménem a heslem.
                Pro všechny fakultní účty se používá stejné přihlašovací jméno, které se skládá z písmene x, pěti znaků příjmení (případně doplněných křestním jménem)
                a rozlišovacích dvou znaků - např. xvoprs13. Všechny fakultní účty mají na počátku stejné heslo. Z bezpečnostních důvodů je vhodné heslo co nejdříve změnit
                (některé systémy to přímo vyžadují). Protože se ale jedná o různé účty, změna hesla u jednoho z nich neznamená změnu na jiném.
            </p>
            <p>
                O jaké účty se jedná?<br /><br />

                <strong>Unixový účet FIT</strong> - je primárním účtem pro autentizaci v prostředí fakulty. Používá se pro přístup do informačního systému FIT, k centrální autentizaci chráněných Web serverů, pro čtení a odesílání elektronické pošty, atp. Slouží také pro přihlašování na servery, pracovní stanice a Linuxy. Změna hesla na kterémkoli serveru nebo v Linuxu se projeví také na ostatních systémech. Na počítačích se systémem Linux se projeví po restartu systému nebo do hodiny, na ostatních serverech do 1 minuty. Tento účet je přidělen po celou dobu studia, umožňuje vytvářet si vlastní web stránky s adresou http://www.stud.fit.vutbr.cz/~xvoprs13 a přijímat elektronickou poštu s adresou xvoprs13@stud.fit.vutbr.cz.
                Login a počáteční heslo je možné vypsat na vstupní stránce informačního systému.<br /><br />

                <strong>Účet do Microsoft Active Directory (AD)</strong>. Počátečně je nastavení tohoto účtu (login a heslo) stejné jako Unixového účtu FIT. Změny hesla jsou ale nezávislé, pokud si změníte heslo do AD, neprojeví se tato změna v Unixu a naopak. V informačním systému je možnost nastavit heslo do AD stejné jako pro Unixový účet FIT (doporučeno).
                Tento účet se používá pro přihlášení do operačních systémů Microsoft Windows 7 na učebnách a v knihovně.<br /><br />

                <strong>Účet pro autentizaci přenosných počítačů při připojení do sítě (Radius účet)</strong>. Tento účet má login shodný jako Unixový účet FIT, ale heslo je záměrně jiné, protože je uloženo v otevřené podobě a zachází se s ním méně bezpečně. Je například povoleno uložit si je v otevřené podobě pro automatické přihlášení do sítě na notebooku. Toto heslo je vygenerováno automaticky na stránce správy účtů v IS FIT a lze jej kdykoli změnit. Změna se uplatní okamžitě.
                <strong>Účet pro informační systém VUT</strong> - VUT login. Spolu s VUT heslem jej použijete především pro přihlašování do celoškolských aplikací, především na intraportál VUT a na elektronickou přihlášku na koleje. Některé celoškolské aplikace (VPN, WiFi síť VUT a OPAC katalog knihoven) místo VUT hesla vyžadují VUT PIN.
                Ve všech aplikacích by mělo být možné místo VUTloginu zadávat osobní číslo (číslo ze studentského průkazu).
                VUT login je zcela nezávislý na fakultních účtech a platí i pro studium na jiných fakultách VUT.
                Počáteční heslo účtu najde nový student v elektronické přihlášce.<br /><br />
            </p>
            <p>
                Práva a povinnosti spojená s užíváním studentských účtů upřesňují Pravidla provozu počítačové sítě VUT [PDF], která jsou závazná pro celé VUT.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Doktorandské účty</h2>
            <p>
                Pro doktorandy platí stejná pravidla jako pro studenty. Pokud má doktorand pracovní poměr na FIT, pak má navíc zaměstnanecký účet FIT ve
                tvaru ipříjmení. Studentský účet je určen pro studentské záležitosti (do informačního systému, koleje, apod.).
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Bezpečnost účtů, změna hesla</h2>
            <p>
                Účet smí používat výhradně uživatel, kterému byl účet přidělen. Není dovoleno zapůjčení účtu (hesla) a tím přístup k výpočetním a informačním systémům jiným osobám. Uživatelé jsou navíc povinni nevytvářet příležitosti pro neoprávněné užití - např. volbou snadno uhodnutelného hesla nebo tím, že je neudržují v tajnosti. Heslo by nemělo být:

                <br />

                <ul>
                    <li>Kratší než 8 znaků. Některé systémy Unix (ty, které používají pro kontrolu hesla původní algoritmus DES) testují pouze prvních 8 znaků hesla.</li>
                    <li>Uživatelské jméno, příjmení a jakákoli jména obecně.</li>
                    <li>Slovo ze slovníku (anglické, české), a to ani s doplněním číslic či speciálních znaků.</li>
                    <li>Osobní číslo, rodné číslo, číslo OP, apod.</li>
                </ul>

                Pozn.: Při analýze jedné velké databáze hesel se ukázalo, že uživatelé často volí triviální hesla:

                <table>
                    <tr>
                        <th>#</th>
                        <th>Heslo</th>
                        <th>Počet uživatelů</th>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td>123456</td>
                        <td>290731</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>12345</td>
                        <td>79078</td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>123456789</td>
                        <td>76790</td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Password</td>
                        <td>61958</td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>iloveyou</td>
                        <td>51622</td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>princess</td>
                        <td>35231</td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td>rockyou</td>
                        <td>22588</td>
                    </tr>
                    <tr>
                        <td>8.</td>
                        <td>1234567</td>
                        <td>21726</td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>12345678</td>
                        <td>20553</td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>abc123</td>
                        <td>17542</td>
                    </tr>
                </table>
            </p>
            <p>
                Z tohoto důvodu není povoleno na serverech FIT volit vlastní heslo, ale pouze některé z náhodně předgenerovaných.<br />
                Hesla v Unixu se mění příkazem passwd (na libovolném Unixovém systému), ve Windows 7 pomocí trojhmatu Ctrl-Alt-Del -> Change Password. Při změnách dejte pozor na to, zda není zapnutá
                česká klávesnice - jinak heslo může obsahovat znaky, které už nebudete schopni znovu zadat.<br />

                V případě, že heslo zapomenete, postupujte podle <a href="#">těchto pokynů</a>. Heslo bude změněno po prokázání totožnosti (ověření, že žadatel je oprávněným uživatelem účtu). K prokázání
                totožnosti stačí platný průkaz studenta, případně jiný úřední doklad s podobenkou držitele a uvedením jména. Heslo není zásadně měněno na žádost emailem, po telefonu, apod.<br />
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Bezpečnost souborů</h2>
            <p>
                Vždy je třeba předpokládat, že vaše soubory mohou být čitelné kýmkoliv v síti Internet. Správci systémů vynakládají značné úsilí, aby k tomu nedošlo,
                ale v otázkách bezpečnosti je nutno předpokládat vždy to nejhorší. Co tedy můžete udělat pro bezpečnost svých dat? Výčet je seřazen od nejbezpečnější metody k nejméně bezpečné:<br /><br />

                1.Neukládejte důvěrné informace v počítači! (zejména nevhodné je ukládat si zde čísla bankovních účtů, piny, apod.)<br />
                2.Zpracujte důvěrné informace v počítači, ale po skončení je přesuňte na výměnné médium a v počítači je zrušte.<br />
                3.Zašifrujte data příkazem GNU <strong>gpg</strong> (viz man gpg).<br />
                4.V každém případě nastavte svým souborům taková práva, aby nebyly čitelné pro ostatní uživatele (v Unixu příkazem <strong>chmod og-rw file</strong>,
                ve Windows v Exploreru pravým tlačítkem <strong>Properties -> Security</strong>) a udržujte v tajnosti své heslo!
            </p>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
