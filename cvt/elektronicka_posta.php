<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Elektronická pošta</h2>
            <p>
                Všichni studenti FIT mají zřízen účet na Unixovém fakultním studentském serveru <strong>eva.fit.vutbr.cz</strong> po celou dobu studia na FIT. Na tento server přichází veškerá studentská pošta a zde si mohou studenti také vytvářet své domácí Web stránky. Oficiální e-mailová adresa studenta FIT je

                <strong>login@stud.fit.vutbr.cz</strong>
                Tato adresa je oficiální adresou studenta FIT a na ni budou doručovány veškeré e-maily týkající se studia na FIT. <strong>Dle studijního řádu musí studenti při elektronické komunikaci s fakultou používat tuto adresu.</strong> Doporučujeme studentům nepřesměrovávat si poštu na jiné poštovní servery pomocí souboru .forward, ale použít bezpečnější a proti zacyklení pošty odolnější <a href="#">.procmailrc.</a>
                Pro přihlášení se používá unixový účet FIT (viz. <a href="#">účty a bezpečnost dat)</a>.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Čtení pošty</h2>
            <p>
                Pro čtení pošty máte několik možností:<br />

                <strong>A.</strong>	Poštu ponechat na serveru eva a číst ji lokálně pomocí programu <strong>mutt</strong> nebo <strong>alpine</strong> - po přihlášení programem <strong>ssh</strong> (Putty ve Windows).<br />
                <strong>B.</strong>	Poštu ponechat na serveru eva a číst ji vzdáleně programem, který dokáže komunikovat s IMAP4 serverem (Mozilla Thunderbird, Outlook, Android/K9-Mail, apod.).
                Pokud nechcete nebo nemůžete použít klienta IMAP, můžete pro poštu využít Web email <a href="https://roundcube.fit.vutbr.cz">https://roundcube.fit.vutbr.cz</a> nebo <a href="https://email.fit.vutbr.cz">https://email.fit.vutbr.cz</a>.<br />
                <strong>C.</strong>	Poštu <a href="#">přesměrovat</a> na jiný server. Pokud si ovšem přesměrujete poštu nesprávně, může dojít velice snadno při první chybě doručení k zacyklení pošty a tím zahlcení
                poštovní schránky chybovými zprávami. Pokud přesto potřebujete poštu někam přesměrovat, řiďte se pokyny pro <a href="#">doručovací program procmail</a> nebo použijte pro přesměrování <a href="https://email.fit.vutbr.cz">https://email.fit.vutbr.cz</a>.
                Pokud nebude nějaký dopis doručen díky nesprávnému přesměrování pošty, můžete přijít o důležité studijní informace a pokyny.<br /><br />
                Varianty A a B mají výhodu v tom, že pošta je dostupná z kterékoli počítače připojeného k Internetu (a přímo také ze všech Linuxů a pracovních stanic CVT), velikost poštovní schránky není omezena a příchozí pošta je filtrována proti virům i proti spamu.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Odesílání a příjem pošty</h2>
            <p>
                Při odesílání pošty prostředníctvím počítačové sítě VUT je dle <a href="http://www.fit.vutbr.cz/CVT/net/Smernice-22-2017.pdf">Pravidel provozu počítačové sítě VUT</a> požadováno, aby adresa odesílatele byla jeho oficiální adresou.
                Odesílání pošty přes jiné než oficiální poštovní servery VUT je zakázáno a je zablokováno na úrovni firewallu VUT. Rozesílání spamu a jiných materiálů odporujících zásadám
                přijatelného použití počítačové sítě VUT a sítě CESNET2 je přestupkem vůči <a href="http://www.fit.vutbr.cz/CVT/net/Smernice-22-2017.pdf">Pravidlům provozu počítačové sítě VUT [PDF]</a>. Velikost odesílaných a přijímaných dopisů včetně příloh
                je omezena na 32 MB. Při odesílání a příjmu jsou dopisy automaticky kontrolovány na <a href="http://www.fit.vutbr.cz/CVT/net/spam.php.cs">nežádoucí obsah</a> a případně odmítány nebo zahazovány. Pokud dopis odesíláte a obsahuje nebezpečný obsah,
                vrátí se vám s oznámením o odmítnutí nebo odstranění přílohy. Oznámení o zahozených zavirovaných dopisech nejsou zasílány, protože takových chodí při virových záplavách tisíce denně.
                Přehled nedoručených dopisů a odstraněných příloh najdete v informačním systému FIT.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Omezení příjmu elektronické pošty</h2>
            <p>
                Velikost poštovní schránky není striktně omezena, ale z důvodu automatické archivace je doporučeno udržovat velikost vstupní schránky (INBOX) do limitu 1 GB.
                Proto je třeba dopisy z poštovní schánky zavčas vybírat, mazat, případně přesunovat do jiné schránky v domácím adresáři.
            </p>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Parametry pro nastavení IMAP4 klienta</h2>
            <table>
                <tr>
                    <th><strong>Parametr</strong></th>
                    <th><strong>Nastavení</strong></th>
                    <th><strong>Komentář</strong></th>
                </tr>
                <tr>
                    <td><strong>Odesílací adresa</strong></td>
                    <td>login@stud.fit.vutbr.cz</td>
                    <td>Pro odesílání pošty používejte zásadně vždy oficiální adresu.</td>
                </tr>
                <tr>
                    <td><strong>SMTP server</strong></td>
                    <td>smtp.stud.fit.vutbr.cz</td>
                    <td>SMTP server pro odchozí poštu</td>
                </tr>
                <tr>
                    <td><strong>SMTP port </strong></td>
                    <td>25</td>
                    <td>Lze použít v nouzi, někteří poskytovatelé Internetu jej blokují.</td>
                </tr>
                <tr>
                    <td><strong>SMTP port</strong></td>
                    <td>587</td>
                    <td>Port 587 je určen pro odesílání pošty z klientských programů, vyžaduje autentizaci (tj. login a heslo na Unix). Protože je heslo zasíláno v otevřené podobě, je třeba současně použít šifrovaný protokol SSL. Bohužel Outlook neumí na jiném portu než 25 spustit SSL příkazem STARTTLS, takže pro Outlook musíte použít port 465.
                    </td>
                </tr>
                <tr>
                    <td><strong>SMTP přes SSL</strong></td>
                    <td>anp</td>
                    <td>Při autentizaci SMTP jde heslo v otevřené podobě, použijte proto vždy šifrované spojení.
                    </td>
                </tr>
                <tr>
                    <td><strong>Autorizace</strong></td>
                    <td>ano</td>
                    <td>Pro odesílání pošty z klientů připojujících se mimo síť FIT je vyžadována autentizace (tj. login a heslo na Unix - pozor, pouze login, nikoli celá adresa). U Outlooku nezaškrtávejte SPA (Secure Password Authentication), není podporováno.
                    </td>
                </tr>
                <tr>
                    <td><strong>IMAP server</strong></td>
                    <td>imap.stud.fit.vutbr.cz</td>
                    <td>IMAP server pro čtení</td>
                </tr>
                <tr>
                    <td><strong>IMAP port</strong></td>
                    <td>993</td>
                    <td>Port pro protokol IMAP šifrovaným spojením SSL. Protože je heslo zasíláno protokolem IMAP otevřeně, není podporován vnější přístup na nešifrovaný port IMAP (143).</td>
                </tr>
                <tr>
                    <td><strong>IMAP přes SSL</strong></td>
                    <td>ano</td>
                    <td>Vzdálený přístup je povolen jen šifrovaným protokolem.</td>
                </tr>
            </table>
            <table>
                <tr>
                    <th>Studenti doktorského studia, i-účet</th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td><strong>Odesílací adresa</strong></td>
                    <td>login@fit.vutbr.cz</td>
                    <td>liší se pouze tyto tři parametry, jinak platí výše uvedené</td>
                </tr>
                <tr>
                    <td>SMTP server</td>
                    <td>smtp.fit.vutbr.cz</td>
                    <td></td>
                </tr>
                <tr>
                    <td>IMAP server/td>
                    <td>imap.fit.vutbr.cz</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="b-detail">
        <div class="grid__cell size--t-12-12 holder holder--lg b-detail__head">
            <h2 class="">Blokování schránky</h2>
            <p>
                Přístup ke schránce je zablokován, kdykoliv je zablokován účet, např. při ukončení studia na FIT. Po případném odblokování účtu je schránka opět přístupná.<br />
                Velikost schránky není omezená. Doporučujeme ale, aby velikost příchozí pošty nepřesáhla 1GB. Dopisy, které chcete uchovat, můžete přesunout do jiných schránek na serveru.
                Podobně doporučujeme udržovat v rozumných mezích schránku s odeslanou poštou. Důvodem je zátěž zálohovacího serveru, tyto schránky se velmi často mění, je tedy nutné je zálohovat
                i při rozdílových zálohách, které pak jsou zbytečně velké.
            </p>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
