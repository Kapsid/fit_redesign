<?php
/**
 * Created by PhpStorm.
 * User: urban
 * Date: 29.05.2018
 * Time: 22:13
 */

class PredmetyModel
{

	protected $file;

	protected $subjects;

	protected $values;

	public function __construct($file)
   {
		$this->file = $file;
   }

   public function getAllSubjects(){
   	 $subjectsArray = [];

   	 return new SubjectsCollection($subjectsArray);
   }

	public function set($key, $value) {
		$this->values[$key] = $value;
	}

	public function output() {
		if (!file_exists($this->file)) {
			return "Error loading template file ($this->file).";
		}
		$output = file_get_contents($this->file);

		foreach ($this->values as $key => $value) {
			$tagToReplace = "[@$key]";
			$output = str_replace($tagToReplace, $value, $output);
		}

		return $output;
	}
}