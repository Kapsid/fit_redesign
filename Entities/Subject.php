<?php
/**
 * Created by PhpStorm.
 * User: urban
 * Date: 30.05.2018
 * Time: 21:01
 */

class Subject
{

	public $name;

	public $semester;

	public $teacher;

	public $studyType;

	/**
	 * Subject constructor.
	 * @param $subject
	 */
	public function __construct($subject)
	{
		$this->name = $subject["name"];
		$this->semester = $subject["semester"];
		$this->teacher = $subject["teacher"];
		$this->studyType = $subject["studyType"];
	}
}
