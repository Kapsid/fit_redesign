<?php
/**
 * Created by PhpStorm.
 * User: urban
 * Date: 29.05.2018
 * Time: 22:17
 */

public class SubjectsCollection
{
	public $subjects;

	private $subjectsArray;

	public function __construct($subjectsArray)
	{
		$this->subjects = [];
		$this->subjectsArray = $this->getSubjects($subjectsArray);
	}

	public function getSubjects($subjectsArray) : array{
		foreach($subjectsArray as $subject){
			array_push($subjectsArray, new Subject($subject));
		}
	}
}