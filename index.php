<?php
include 'header.php';
?>
    <nav id="m-breadcrumb" class="m-breadcrumb header__breadcrumb" aria-label="Drobečková navigace" role="navigation">
        <a href="#" class="m-breadcrumb__nav m-breadcrumb__nav--prev">
						<span class="icon-svg icon-svg--angle-l m-breadcrumb__nav-icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-l" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

        </a>

        <div class="m-breadcrumb__wrap">
            <ol class="m-breadcrumb__list">
                <li class="m-breadcrumb__item">
                    <a href="#" class="m-breadcrumb__link">Úvod</a>
                </li>
                <li class="m-breadcrumb__item">
                        <span class="icon-svg icon-svg--angle-r m-breadcrumb__separator">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                    <a href="#" class="m-breadcrumb__link" aria-current="page">O fakultě</a>
                </li>
                <li class="m-breadcrumb__item">
                        <span class="icon-svg icon-svg--angle-r m-breadcrumb__separator">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                    <a href="#" class="m-breadcrumb__link" aria-current="page"><strong>Studijní oddělení</strong></a>
                </li>
            </ol>
        </div>

        <a href="#" class="m-breadcrumb__nav m-breadcrumb__nav--next">
						<span class="icon-svg icon-svg--angle-r m-breadcrumb__nav-icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

        </a>
    </nav>
    </header>

    <main id="main" class="main" role="main">
        <div class="b-intro b-intro--pattern holder holder--lg b-hero">
            <div class="b-hero__content holder holder--lg">
                <div class="b-hero__inner">
                    <h1 class="title title--fit" style="text-align: left;">
                        <span class="title__item">Fakulta</span><span class="title__item">informačních</span><br><span
                                class="title__item">technologií</span></span>
                    </h1>
                    <a href="#" class="playlink b-hero__playlink hide--m mt40">
                        <span class="vhide">Přehrát</span>
                    </a>
                </div>
            </div>

            <div class="b-hero__img">
                <img src="../img/illust/b-hero--02.jpg" width="720" height="540" alt="">
                <a href="#" class="playlink b-hero__playlink hide--t hide--d">
                    <span class="vhide">Přehrát</span>
                </a>
            </div>
        </div>
        <a href="#" class="b-cta b-cta--img pt10 pb10">
            <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--02.jpg');">
                <img src="../img/illust/b-hero-header--02.jpg" width="1440" height="540" alt="">
            </div>
            <div class="b-cta__content holder holder--lg">
                <h2 class="b-cta__title h1 mb30" style="text-align: left;">Kde se minulost potkává s budoucností</h2>
                <p class="mb0 " style="text-align: justify;">
                    Kampus naší fakulty je unikátním spojením citlivě rekonstruované historické budovy a nových
                    moderních staveb.
                    Samozřejmostí jsou nejen špičkově vybavené posluchárny a laboratoře s nejmodernější technikou, ale i
                    zázemí pro stravování
                    , relaxaci a odpočinek. Abys to neměl do školy daleko, tak do 10 minut pěšky jsi na Mánesových
                    kolejích nebo na Purkyňkách.
                    Vše na jednom místě!
                </p>
            </div>
        </a>
        <div class="b-photo-stripe ">
            <ul class="b-photo-stripe__list grid grid--0">
                <li class="b-photo-stripe__item grid__cell size--t-6-12">
                    <img src="../img/illust/b-photo-stripe--lg--01.jpg" width="720" height="360" alt="">
                </li>
                <li class="b-photo-stripe__item grid__cell size--s-6-12 size--t-3-12">
                    <img src="../img/illust/b-photo-stripe--sm--04.jpg" width="360" height="360" alt="">
                </li>
                <li class="b-photo-stripe__item grid__cell size--s-6-12 size--t-3-12">
                    <img src="../img/illust/b-photo-stripe--sm--05.jpg" width="360" height="360" alt="">
                </li>
            </ul>
        </div>
        <div class="b-feature ">
            <div class="b-feature__content holder holder--lg">
                <h2 class="b-feature__title h1">
                    Firmy se o tebe pobijou
                </h2>
                <p class="fz-lg">
                    Jako absolventi naší fakulty budete na trhu práce žádaní a nebudete mít problémy s nalezením
                    nadprůměrně placeného zaměstnání.
                    Součástí každého diplomu, který od nás obdržíte, je i mezinárodně platný dodatek, který potvrzuje,
                    co všechno jste se u nás naučili.
                    Dodatky k diplomu vydané na VUT splňují všechny formální i obsahové požadavky Evropské komise, což
                    dokládá Certifikát Diploma Supplement Label.
                </p>
            </div>

            <div class="b-feature__img holder holder--lg ">
            </div>
        </div>
        <div class="c-highlights border-t border-b">
            <ul class="c-highlights__list grid grid--bd grid--0">
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">
                            33.000 Kč</p>
                        <div class="b-highlight__text">
                            <p>je průměrný nástupný plat absolventů</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">98,8 %</p>
                        <div class="b-highlight__text">
                            <p>absolventů si najde práci do 2 měsíců</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">68 %</p>
                        <div class="b-highlight__text">
                            <p>absolventů má práci ještě před ukončením studia</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">0,001 %</p>
                        <div class="b-highlight__text">
                            <p>absolventů pracuje ve fastfoodovém průmyslu</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="c-articles-lg ">
            <div class="holder holder--lg">
                <h2 class="c-articles-lg__title">Nebo si práci sám vymyslíš.</h2>
            </div>
            <div class="c-articles-lg__wrap border-b">
                <ul class="c-articles-lg__list grid grid--0">
                    <li class="c-articles-lg__item grid__cell size--t-6-12">
                        <article class="b-article-lg holder holder--lg" role="article">
                            <a href="#" class="b-article-lg__link">
                                <div class="b-article-lg__img">
                                    <div class="b-article-lg__img-bg"
                                         style="background-image: url('/img/illust/b-article-lg--03.jpg');"></div>
                                </div>
                                <div class="b-article-lg__head">
                                    <time class="b-article-lg__date date font-secondary" datetime="2017-02-01">
                                        <span class="date__day">1</span>
                                        <span class="date__month">únor</span>
                                        <span class="date__year">2017</span>
                                    </time>
                                    <h3 class="b-article-lg__title">Miliony cestovatelů si pohodlněji plánují dovolenou
                                        díky absolventovi VUT</h3>
                                </div>
                                <div class="b-article-lg__content">
                                    <p>
                                        Díky malé rebélii a velké vytrvalosti jediného zaměstnance zřídila před necelým
                                        rokem úspěšná česká firma STRV pobočku v Brně...
                                    </p>
                                </div>
                            </a>
                        </article>
                    </li>
                    <li class="c-articles-lg__item grid__cell size--t-6-12">
                        <article class="b-article-lg holder holder--lg" role="article">
                            <a href="#" class="b-article-lg__link">
                                <div class="b-article-lg__img">
                                    <div class="b-article-lg__img-bg"
                                         style="background-image: url('/img/illust/b-article-lg--03.jpg');"></div>
                                </div>
                                <div class="b-article-lg__head">
                                    <time class="b-article-lg__date date font-secondary" datetime="2017-02-01">
                                        <span class="date__day">1</span>
                                        <span class="date__month">únor</span>
                                        <span class="date__year">2017</span>
                                    </time>
                                    <h3 class="b-article-lg__title">Miliony cestovatelů si pohodlněji plánují dovolenou
                                        díky absolventovi VUT</h3>
                                </div>
                                <div class="b-article-lg__content">
                                    <p>
                                        Díky malé rebélii a velké vytrvalosti jediného zaměstnance zřídila před necelým
                                        rokem úspěšná česká firma STRV pobočku v Brně...
                                    </p>
                                </div>
                            </a>
                        </article>
                    </li>
                </ul>
            </div>
        </div>
        <a href="#" class="b-cta b-cta--img pt10 pb10">
            <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--02.jpg');">
                <img src="../img/illust/b-hero-header--01.jpg" width="1440" height="540" alt="">
            </div>
            <div class="b-cta__content holder holder--lg">
                <h2 class="b-cta__title h1 mb30" style="text-align: left;">Ale ať nemluvíme jen o škole a práci</h2>
                <p class="mb0 " style="text-align: justify;">
                    Čeká tě ZÁBAVA, neoddělitelná součást studentského života. Cestování, kultura, sport, večírky na
                    kolejích i jinde, Majáles a mnoho dalších nezapomenutelných akcí,
                    na které se můžeš těšit! Čeká tě ZÁBAVA, neoddělitelná součást studentského života. Cestování,
                    kultura, sport, večírky na kolejích i jinde,
                    Majáles a mnoho dalších nezapomenutelných akcí, na které se můžeš těšit!
                </p>
            </div>
            <div class="b-photo-stripe ">
                <ul class="b-photo-stripe__list grid grid--0">
                    <li class="b-photo-stripe__item grid__cell size--t-6-12">
                        <img src="../img/illust/b-photo-stripe--lg--16.jpg" width="720" height="360" alt="">
                    </li>
                    <li class="b-photo-stripe__item grid__cell size--s-6-12 size--t-3-12">
                        <img src="../img/illust/b-photo-stripe--sm--26.jpg" width="360" height="360" alt="">
                    </li>
                    <li class="b-photo-stripe__item grid__cell size--s-6-12 size--t-3-12">
                        <img src="../img/illust/b-photo-stripe--sm--27.jpg" width="360" height="360" alt="">
                    </li>
                </ul>
            </div>
        </a>
        <a href="#" class="b-cta b-cta--img pt10 pb10">
            <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--02.jpg');">
                <img src="../img/illust/b-cta--xl--04.jpg" width="1440" height="540" alt="">
            </div>
            <div class="b-cta__content holder holder--lg">
                <h2 class="b-cta__title h1 mb30" style="text-align: left;">Brno!</h2>
                <p class="mb0 " style="text-align: justify;">
                    Druhé největší město v České republice. Je městem studentů! 20% obyvatel tvoří vysokoškolští
                    studenti.
                    Nabízí bezpočet kulturních i sportovních příležitostí. Je městem s bohatou historií i centrem vědy a
                    výzkumu.
                    Nabízí ideální výchozí pozici pro cestování po České republice a Slovensku, ale i po celé Evropě.
                    Dýchne na tebe svou
                    jedinečnou atmosférou. Dokonce je městem s nevyšší kvalitou života v ČR. <br>Brno si zamiluješ!
                </p>
            </div>
        </a>

        <a href="#" class="b-cta b-cta--img">
            <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--02.jpg');">
                <img src="../img/illust/b-cta--xl--08.jpg" width="1440" height="540" alt="">
            </div>
            <div class="b-cta__content holder holder--lg">
                <h2 class="b-cta__title h1 mb30">Přidej se k ajťákům a společně změníme svět!</h2>
                <p class="mb0">
						<span class="btn btn--sm btn--outline btn--white">
							<span class="btn__text">Nabídka oborů</span>
						</span>
                </p>
            </div>
        </a>
    </main>

    <footer class="footer" role="contentinfo">
        <div class="footer__group pt30--m pb30--m pt40--t pb40--t pt60 pb60 holder holder--lg">
            <form action="?" class="f-newsletter">
                <div class="grid grid--middle">
                    <div class="grid__cell size--6-12">
                        <h2 class="f-newsletter__title h3">
                            <label for="f-newsletter__email">
                                Buď v obraze! Dostávej aktuality z FIT přímo na mail a už nic nepropásni!
                            </label>
                        </h2>
                    </div>

                    <div class="grid__cell size--6-12">
                        <p class="inp inp--group mb0">
									<span class="inp__fix">
										<label for="f-newsletter__email" class="inp__label inp__label--inside">Tvoje nejlepší e-mailová adresa</label>
										<input type="text" class="inp__text" id="f-newsletter__email"
                                               placeholder="Tvoje nejlepší e-mailová adresa">
									</span>
                            <span class="inp__btn">
										<button class="btn btn--secondary btn--block--m" type="submit">
											<span class="btn__text">Chci aktuality do mailu</span>
										</button>
									</span>
                        </p>
                    </div>
                </div>
            </form>
        </div>

<?php
include 'footer.php'
?>