<?php
include '../header.php';

$actualities = [
    ['day' => '1', 'month' => 'leden', 'year' => '2017', 'heading' => 'Aktualita 1', 'title' => 'Titulek novinky', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""],
    ['day' => '2', 'month' => 'duben', 'year' => '2018', 'heading' => 'Aktualita 2', 'title' => 'Titulek novinky', 'short-text' => 'Kratky popisek...', 'link' => '#', "image" => ""],
]
?>
    <nav id="m-breadcrumb" class="m-breadcrumb header__breadcrumb" aria-label="Drobečková navigace" role="navigation">
        <a href="#" class="m-breadcrumb__nav m-breadcrumb__nav--prev">
						<span class="icon-svg icon-svg--angle-l m-breadcrumb__nav-icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-l" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

        </a>

        <div class="m-breadcrumb__wrap">
            <ol class="m-breadcrumb__list">
                <li class="m-breadcrumb__item">
                    <a href="#" class="m-breadcrumb__link">Úvod</a>
                </li>
                <li class="m-breadcrumb__item">
                        <span class="icon-svg icon-svg--angle-r m-breadcrumb__separator">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                    <a href="#" class="m-breadcrumb__link" aria-current="page">O fakultě</a>
                </li>
                <li class="m-breadcrumb__item">
                        <span class="icon-svg icon-svg--angle-r m-breadcrumb__separator">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                    <a href="#" class="m-breadcrumb__link" aria-current="page"><strong>Studijní oddělení</strong></a>
                </li>
            </ol>
        </div>

        <a href="#" class="m-breadcrumb__nav m-breadcrumb__nav--next">
						<span class="icon-svg icon-svg--angle-r m-breadcrumb__nav-icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

        </a>
    </nav>
    </header>

    <main id="main" class="main" role="main">
        <a href="#" class="b-cta b-cta--img pt10 pb10">
            <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--02.jpg');">
                <img src="../img/illust/b-hero-header--02.jpg" width="1440" height="540" alt="">
            </div>
            <div class="b-cta__content holder holder--lg">
                <h2 class="b-cta__title h1 mb30" style="text-align: left;">Když se minulost potkává s budoucností</h2>
                <p>
                    Jaké to je učit se mezi zdmi kláštera ze 14. století o technologiích budoucnosti?
                    Kampus FIT je unikátním spojením citlivě rekonstruovaného historického areálu a nových moderních
                    staveb.
                    Kromě špičkově vybavených poslucháren a laboratoří s nejmodernější technikou tu najdete i menzu,
                    knihovnu nebo třeba studentský klub.
                    Daleko to není do centra ani na koleje - na Purkyňkách nebo Mánesových kolejích jste za 10 minut
                </p>
            </div>
        </a>
        <div class="b-photo-stripe ">
            <ul class="b-photo-stripe__list grid grid--0">
                <li class=" b-photo-stripe__item grid__cell size--t-6-12">
                    <img class="line-img" src="../img/illust/b-photo-stripe--lg--01.jpg" alt="">
                </li>
                <li class=" b-photo-stripe__item grid__cell size--s-6-12">
                    <img class="line-img" src="../img/studujtenafit/pic3.jpg" alt="">
                </li>
            </ul>
        </div>
        <div class="b-feature ">
            <div class="b-feature__content holder holder--lg">
                <h2 class="b-feature__title h1">
                    Blízko praxi
                </h2>
                <p class="fz-lg">
                    Víme, jak důležité je provázání s praxí. Proto spolupracujeme s klíčovými podniky v oboru. Studenti
                    tak mají možnost pracovat na průmyslových
                    tématech už v rámci školních projektů. V průběhu studia navíc můžete získat řadu certifikátů, které
                    vám v praxi pomůžou.
                    Získat pak nadprůměrné placené zaměstnání je snadné!
                </p>
            </div>

            <div class="b-feature__img holder holder--lg ">
            </div>
        </div>
        <div class="c-highlights border-t border-b">
            <ul class="c-highlights__list grid grid--bd grid--0">
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">
                            34.907 Kč</p>
                        <div class="b-highlight__text">
                            <p>je průměrný nástupní plat absolventa</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">96 %</p>
                        <div class="b-highlight__text">
                            <p>absolventů si najde práci do 3 měsíců od ukončení školy</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">87 %</p>
                        <div class="b-highlight__text">
                            <p>absolventů by se opět rozhodlo pro studium na FIT</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight holder">
                        <p class="b-highlight__title font-secondary">0,001 %</p>
                        <div class="b-highlight__text">
                            <p>absolventů pracuje ve fastfoodovém průmyslu</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="c-articles-lg ">
            <div class="logos">
                <ul class="grid grid grid--1">
                    <li class="helper grid__cell size--t-2-12 size--3-12">
                        <img src="../img/logos/avast.png" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12">
                        <img class="helper-img" src="../img/logos/cadwork.png" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/camea.png" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/Cesnet.gif" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/codasip.png" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/cz.nic.png" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/Honeywell_Primary_Logo_RGB.jpg" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/PHONEXIA.png" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/RED HAT.png" alt="">
                    </li>
                    <li class="helper grid__cell size--t-2-12 border-t--t">
                        <img src="../img/logos/VR Group.png" alt="">
                    </li>
                </ul>
            </div>
        </div>
        <div class="c-articles-lg ">
            <div class="holder holder--lg">
                <h2 class="c-articles-lg__title">FIT aktuálně</h2>
            </div>
            <div class="c-articles-lg__wrap border-b">
                <ul class="c-articles-lg__list grid grid--0">
                    <?php foreach ($actualities as $actuality) {
                        echo "<li class=\"c-articles-lg__item grid__cell size--t-6-12\">
						<article class=\"b-article-lg holder holder--lg\" role=\"article\">
							<a href=\"{$actuality['link']}\" class=\"b-article-lg__link\">
								<div class=\"b-article-lg__img\">
									<div class=\"b-article-lg__img-bg\" style=\"background-image: url({$actuality['image']});\"></div>
								</div>
								<div class=\"b-article-lg__head\">
									<time class=\"b-article-lg__date date font-secondary\" datetime=\"2017-02-01\">
										<span class=\"date__day\">{$actuality['day']}</span>
										<span class=\"date__month\">{$actuality['month']}</span>
										<span class=\"date__year\">{$actuality['year']}</span>
									</time>
									<h3 class=\"b-article-lg__title\">{$actuality['title']}</h3>
								</div>
								<div class=\"b-article-lg__content\">
									<p>
										{$actuality['short-text']}
									</p>
								</div>
							</a>
						</article>
					</li>";
                    } ?>
                </ul>
            </div>
        </div>
        <div class="b-cta holder holder--lg">
            <h2 class="b-cta__title h1 mb30" style="text-align: left;">Škola, která žije</h2>
            <p class="mb0 " style="text-align: justify;">
                Studentské konference, přednášky předních světových odborníků ale třeba i studentský klub nebo festival
                kapel. Na FIT se zkrátka nudit nebudete.
            </p>
        </div>
        <div class="holder holder--lg">
            <div class="sg-box">
                <div class="sg-box__item">
                    <div class="sg-box__item-code">
                        <div class="js-gallery">
                            <div class="c-gallery">
                                <ul class="c-gallery__list grid grid--10">
                                    <li class="c-gallery__item grid__cell size--t-6-12">
                                        <a href="../img/studujtenafit/gallery1.jpg" data-gallery
                                           class="c-gallery__link">
                                            <div class="c-gallery__img"
                                                 style="background-image: url('/img/illust/c-gallery--01.jpg');">
                                                <img src="../img/studujtenafit/gallery1.jpg" width="280" height="190"
                                                     alt="">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="c-gallery__item grid__cell size--t-6-12">
                                        <a href="../img/studujtenafit/gallery2.jpg" data-gallery
                                           class="c-gallery__link">
                                            <div class="c-gallery__img"
                                                 style="background-image: url('/img/illust/c-gallery--01.jpg');">
                                                <img src="../img/studujtenafit/gallery2.jpg" width="280" height="190"
                                                     alt="">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="c-gallery__item grid__cell size--t-6-12">
                                        <a href="../img/studujtenafit/gallery3.jpg" data-gallery
                                           class="c-gallery__link">
                                            <div class="c-gallery__img"
                                                 style="background-image: url('/img/illust/c-gallery--01.jpg');">
                                                <img src="../img/studujtenafit/gallery3.jpg" width="280" height="190"
                                                     alt="">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="c-gallery__item grid__cell size--t-6-12">
                                        <a href="../img/studujtenafit/gallery4.jpg" data-gallery
                                           class="c-gallery__link">
                                            <div class="c-gallery__img"
                                                 style="background-image: url('/img/illust/c-gallery--01.jpg');">
                                                <img src="../img/studujtenafit/gallery4.jpg" width="280" height="190"
                                                     alt="">
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="b-cta b-cta--img pt10 pb10">
            <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--02.jpg');">
                <img src="../img/illust/b-cta--xl--04.jpg" width="1440" height="540" alt="">
            </div>
            <div class="b-cta__content holder holder--lg">
                <h2 class="b-cta__title h1 mb30" style="text-align: left;">Brno!</h2>
                <p>
                    Brno je centrem vědy a výzkumu, ale hlavně městem se skvělou atmosférou.
                    Tu spoluvytváří také více než 80 tisíc studentů brněnských univerzit. Bary, restaurace, kavárny,
                    koncerty či festivaly, v Brně se zkrátka nudit nebudete!
                </p>
            </div>
        </a>

        <div class="b-cta holder holder--lg">
            <h2 class="b-cta__title h1 mb30" style="text-align: left;">Mezinárodní spolupráce</h2>
            <p class="mb0 " style="text-align: justify;">
                Získat zkušenosti ze zahraničí je nenahraditelná příležitost. Využijte partnerské organizace a řadu
                programů, které FIT nabízí, a prožijte část studia v jiné zemi.
            </p>
        </div>

        <a href="nabidka-oboru.php" class="b-cta b-cta--img">
            <div class="b-cta__img" style="background-image: url('/img/illust/b-cta--xl--02.jpg');">
                <img src="../img/illust/b-cta--xl--08.jpg" width="1440" height="540" alt="">
            </div>
            <div class="b-cta__content holder holder--lg">
                <h2 class="b-cta__title h1 mb30">Staňte se profíkem v IT světě</h2>
                <p class="mb0">
						<span class="btn btn--sm btn--outline btn--white">
							<span class="btn__text">Nabídka oborů</span>
						</span>
                </p>
            </div>
        </a>
    </main>

<?php
include '../footer.php'
?>