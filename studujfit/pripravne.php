<?php
include '../header.php';
?>
    <main style="margin: 105px 0px 0px 0px;">
        <div class="holder holder--lg">
            <div class="sg-box">
                <h1 class="c-attrs__title h2">Přípravné kurzy k přijímací zkoušce</h1>

                <div class="sg-box__item">
                    <div class="sg-box__item-code sg-box__item-code--bleed">
                        <div class="c-attrs holder holder--lg bg">
                            <div class="grid">
                                <div class="grid__cell grid__cell--grow size--t-6-12 mb30--m">
                                    <div class="b-course b-course--shadow">
                                        <h2 class="b-course__title">Matematika</h2>
                                        <div class="b-course__annot">
                                            <p>
                                                Přípravný kurz z matematiky
                                            </p>
                                        </div>
                                        <p class="b-course__btn">
                                            <a href="#" class="btn btn--outline btn--sm">
                                                <span class="btn__text">Ústřední knihovna</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="grid__cell grid__cell--grow size--t-6-12">
                                    <div class="b-course b-course--shadow">
                                        <h2 class="b-course__title">Fyzika</h2>
                                        <div class="b-course__annot">
                                            <p>
                                                Připravný kurz z fyziky.
                                            </p>
                                        </div>
                                        <p class="b-course__btn">
                                            <a href="#" class="btn btn--outline btn--sm">
                                                <span class="btn__text">Ústřední knihovna</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>