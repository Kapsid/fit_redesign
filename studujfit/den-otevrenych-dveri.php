<?php
include '../header.php';
?>
    <main id="main" class="main" role="main">
        <div class="b-intro b-intro--pattern holder holder--lg">
            <p class="mb0"></p>
        </div>

        <div class="grid grid--bd">
            <div class="grid__cell size--t-6-12">
                <div class="holder holder--lg pt40--m pb20--m pt40--t pb20--t pt80 pb40">
                    <h1>Den otevřených dveří</h1>
                    <p class="fz-lg">
                        Zajímá vás studium IT? Chcete zjistit, co se na Fakultě informačních technologií naučíte a kde
                        pak můžete najít uplatnění?
                        Setkat se s vyučujícími nebo si jen prohlédnout unikátní kampus a technologie, které se tu
                        vyvíjí?
                        Přijďte se seznámit s Fakultou informačních technologií!
                    </p>
                    <div class="b-schedule pt25--d">
                        <h2 class="b-schedule__title">Program</h2>
                        <ul class="b-schedule__list">
                            <li class="b-schedule__item">
                                <time class="b-schedule__date font-secondary"
                                      datetime="2017-01-01 10:00/2017-01-01 11:30">
                                    9.15 - 13.15
                                </time>
                                <p>
                                    V rámci Dne otevřených dveří bude ve spolupráci s Mensou ČR probíhat každou hodinu
                                    testování IQ
                                    (10 % nejlepších uchazečů může požádat o přednostní přijetí ke studiu) za
                                    zvýhodněnou cenu 150 Kč.
                                    K testům je možné přihlásit se zde:
                                </p>
                                <p>18.12. 2017 <br/>
                                    9:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8230">http://www.mensa.cz/volny-cas/detail-akce?id_a=8230</a><br/>
                                    10:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8231">http://www.mensa.cz/volny-cas/detail-akce?id_a=8231</a><br/>
                                    11:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8232">http://www.mensa.cz/volny-cas/detail-akce?id_a=8232</a><br/>
                                    12:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8233">http://www.mensa.cz/volny-cas/detail-akce?id_a=8233</a><br/>
                                <p>16.2. 2018 <br/>
                                    9:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8234">http://www.mensa.cz/volny-cas/detail-akce?id_a=8234</a><br/>
                                    10:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8235">http://www.mensa.cz/volny-cas/detail-akce?id_a=8235</a><br/>
                                    11:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8236">http://www.mensa.cz/volny-cas/detail-akce?id_a=8236</a><br/>
                                    12:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8237">http://www.mensa.cz/volny-cas/detail-akce?id_a=8237</a><br/>
                                    13:15 <a href="http://www.mensa.cz/volny-cas/detail-akce?id_a=8238">http://www.mensa.cz/volny-cas/detail-akce?id_a=8238</a><br/>
                                </p>
                            </li>
                            <li class="b-schedule__item">

                                <time class="b-schedule__date font-secondary"
                                      datetime="2017-01-01 10:00/2017-01-01 11:30">
                                    9.30 - 10.30 a 11.30 - 12.30
                                </time>
                                <p>
                                    O tom, jak vypadá studium na Fakultě informačních technologií, se dozvíte na
                                    přednáškách našich vyučujících. Ty začínají vždy v 9:30 a 11:30 v posluchárně D105.
                                </p>
                            </li>
                            <li class="b-schedule__item">

                                <time class="b-schedule__date font-secondary"
                                      datetime="2017-01-01 10:00/2017-01-01 11:30">
                                    9.00 - 13.30
                                </time>
                                <p>
                                    Součástí Dne otevřených dveří je také videoprezentace fakulty v posluchárně D0207,
                                    na prohlídku areálu vás může v pravidelných intervalech vzít Studentská unie
                                </p>
                            </li>
                        </ul>
                    </div>
                    <p>
                        <a href="https://www.facebook.com/events/929881997175875/"
                           class="btn btn--secondary btn--outline btn--sm btn--icon-l">
								<span class="btn__text" style="padding-left: 20px">
									<span class="icon-svg icon-svg--facebook btn__icon">
                                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <use xlink:href="/img/bg/icons-svg.svg#icon-facebook" x="0" y="0" width="100%"
                                             height="100%"></use>
                                    </svg>
                                </span>
								Akce na Facebooku
								</span>
                        </a>
                    </p>
                    <br/><br/>

                    <h2>Jak se k nám dostanete</h2>
                    <p class="fz-lg">
                        Sídlíme kousek od dopravního uzlu Semilasso. Dostat se k nám můžete pohodlně autem, vlakem,
                        tramvají, autobusem i trolejbusem. Pokud jedete z centra, hromadnou dopravou tu budete za cca 15
                        minut
                        (tram 1 a 6, zastávka Semilasso).
                    </p>
                </div>
            </div>

            <div class="grid__cell size--t-6-12 flex-col">
                <div class="grid grid--bd flex-grow">
                    <div class="grid__cell size--6-12">
                        <div class="holder holder--md pt40--m pb20--m pt40--t pb20--t pt80 pb40">
                            <h2>Kde</h2>
                            <p class="fz-md">
                                Fakulta informačních technologií<br/>
                                posluchárenský komplex D<br/>
                                Božetěchova 1, Brno - Královo Pole

                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--6-12 border-t--t">
                        <div class="holder holder--md holder--lg-r pt40--m pb20--m pt40--t pb20--t pt80 pb40">
                            <h2>Kdy</h2>
                            <ul class="fz-md">
                                <li>
                                    v pondělí 18.12. 2017 od 9 do 14 hod.
                                </li>
                                <li>
                                    v pátek 16.2. 2018 od 9 do 14 hod.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="b-gmap">
                    <div class="b-gmap__holder b-gmap__holder--sm"
                         data-address="Fakulta informačních technologií VUT v Brně"></div>
                </div>
            </div>
        </div>

        <div class="b-cta holder holder--lg border-t">
            <p class="mb0">
                <a href="https://www.vutbr.cz/eprihlaska/" class="btn ">
                    <span class="btn__text">Podejte si e-přihlášku</span>
                </a>
            </p>
        </div>
    </main>
<?php
include '../footer.php'
?>