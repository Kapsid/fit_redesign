<?php
include '../header.php';

$bachelorsCourses = [["name" => "Informační technologie", "description" => "popis oboru.."]];

$masterCourses = [["name" => "Informační systémy", "description" => "popis oboru..."],
    ["name" => "Management a informační technologie", "description" => "popis oboru..."],
    ["name" => "Matematické metody v informačních technologiích", "description" => "popis oboru..."],
    ["name" => "Bezpečnost informačních technologií", "description" => "popis oboru..."],
    ["name" => "Bioinformatika a biocomputing", "description" => "popis oboru..."],
    ["name" => "Inteligentní systémy", "description" => "popis oboru..."],
    ["name" => "Počítačové sítě a komunikace", "description" => "popis oboru..."],
    ["name" => "Počítačové a vestavěné systémy", "description" => "popis oboru..."],
    ["name" => "Počítačová grafika a multimédia", "description" => "popis oboru..."]
];

$phdCourses = [["name" => "Výpočetní technika a informatika", "description" => "Ovládni svět vzájemné symbiózy člověka s technológií.", "type" => "daily"],
    ["name" => "Výpočetní technika a informatika", "description" => "Ovládni svět vzájemné symbiózy člověka s technológií.", "type" => "mixed"],
];

?>
<main style="margin: 105px 0px 0px 0px;">
    <div class="holder holder--lg">
        <div class="sg-box">
            <div class="sg-box__item">
                <div class="sg-box__item-code sg-box__item-code--bleed">
                    <div class="c-fields holder holder--lg">
                        <h2 class="c-fields__title h1">Vyber si svůj obor</h2>

                        <nav class="m-tabs m-tabs--lg" role="navigation">
                            <div class="m-tabs__wrap">
                                <ul class="m-tabs__list" role="tablist">
                                    <li class="m-tabs__item">
                                        <a href="#bakalarske-studium2" class="m-tabs__link" role="tab"
                                           aria-controls="bakalarske-studium2" aria-selected="true">
													<span class="m-tabs__inner" style="margin-left:1px;">
														<span class="m-tabs__title">Bakalářské studium <span
                                                                    class="text-defaultcase">(Bc.)</span></span>
														<span class="m-tabs__desc font-primary">Pro středoškoláky, kteří chtějí na vysokou</span>
													</span>
                                        </a>
                                    </li>
                                    <li class="m-tabs__item">
                                        <a href="#magisterske-studium2" class="m-tabs__link" role="tab"
                                           aria-controls="magisterske-studium2">
													<span class="m-tabs__inner">
														<span class="m-tabs__title">Magisterské navazující studium <span
                                                                    class="text-defaultcase">(Ing.)</span></span>
														<span class="m-tabs__desc font-primary">Pro bakaláře, kteří chtějí víc</span>
													</span>
                                        </a>
                                    </li>
                                    <li class="m-tabs__item">
                                        <a href="#doktorske-studium2" class="m-tabs__link" role="tab"
                                           aria-controls="doktorske-studium2">
													<span class="m-tabs__inner" style="margin-right:1px;">
														<span class="m-tabs__title">Doktorské studium <span
                                                                    class="text-defaultcase">(PhD.)</span></span>
														<span class="m-tabs__desc font-primary">Pro magistry a inženýry, kteří chtějí být vědci</span>
													</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                        <div id="bakalarske-studium2" role="tabpanel">
                            <h3 class="vhide">Bakalářské studium</h3>

                            <div class="grid">
                                <div class="grid__cell size--t-12-12 mb20--m">
                                    <h4 class="c-fields__subtitle h2" data-tooltip="#bakalarske-prezencni-studium">
                                        Prezenční studium <a href="#bakalarske-prezencni-studium" class="link-tip">Co to
                                            je?</a></h4>

                                    <div class="c-fields__tooltip tooltip" id="bakalarske-prezencni-studium">
                                        <div class="tooltip__wrap scroll scroll--mobile">
                                            <div class="tooltip__content scroll__content">
                                                <p>
                                                    Prezenční forma studia = dříve denní forma – výuka může probíhat
                                                    kterýkoliv všední den,
                                                    dopoledne i odpoledne, student má povinnost navštěvovat povinné
                                                    semináře, člověk, který studuje VŠ v prezenční formě má status
                                                    studenta
                                                    (a z toho vyplývající výhody).
                                                </p>
                                            </div>
                                        </div>
                                        <a href="#bakalarske-prezencni-studium" class="tooltip__close"
                                           data-tooltip="#bakalarske-prezencni-studium">
													<span class="icon-svg icon-svg--close ">
									<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<use xlink:href="/img/bg/icons-svg.svg#icon-close" x="0" y="0" width="100%"
                                             height="100%"></use>
									</svg>
								</span>

                                            <span class="vhide">Zavřít</span>
                                        </a>
                                    </div>

                                    <ul class="c-fields__list">
                                        <?php foreach ($bachelorsCourses as $bcCourse) {
                                            echo "<li class=\"c-fields__item\">
										<h5 class=\"c-fields__name font-primary h4\">
											<a href=\"detail-oboru.php\">{$bcCourse["name"]}</a>
										</h5>
										{$bcCourse["description"]}
									</li>";
                                        }; ?>

                                    </ul>
                                </div>

                            </div>
                        </div>

                        <div id="magisterske-studium2" role="tabpanel" aria-hidden="true">
                            <h3 class="vhide">Magisterské studium</h3>
                            <div class="grid">
                                <div class="grid__cell size--t-12-12 mb20--m">
                                    <h4 class="c-fields__subtitle h2" data-tooltip="#magisterske-prezencni-studium">
                                        Prezenční studium <a href="#magisterske-prezencni-studium" class="link-tip">Co
                                            to je?</a></h4>

                                    <div class="c-fields__tooltip tooltip" id="magisterske-prezencni-studium">
                                        <div class="tooltip__wrap scroll scroll--mobile">
                                            <div class="tooltip__content scroll__content">
                                                <p>
                                                    Prezenční forma studia = dříve denní forma – výuka může probíhat
                                                    kterýkoliv všední den,
                                                    dopoledne i odpoledne, student má povinnost navštěvovat povinné
                                                    semináře, člověk, který studuje VŠ v prezenční formě má status
                                                    studenta
                                                    (a z toho vyplývající výhody).
                                                </p>
                                            </div>
                                        </div>
                                        <a href="#bakalarske-prezencni-studium" class="tooltip__close"
                                           data-tooltip="#bakalarske-prezencni-studium">
													<span class="icon-svg icon-svg--close ">
									<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<use xlink:href="/img/bg/icons-svg.svg#icon-close" x="0" y="0" width="100%"
                                             height="100%"></use>
									</svg>
								</span>

                                            <span class="vhide">Zavřít</span>
                                        </a>
                                    </div>

                                    <ul class="c-fields__list">
                                        <?php foreach ($masterCourses as $masterCourse) {
                                            echo "<li class=\"c-fields__item\">
										<h5 class=\"c-fields__name font-primary h4\">
											<a href=\"detail-oboru.php\">{$masterCourse["name"]}</a>
										</h5>
										{$masterCourse["description"]}
									</li>";
                                        }; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div id="doktorske-studium2" role="tabpanel" aria-hidden="true">
                            <h3 class="vhide">Doktorské studium</h3>
                            <div class="grid">
                                <div class="grid__cell size--t-6-12 mb20--m">
                                    <h4 class="c-fields__subtitle h2" data-tooltip="#doktorske-prezencni-studium">
                                        Prezenční studium <a href="#doktorske-prezencni-studium" class="link-tip">Co to
                                            je?</a></h4>

                                    <div class="c-fields__tooltip tooltip" id="doktorske-prezencni-studium">
                                        <div class="tooltip__wrap scroll scroll--mobile">
                                            <div class="tooltip__content scroll__content">
                                                <p>
                                                    Prezenční forma studia = dříve denní forma – výuka může probíhat
                                                    kterýkoliv všední den, dopoledne
                                                    i odpoledne, student má povinnost navštěvovat povinné semináře,
                                                    člověk, který studuje VŠ v prezenční formě
                                                    má status studenta (a z toho vyplývající výhody).
                                                </p>
                                            </div>
                                        </div>
                                        <a href="#bakalarske-prezencni-studium" class="tooltip__close"
                                           data-tooltip="#bakalarske-prezencni-studium">
									<span class="icon-svg icon-svg--close ">
									<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<use xlink:href="/img/bg/icons-svg.svg#icon-close" x="0" y="0" width="100%"
                                             height="100%"></use>
									</svg>
								</span>

                                            <span class="vhide">Zavřít</span>
                                        </a>
                                    </div>

                                    <ul class="c-fields__list">
                                        <?php foreach ($phdCourses as $phdCourse) {
                                            if ($phdCourse["type"] === "daily") {
                                                echo "<li class=\"c-fields__item\">
                                                <h5 class=\"c-fields__name font-primary h4\">
                                                    <a href=\"detail-oboru.php\">{$phdCourse["name"]}</a>
                                                </h5>
                                                {$phdCourse["description"]}
                                            </li>";
                                            }
                                        }; ?>
                                    </ul>
                                </div>

                                <div class="grid__cell size--t-6-12">
                                    <h4 class="c-fields__subtitle h2" data-tooltip="#bakalarske-kombinovane-studium">
                                        Kombinované studium <a href="#bakalarske-kombinovane-studium" class="link-tip">Co
                                            to je?</a></h4>

                                    <div class="c-fields__tooltip tooltip" id="bakalarske-kombinovane-studium">
                                        <div class="tooltip__wrap scroll scroll--mobile">
                                            <div class="tooltip__content scroll__content">
                                                <p>
                                                    Kombinované studium je kombinací dálkového a prezenčního
                                                    studia.Největší rozdíl spočívá v tom,
                                                    že výuka v kombinované formě studia probíhá na naší fakultě pouze o
                                                    sobotách, jedenkrát za 14 dnů,
                                                    formou tutoriálů a cvičení. Při sobotní výuce tutoriálů jsou
                                                    probrány pouze nejobtížnější partie látky
                                                    daného předmětu. Kombinovaná forma výuky proto vyžaduje výrazný
                                                    podíl samostatné práce studenta.
                                                    Studenti kombinované formy studia mají většinu práv i povinností
                                                    stejné, jako studenti prezenční formy.
                                                    Nemohou však například obdržet ubytovací stipendium. Poněvadž i
                                                    student kombinované formy studia je řádným studentem
                                                    ve smyslu vysokoškolského zákona, nemusí platit zdravotní pojištění.
                                                    Kombinovaně u nás studují většinou studenti,
                                                    kteří sami podnikají nebo jsou již zaměstnáni v oboru.
                                                </p>
                                            </div>
                                        </div>
                                        <a href="#bakalarske-kombinovane-studium" class="tooltip__close"
                                           data-tooltip="#bakalarske-kombinovane-studium">
									<span class="icon-svg icon-svg--close ">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-close" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                            <span class="vhide">Zavřít</span>
                                        </a>
                                    </div>

                                    <ul class="c-fields__list">
                                        <?php foreach ($phdCourses as $phdCourse) {
                                            if ($phdCourse["type"] === "mixed") {
                                                echo "<li class=\"c-fields__item\">
                                                <h5 class=\"c-fields__name font-primary h4\">
                                                    <a href=\"detail-oboru.php\">{$phdCourse["name"]}</a>
                                                </h5>
                                                {$phdCourse["description"]}
                                            </li>";
                                            }
                                        }; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
include '../footer.php'
?>
