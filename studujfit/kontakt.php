<?php
include '../header.php';
?>
    <main id="main" class="main" role="main">
        <div class="b-intro b-intro--pattern holder holder--lg">
            <p class="mb0">
            </p>
        </div>

        <div class="b-gmap">
            <div class="grid grid--0">
                <div class="grid__cell size--t-7-12 size--9-12">
                    <div class="b-gmap__holder" data-address="Fakulta informačních VUT v Brně"></div>
                </div>

                <div class="grid__cell size--t-5-12 size--3-12">
                    <div class="b-contact b-contact--map b-contact--primary holder holder--md">
                        <h2 class="b-contact__title">Kontaktujte nás</h2>
                        <div class="b-contact__text">
                            <p>
                                <a href="tel:+420541141145" class="link-icon">
                                    tel.: +420 5 4114 1145
                                </a><br>
                                <a href="mailto:studijni@fit.vutbr.cz" class="link-icon">
                                    mail.: studijni@fit.vutbr.cz
                                </a><br>
                                <a href="www.fit.vutbr.cz" class="link-icon">
                                    www.fit.vutbr.cz
                                </a>
                                <a href="https://www.facebook.com/FIT.VUT/?ref=bookmarks" class="link-icon">
                                    fb: https://www.facebook.com/FIT.VUT/?ref=bookmarks
                                </a>
                            </p>
                        </div>
                    </div>

                    <div class="b-contact b-contact--map holder holder--md">
                        <h2 class="b-contact__title">Kde nás najdete</h2>
                        <div class="b-contact__text">
                            <p>
                                <strong>Fakulta informačních technologií</strong><br/>
                                Božetěchova 1/2, Brno 612 66
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>