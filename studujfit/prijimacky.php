<?php
include '../header.php';
?>
    <div class="b-admission holder holder--lg pb60--d">
        <h2 class="b-admission__title h1">Přijímací řízení</h2>
        <form action="?" class="f-subjects b-admission__filter">
            <div class="f-subjects__filter f-subjects__filter--full">
                <p class="inp inp--multiple">
                    <span class="inp__fix minw180">
                        <label for="degree" class="inp__label inp__label--inside">Stupeň studia</label>
                        <select name="degree" id="degree" class="select js-select">
                            <option disabled placeholder>Stupeň studia</option>
                            <option selected>Bakalářský</option>
                        </select>
                    </span>
                    <span class="inp__fix minw440">
                        <label for="field" class="inp__label inp__label--inside">Obor</label>
                        <select name="field" id="field" class="select js-select">
                            <option disabled placeholder>Obor</option>
                            <option selected>Informační technologie</option>
                        </select>
                    </span>
                </p>
            </div>
        </form>
    </div>

    <div class="c-highlights border-t">
        <ul class="c-highlights__list grid grid--bd grid--0">
            <li class="c-highlights__item grid__cell size--t-4-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">1 332</p>
                    <div class="b-highlight__text">
                        <p>Uchazečů v roce 2017</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-4-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">812</p>
                    <div class="b-highlight__text">
                        <p>Přijatých studentů</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-4-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">61 %</p>
                    <div class="b-highlight__text">
                        <p>Úspěšnost studentů</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="c-terms border-t" id="c-terms">
        <a href="#c-terms" class="link-slide c-terms__slide" data-slide="#c-terms">
            <span class="icon-svg icon-svg--angle-d link-slide__icon">
                <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="/img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
                </svg>
            </span>

            <span class="vhide">Pokračovat</span>
        </a>

        <div class="holder holder--lg">
            <h2 class="c-terms__title">Důležité termíny</h2>
        </div>

        <div class="c-terms__wrap">
            <div class="c-terms__list grid grid--t-40 grid--80 js-carousel">
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2017-12-18" class="b-program__date">18. prosince 2017</time>
                            Den otevřených dveří 1
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Zajímá vás studium IT? Chcete zjistit, co se na Fakultě informačních technologií naučíte
                                a kde pak můžeš najít uplatnění? Setkat se s vyučujícími nebo si jen prohlédnout
                                unikátní kampus a technologie, které se tu vyvíjí?
                            </p>
                            <p class="mt25">
                                <a href="den-otevrenych-dveri.php" class="btn btn--sm btn--outline">
                                    <span class="btn__text">Více o DOD</span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-02-16" class="b-program__date">16. února 2018</time>
                            Den otevřených dveří 2
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Zajímá vás studium IT? Chcete zjistit, co se na Fakultě informačních technologií naučíte
                                a kde pak můžeš najít uplatnění? Setkat se s vyučujícími nebo si jen prohlédnout
                                unikátní kampus a technologie, které se tu vyvíjí?
                            </p>
                            <p class="mt25">
                                <a href="den-otevrenych-dveri.php" class="btn btn--sm btn--outline">
                                    <span class="btn__text">Více o DOD</span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-03-31" class="b-program__date">31. března 2018</time>
                            Uzávěrka přihlášek
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Pokud chcete na FIT, musíte si do 31.3. podat a zaplatit přihlášku, nejlépe
                                elektronicky.
                                Pokud jste nesplnil/a podmínky pro přednostní přijetí, přihlaste se na národní
                                srovnávací zkoušky SCIO (OSP, MAT nebo VŠP) a nezapomeňte poskytnout souhlas s předáním
                                svého výsledku FIT VUT - tak se dozvíme o vašem výsledku. </p>
                            <p class="mt25">
                                <a href="#" class="btn btn--sm ">
                                    <span class="btn__text">Podat e-přihlášku</span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-03-31" class="b-program__date">31. března 2018</time>
                            Žádosti o přednostní přijetí
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Splnil/a jste podmínky pro přednostní přijetí? Skvělé! Dejte nám o tom vědět v listinné
                                formě nejpozději do 31.3. (dokument je součástí e-přihlášky, pošlete jej pak na studijní
                                oddělení). Pokud některé výsledky ještě neznáte, např. z maturitní zkoušky, která vás
                                teprve čeká, můžete podat žádost i později - nejpozději a bez zbytečného odkladu 31.5.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-04-28" class="b-program__date">28. dubna 2018</time>
                            SCIO test zdarma
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Národní srovnávací zkoušky SCIO (OSP, MAT nebo VŠP) si můžete vyzkoušet, kolikrát
                                potřebujete - započítáme jen ten nejlepší výsledek. Pokud jste včas podal/a a zaplatil/a
                                přihlášku, FIT vám umožní absolvovat jeden test zdarma - v sobotu 28.4. 2018. Vybrat si
                                můžete jakékoliv místo, SCIO vám vygeneruje a zašle slevový kód. </p>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-05-26" class="b-program__date">26. květen 2018</time>
                            Náhradní termín
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Pokud se nemůžete z vážných důvodů testů národních srovnávacích zkoušek zúčastnit v
                                řádném termínu, musíte do 2.5. předat předsedovi přijímací komise písemnou omluvu. Pokud
                                ji přijme, umožní vám fakulta zdarma absolvovat náhradní test OSP nebo VŠP v sobotu
                                26.5. 2018. </p>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-06-20" class="b-program__date">20. červen 2018</time>
                            Zápis
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Teprve zápisem se stáváte řádným studentem FIT. Pokud se k zápisu nemůžete dostavit
                                osobně, můžete se zapsat prostřednictvím svého zástupce vybaveného plnou mocí.</p>
                            <a href="#" class="btn btn--sm ">
                                <span class="btn__text">Zápis</span>
                            </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-09-15" class="b-program__date">15. září 2018</time>
                            Imatrikulace
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Imatrikulace, která probíhá poslední pátek před zahájením výuky v semestru, je
                                slavnostní akademický obřad přijetí studenta ke studiu. Nenechte si ji ujít! </p></div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-09-15" class="b-program__date">15. - 16. září 2018</time>
                            Start@FIT
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                Bojíte se, že se na FIT nevyznáte? Tahle uvítací akce pro prváky vám pomůže zorientovat
                                se. Setkáte se svými novými spolužáky i učiteli, prohlédnete si fakultu, zjistíte, jak
                                to na FIT chodí, ve studentském klubu si můžete zahrát deskovky nebo si dát pivo a večer
                                zajít na koncert.</p>
                            <a href="https://www.facebook.com/events/249241215559259/" class="btn btn--sm ">
                                <span class="btn__text">Facebook</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="c-terms__item grid__cell size--t-auto size--4-12">
                    <div class="b-program b-program--shadow">
                        <h2 class="b-program__title h3 pt25 pb25">
                            <time datetime="2018-09-15" class="b-program__date">15. září 2018</time>
                            Začátek semestru
                        </h2>
                        <div class="b-program__content pb30">
                            <p>
                                V pondělí 17.9. začíná první vyučovací týden zimního semestru. </p></div>
                    </div>
                </div>
            </div>

            <a href="#" class="arrow arrow--prev is-disabled c-terms__prev">
                    <span class="icon-svg icon-svg--angle-l arrow__icon">
                        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <use xlink:href="/img/bg/icons-svg.svg#icon-angle-l" x="0" y="0" width="100%"
                                 height="100%"></use>
                        </svg>
                    </span>

                <span class="vhide">Předchozí</span>
            </a>

            <a href="#" class="arrow arrow--next c-terms__next">
                <span class="icon-svg icon-svg--angle-r arrow__icon">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%"
                             height="100%"></use>
                    </svg>
                </span>

                <span class="vhide">Následující</span>
            </a>
        </div>
    </div>

    <div class="c-highlights holder holder--lg pt40--m pb40--m pt40--t pb40--t pt55 pb75" data-toggle>
        <h2 class="c-highlights__title mb55--d">Přijímačky dělat nemusíš, když&hellip;</h2>

        <ul class="c-highlights__list c-highlights__list--bd grid grid--bd grid--0">
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                <div class="b-highlight b-highlight--sm holder">
                    <div class="b-highlight__text">
                        <p>jste absolvoval/a mezinárodní zkoušku <strong>SAT v části Mathematics s alespoň 545
                                body</strong> nebo <strong>SAT Subject Test in Mathematics Level 1 s alespoň 665
                                body</strong> nebo
                            <strong>SAT Subject Test Mathematics Level 2 s alespoň 745 body</strong> (doklad nesmí být
                            starší než 2 roky). </p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                <div class="b-highlight b-highlight--sm holder">
                    <div class="b-highlight__text">
                        <p>jste úspěšným řešitelem krajského kola <strong>Matematické olympiády</strong> vyhlašované
                            MŠMT v kategorii A, B, C nebo P nebo <strong>Matematickej olympiády</strong> vyhlašované
                            MŠVVŠ SR v kategorii A, B nebo C (doklad nesmí být starší než 4 roky). </p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                <div class="b-highlight b-highlight--sm holder">
                    <div class="b-highlight__text">
                        <p>Jste úspěšným řešitelem krajského kola <strong>Fyzikální olympiády</strong> vyhlašované MŠMT
                            nebo <strong>Fyzikálnej olympiády</strong> vyhlašované MŠVVŠ SR v kategoriích A, B, C nebo D
                            (doklad nesmí být starší než 4 roky). </p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                <div class="b-highlight b-highlight--sm holder">
                    <div class="b-highlight__text">
                        <p>umístil/a jste se na 1. až 3. místě krajské přehlídky <strong>Středoškolské odborné
                                činnosti</strong> vyhlašované MŠMT nebo Stredoškolskej odbornej činnosti vyhlašované
                            MŠVVŠ SR</p>
                    </div>
                </div>
            </li>
        </ul>

        <div class="c-highlights__more" data-toggleable>
            <ul class="c-highlights__list c-highlights__list--bd grid grid--bd grid--0">
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>postoupil/a jste do národního kola mezinárodní programátorské soutěže <strong>Creative
                                    Baltie</strong> v kategorii D (doklad nesmí být starší než 4 roky). </p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>jste úspěšným řešitelem krajského kola <strong>Olympiády v informatike</strong>
                                vyhlašované MŠVVŠ SR v kategorii A nebo B (doklad nesmí být starší než 4 roky). </p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>jste u maturitní zkoušky z matematiky dosáhl/a percentil 80 nebo více</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>jste u výběrové zkoušky z matematiky (Matematika+) vyhlašované MŠMT dosáhl/a percentil 60
                                nebo více</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>ve standardizovaném vstupním IQ testu prováděném společností Mensa jste dosáhl/a
                                percentil 90 nebo víc</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>jste jako uchazeč v rámci celoživotního vzdělávání na FIT VUT absolvoval/a předměty
                                zařazené do bakalářského studijního programu, které bude možné po přijetí do řádného
                                studia uznat, za alespoň 36 kreditů. Do tohoto limitu lze započítat až 18 kreditů za
                                předměty aktuálně studované v rámci celoživotního vzdělávání na FIT VUT.</p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>Jsi maturoval z <strong>matematiky nebo fyziky</strong> za&nbsp;1&nbsp;nebo&nbsp;2 <span
                                        class="color-primary font-bold">&ast;</span></p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>V <strong>Národních srovnávacích zkouškách</strong> v matematické části jsi dosáhla
                                percentilu 60&nbsp;a&nbsp;více <span class="color-primary font-bold">&ast;</span></p>
                        </div>
                    </div>
                </li>
                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                    <div class="b-highlight b-highlight--sm holder">
                        <div class="b-highlight__text">
                            <p>Máš za sebou <strong>přípravný kurz z matematiky nebo fyziky</strong> za&nbsp;1&nbsp;nebo&nbsp;2
                                <span class="color-primary font-bold">&ast;</span></p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <p class="c-highlights__link-more">
            <a href="#" class="link-more font-secondary js-toggle" data-less="Skrýt">
                <span class="link-more__label">Zobrazit další</span>
                <span class="icon-svg icon-svg--angle-d link-more__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

            </a>
        </p>

        <p class="c-highlights__hint">
            <span class="color-primary font-bold">&ast;</span> Sedí něco z toho na tebe? Super! Až podáš přihlášku, <a
                    href="#" class="font-bold">kontaktuj nás</a>.
        </p>
    </div>

    <div class="grid grid--0 border-t">
        <div class="grid__cell size--t-6-12 border-b">
            <div class="holder holder--lg holder--sm-r pt20 pb20 h100p--t h100p">
                <div class="scroll">
                    <div class="scroll__content pt20--m pb20--m pt20--t pb20--t pt60 pb60">
                        <h2 class="h1">Přijímací zkouška</h2>
                        <p class="fz-lg">
                            Vůbec se neboj, že tě budeme ústně zkoušet. Co víš, to prostě napíšeš! Na oboru
                            Biomedicínská technika a bioinformatika si tě prověříme z matiky a biologie. Ale není se
                            čeho bát. Jsme tady abychom ti pomohli jak jen můžeme.
                        </p>

                        <h3 class="h5 mt35">Testy z loňska</h3>
                        <div class="grid mb35">
                            <div class="grid__cell size--6-12">
                                <ul class="list-files list-files--compact">
                                    <li class="list-files__item">
                                        <a href="#" class="link-file">
													<span class="icon-svg icon-svg--pdf link-file__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-pdf" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                            <strong class="link-file__name">Matematika 2016 — A</strong>
                                        </a>
                                    </li>
                                    <li class="list-files__item">
                                        <a href="#" class="link-file">
													<span class="icon-svg icon-svg--pdf link-file__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-pdf" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                            <strong class="link-file__name">Biologie 2016 — A</strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="grid__cell size--6-12">
                                <ul class="list-files list-files--compact">
                                    <li class="list-files__item">
                                        <a href="#" class="link-file">
													<span class="icon-svg icon-svg--pdf link-file__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-pdf" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                            <strong class="link-file__name">Matematika 2016 — B</strong>
                                        </a>
                                    </li>
                                    <li class="list-files__item">
                                        <a href="#" class="link-file">
													<span class="icon-svg icon-svg--pdf link-file__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-pdf" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                            <strong class="link-file__name">Biologie 2016 — B</strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <h3 class="h5 mb15">Poraď se s ostatními</h3>
                        <p>
                            <a href="#" class="btn btn--sm btn--outline btn--secondary btn--icon-l">
										<span class="btn__text">
											<span class="icon-svg icon-svg--facebook btn__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-facebook" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

											Skupina pro uchazeče
										</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid__cell size--t-6-12">
            <div class="grid grid--0">
                <div class="grid__cell size--6-12">
                    <a href="#" class="b-video">
                        <div class="b-video__img">
                            <img src="/img/illust/b-video--04.jpg" width="360" height="360" alt="">
                        </div>
                        <div class="b-video__content holder holder--md">
                            <p class="b-video__playlink playlink mta">
                                <span class="vhide">Přehrát</span>
                            </p>
                            <h2 class="b-video__title font-primary h4 mta">Jak řešit příklady</h2>
                        </div>
                    </a>
                </div>

                <div class="grid__cell size--6-12">
                    <div class="b-cta holder pt105--d border-l--t">
                        <h2 class="b-cta__title h1 mb30">Přihlaš se on-line</h2>
                        <p class="b-cta__subtitle font-secondary">Do 31. března 2018</p>
                        <p class="mb0">
                            <a href="#" class="btn">
                                <span class="btn__text">Podat e-přihlášku</span>
                            </a>
                        </p>
                    </div>
                </div>

                <div class="grid__cell">
                    <div class="tip tip--lg">
                        <div class="tip__content">
									<span class="icon-svg icon-svg--tip tip__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-tip" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                            <h2 class="tip__title h1">Víš, že&hellip;</h2>
                            <p>
                                &hellip;top 500 maturantů od nás dostane jednorázové stipendium 6000 Kč!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="c-courses holder holder--lg">
        <div class="c-courses__wrap">
            <div class="b-course b-course--shadow">
						<span class="icon-svg icon-svg--preparatory-courses b-course__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-preparatory-courses" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                <h3 class="b-course__title h2">Přípravné kurzy</h3>
                <p class="b-course__subtitle font-secondary">Připrav se na 100 </p>
                <div class="b-course__annot">
                    <p>
                        Jestli si nejsi svými znalostmi v matice a biologii úplně jista, není nic snazšího, než se
                        přihlásit do našich přípravných kurzů. Probereme okruhy, které tě čekají na přijímačkách, a pak
                        už by to měla být brnkačka!
                    </p>
                </div>
                <p class="b-course__btn">
                    <a href="#" class="btn btn--outline btn--sm">
                        <span class="btn__text">Matematika</span>
                    </a>
                    <a href="#" class="btn btn--outline btn--sm">
                        <span class="btn__text">Biologie</span>
                    </a>
                </p>
            </div>
        </div>
    </div>

    <div class="grid grid--0 grid--bd border-t">
        <div class="grid__cell size--t-6-12">
            <div class="b-cta holder holder--lg pt110--d">
                <h2 class="b-cta__title h1 mb30">Přihlaš se on-line</h2>
                <p class="b-cta__subtitle font-secondary">Do 31. března 2018</p>
                <p class="mb0">
                    <a href="#" class="btn ">
                        <span class="btn__text">Podat e-přihlášku</span>
                    </a>
                </p>
            </div>
        </div>
        <div class="grid__cell size--t-6-12">
            <div class="b-cta holder holder--lg pt110--d">
                <h2 class="b-cta__title h1 mb30">Pomůžeme ti s něčím?</h2>
                <p class="b-cta__subtitle font-secondary">Kontaktuj oddělení pro přijímací řízení</p>
                <p class="b-cta__contact mb0">
                    <a href="tel:+420541146346" class="link-icon font-bold">
			<span class="icon-svg icon-svg--phone link-icon__icon color-gray">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                        + 420 541 146 346
                    </a>
                    <a href="mailto:prijimaci-rizeni@feec.vutbr.cz" class="link-icon font-bold">
			<span class="icon-svg icon-svg--message link-icon__icon color-gray">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                        prijimaci-rizeni@feec.vutbr.cz
                    </a>
                </p>
            </div>
        </div>
    </div>

<?php
include '../footer.php'
?>