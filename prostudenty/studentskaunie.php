<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h1 class="b-detail__title">Studentská unie</h1>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        Jsme seskupení studentů zajímajících se o dění na fakultě –chcemestát při rozhodnutích,
                        které ovlivňují studenty, zastupovat je a chránit jejich (a tedy i naše) zájmy.Kromě toho ale
                        chceme také organizovat různé akce, ať už pro odpočinek od školních povinností, zábavu, ale i
                        další vzdělání.
                    </p>
                </div>
            </div>

            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="b-detail__title">Akce</h2>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <h3>Otevírání studentského klubu U Kachničky</h3>
                    <p>
                        Víme, že život studenta je náročný, a proto máme na fakultě prostor, kde si můžete přijít
                        odpočinout od projektů,
                        zahrát si nějakou hru, nebo jen tak zajít s kamarády na pivo.O tento klub se staráme,
                        rozšiřujeme nabídku deskovýchher,
                        ale snažíme se přidávat i jiné aktivity v těchto krásných prostorách.
                    </p>
                    <div class="sg-box__item">
                        <div class="sg-box__item-code">
                            <div class="js-gallery">
                                <div class="c-gallery">
                                    <ul class="c-gallery__list grid grid--10">
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/kachnicka1.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/kachnicka1.jpg');">
                                                    <img src="../img/su/kachnicka1.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/kachnicka2.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/kachnicka2.jpg');">
                                                    <img src="../img/su/kachnicka2.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/kachnicka3.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/kachnicka3.jpg');">
                                                    <img src="../img/su/kachnicka3.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <h3>Rock@FIT a DZD</h3>
                    <p>
                        Máme rádi nejen rock, a proto dáváme prostor kapelám, kde působí studenti FITu, ať předvedou, co
                        je baví.
                        Je to odpoledne plné hudby a zábavy, protože ve stejný den uspořádáme i den zavřených dveří
                        –DZD.Takže mimo jiné můžete debugovat
                        fakultu,prohlédnout si podzemí a soutěžit o různé ceny –nechtese překvapit!
                    </p>

                    <div class="sg-box__item">
                        <div class="sg-box__item-code">
                            <div class="js-gallery">
                                <div class="c-gallery">
                                    <ul class="c-gallery__list grid grid--10">
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/rockfit1.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/rockfit1.jpg');">
                                                    <img src="../img/su/rockfit1.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/rockfit2.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/rockfit2.jpg');">
                                                    <img src="../img/su/rockfit2.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <h3>Deskovky a herní turnaje</h3>
                    <p>
                        Náš studentský klub U Kachničkymá skvělý potenciál pro pořádání různých akcí.
                        Uspořádáme turnaje v deskových, ale i těch počítačovýchhrách.Stále hledámei nadšence s
                        trochou volného času, kteří by pomohli s jejich organizací, takže pokud byste chtěli pomoci,
                        kontaktujte nás
                    </p>
                    <div class="sg-box__item">
                        <div class="sg-box__item-code">
                            <div class="js-gallery">
                                <div class="c-gallery">
                                    <ul class="c-gallery__list grid grid--10">
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/herne-turnaje1.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/herne-turnaje1.jpg');">
                                                    <img src="../img/su/herne-turnaje1.jpg" width="280" height="190"
                                                         alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/herne-turnaje2.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/herne-turnaje2.jpg');">
                                                    <img src="../img/su/herne-turnaje2.jpg" width="280" height="190"
                                                         alt="">
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <h3>Vítání prváků</h3>
                    <p>
                        Každoročně na začátku semestru pomáháme seznamovat se novým studentům se životem na vysoké škole
                        zprostředkováním našich
                        zkušeností.Ale nejsou to jen nudné přednášky, chceme také, aby se prváci poznali navzájem a
                        vyvážila sekvanta nových informací
                        trochou zábavy v našem studentském klubu a dalších aktivitách, které obměňujeme.
                    </p>

                    <div class="sg-box__item">
                        <div class="sg-box__item-code">
                            <div class="js-gallery">
                                <div class="c-gallery">
                                    <ul class="c-gallery__list grid grid--10">
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/vitani-prvaku1.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/vitani-prvaku1.jpg');">
                                                    <img src="../img/su/vitani-prvaku1.jpg" width="280" height="190"
                                                         alt="">
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <h3>Plesy</h3>
                    <p>
                        Ať už byste přišli na náš Studentský ples FIT, Reprezentační ples FIT a FEKTnebo i Ples VUT,
                        v každém z nich máme prsty.Sami organizujeme první jmenovaný, který bývá koncem února.
                        Je to jedinečná příležitost zažít příjemný večer s přáteli, zatancovat si a oprášit obleky a
                        šaty.
                    </p>

                    <div class="sg-box__item">
                        <div class="sg-box__item-code">
                            <div class="js-gallery">
                                <div class="c-gallery">
                                    <ul class="c-gallery__list grid grid--10">
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/ples1.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/ples1.jpg');">
                                                    <img src="../img/su/ples1.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/ples2.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/ples2.jpg');">
                                                    <img src="../img/su/ples2.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/ples3.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/ples3.jpg');">
                                                    <img src="../img/su/ples3.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/ples4.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/ples4.jpg');">
                                                    <img src="../img/su/ples4.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <h3>Sit sit</h3>
                    <p>
                        Večer plný jídla, zpěvu a připíjenísi.Tento koncept je rozšířen zejména v severských zemích,
                        ale nám se zalíbil tak, že jsme se ho rozhodli přinést k nám.Vše podstatné se dozvíte na tomto
                        odkazu: <a
                                href="https://en.wikipedia.org/wiki/Sittning">https://en.wikipedia.org/wiki/Sittning</a>.
                        Aby vám to neušlo, sledujte náš Facebook!
                    </p>

                    <div class="sg-box__item">
                        <div class="sg-box__item-code">
                            <div class="js-gallery">
                                <div class="c-gallery">
                                    <ul class="c-gallery__list grid grid--10">
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/sitsit1.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/sitsit1.jpg');">
                                                    <img src="../img/su/sitsit1.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/sitsit2.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/sitsit2.jpg');">
                                                    <img src="../img/su/sitsit2.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/sitsit3.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/sitsit3.jpg');">
                                                    <img src="../img/su/sitsit3.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                        <li class="c-gallery__item grid__cell size--t-4-12">
                                            <a href="../img/su/sitsit4.jpg" data-gallery class="c-gallery__link">
                                                <div class="c-gallery__img"
                                                     style="background-image: url('../img/su/sitsit4.jpg');">
                                                    <img src="../img/su/sitsit4.jpg" width="280" height="190" alt="">
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
include '../footer.php';
?>
