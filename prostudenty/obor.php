<?php
include '../header.php';

$spec_type = $_GET['spec_type'];

$spec_name = $_GET['spec_name'];

$thesis_examples = ['Využití grafického procesoru jako akcelerátoru - technologie OpenCL',
                            'Detekce a klasifikace vojenských cílů ve videosignálu',
                            'Detekce a rozpoznání dopravních značek v obraze',
                            'Přizpůsobení platformy LLVM pro mikroprocesor Motorola 68000',
                            'Analýza síťového provozu pomocí zařízení NIFIC',
                            'Rozšíření knihovny OpenSceneGraph pro zobrazení 3D obrazových dat v medicíně',
                            'Rozpoznávání objektů pomocí neuronových sítí',
                            'Nástroje na podporu administrace a ladění výkonnosti databázového serveru Oracle 11g',
                            'Modelování dopravy s využitím celulárních automatů',
                            'Programátor pro ICSP rozhraní mikrokontrolerů Pic'];

$profile_graduation = ['Absolvent bakalářského studijního oboru Informační technologie má základní teoretické znalosti z oblasti technických i programových prostředků.',
    'Je kvalifikovaný a adaptabilní odborník schopný se přizpůsobit konkrétním podmínkám na pracovišti.',
    'V praxi se může uplatnit jako projektant, konstruktér, a údržbář počítačových systémů a číslicových zařízení, jako technik pro instalace číslicových systémů a
                                počítačových sítí, jako programátor aplikací a správce programů, databázových a informačních systémů a správce počítačových sítí nebo středoškolský učitel
                                informačních technologií a jako kvalifikovaný podnikatel v oboru výpočetní techniky.'];

$years = ['2018/2019','2017/2018','2016/2017'];

$spec = [
        'program_code' => 'IT-BC-3',
        'shortcut' => 'BIT',
        'language' => 'čeština',
        'level' => 'bakalářský',
        'study_form' => 'prezenční',
        'year_cnt' => 3,
        'text_about' => 'Tento obor je jediným bakalářským oborem programu Informační technologie. Studijní obor se řídí společnými zásadami, podmínkami a kriterii,
                        která jsou podrobně definována ve společné části akreditační dokumentace studijního programu a dále společnými předpisy a směrnicemi VUT
                        a zejména FIT VUT, jak jsou uvedeny na internetových adresách <a href="www.vutbr.cz">www.vutbr.cz</a> a <a href="www.fit.vutbr.cz">www.fit.vutbr.cz</a>.',
        'goals' => 'Obor Informační technologie je zaměřen na výchovu absolventů, kteří se mohou v praxi uplatnit jako projektanti, konstruktéři,
                        programátoři a údržbáři počítačových systémů, číslicových zařízení, konfigurací i jednotlivých počítačů, počítačových sítí,
                        systémů založených na počítačích, jako programátoři a správci databázových systémů a informačních systémů.',
        'range' => 'Státní závěrečná zkouška (SZZ) má dvě části, a to obhajobu bakalářské práce a ústní část spočívající v odborné rozpravě o
                        zadaných tématických okruzích vycházejících z povinných předmětů bakalářského studijního programu.',
        'profile_graduation' => $profile_graduation,
        'practice_range' => 'Odborná praxe není studijním programem předepsána.',
        'thesis_examples' => $thesis_examples,
        'thesis_texts' => 'Bakalářské práce jsou uloženy v Knihovně FIT, Božetěchova 2, Brno. Seznam bakalářských prací včetně detailů je dostupný na
                        Webu FIT: <a href="http://www.fit.vutbr.cz/study/DP/">http://www.fit.vutbr.cz/study/DP/</a>',
        'years' => $years,
];

?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class=""><strong>Program: <?php echo "{$spec['program_code']}"; ?></strong>, <?php echo "{$spec['level']} , {$spec['year_cnt']}"; ?>-letý</h2>
                    <h3 class=""><strong>Obor:</strong> <?php echo "$spec_name";?> - BIT (<?php echo "{$spec_type}";?>.)</h3>
                    <p><strong>Zkratka:</strong><?php echo "{$spec['shortcut']}"; ?></p>
                    <p><strong>Jazyk výuky:</strong><?php echo "{$spec['language']}"; ?></p>
                    <p><strong>Forma studia:</strong><?php echo "{$spec['study_form']}"; ?></p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        <strong>Zařazení oboru do studijního programu:</strong><br />

                        <?php echo "{$spec['text_about']}"; ?>

                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        <strong>Cíle studia:</strong><br />
                        <?php echo "{$spec['goals']}"; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        <strong>Rozsah státních závěrečných zkoušek:</strong><br />

                        <?php echo "{$spec['range']}"; ?>

                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        <strong>Profil absolventa oboru:</strong><br />

                        <ul>
                            <?php foreach($profile_graduation as $profile_graduate){
                                echo "<li>{$profile_graduate}</li>";
                            } ?>
                        </ul>

                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        <strong>Obsah a rozsah odborné praxe:</strong><br />

                        <?php echo "{$spec['practice_range']}"; ?>

                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        <strong>Příklad témat závěrečných prací:</strong><br />

                        <ul>
                            <?php foreach($spec['thesis_examples'] as $thesis_example){
                                echo "<li>{$thesis_example}</li>";
                            } ?>
                        </ul>

                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        <?php echo "{$spec['thesis_texts']}"; ?>

                    </p>
                </div>
            </div>
        </div>
        <div class="sg-box__item">
            <div class="sg-box__item-annot">
            </div>
            <div class="sg-box__item-code sg-box__item-code--bleed">
                <div class="b-admission holder holder--lg pb60--d">
                    <h2 class="b-admission__title h1">Výběr akademického roku</h2>
                    <form action="?" class="f-subjects b-admission__filter">
                        <div class="f-subjects__filter f-subjects__filter--full">
                            <p class="inp inp--multiple">
                                <span class="inp__fix minw440">
                                    <label for="field1" class="inp__label inp__label--inside">Akademický rok</label>
                                    <select name="program-year-select" class="select js-select">
                                        <?php foreach($spec['years'] as $year){
                                            echo "<option>{$year}</option>";
                                        } ?>
                                    </select>
                                </span>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="2019" class="program_year">
            <div class="b-detail">
                <div class="">

                    <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                        <div class="b-detail__abstract fz-lg">
                            <p>
                                <strong>Akademický rok: 2018/2019</strong><br />

                                Tento studijní plán je určen pro studenty, kteří zahájili studium v ak. r. 2015/16 a později. <br />

                                Student si musí zapsat nejprve opakované povinné a povinně volitelné předměty. Dále si student
                                zapisuje předměty povinné a povinně volitelné pro příslušný ročník. Až pak si může zapsat předměty volitelné pro příslušný ročník nebo
                                z nižších ročníků. Pokud si student pro daný semestr nezapíše všechny povinné a povinně volitelné předměty (vyjma předmětů ITT - Semestrální projekt a IBT - Bakalářská práce), smí si v daném semestru zapsat nejvýše jeden volitelný předmět.
                            </p>
                            <table class="table-meta table-subjects">
                                <tbody>
                                <h3>
                                    1.Ročník - zimní semestr
                                </h3>
                                <tr>
                                    <th>Zkr</th>
                                    <th>P</th>
                                    <th>Kr</th>
                                    <th>Název</th>
                                    <th>Ukon</th>
                                    <th>Garant</th>
                                    <th>Fak</th>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table-meta table-subjects">
                                <tbody>
                                <h3>
                                    1.Ročník - letní semestr
                                </h3>
                                <tr>
                                    <th>Zkr</th>
                                    <th>P</th>
                                    <th>Kr</th>
                                    <th>Název</th>
                                    <th>Ukon</th>
                                    <th>Garant</th>
                                    <th>Fak</th>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table-meta table-subjects">
                                <tbody>
                                <h3>
                                    2.Ročník - zimní semestr
                                </h3>
                                <tr>
                                    <th>Zkr</th>
                                    <th>P</th>
                                    <th>Kr</th>
                                    <th>Název</th>
                                    <th>Ukon</th>
                                    <th>Garant</th>
                                    <th>Fak</th>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td>D<a class="table-link" href="../prostudenty/predmet_detail.php">iskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table-meta table-subjects">
                                <tbody>
                                <h3>
                                    2.Ročník - letní semestr
                                </h3>
                                <tr>
                                    <th>Zkr</th>
                                    <th>P</th>
                                    <th>Kr</th>
                                    <th>Název</th>
                                    <th>Ukon</th>
                                    <th>Garant</th>
                                    <th>Fak</th>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table-meta table-subjects">
                                <tbody>
                                <h3>
                                    3.Ročník - zimní semestr
                                </h3>
                                <tr>
                                    <th>Zkr</th>
                                    <th>P</th>
                                    <th>Kr</th>
                                    <th>Název</th>
                                    <th>Ukon</th>
                                    <th>Garant</th>
                                    <th>Fak</th>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table-meta table-subjects">
                                <tbody>
                                <h3>
                                    3.Ročník - letní semestr
                                </h3>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                <tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr><tr class="not-needed">
                                    <td>IDA</td>
                                    <td>P</td>
                                    <td>7</td>
                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                    <td>Zk</td>
                                    <td>Kovár Martin</td>
                                    <td>FEKT</td>
                                </tr>
                                </tbody>
                            </table>
                            <p>
                                <strong>Ukon:</strong>	Způsob zakončení předmětu: Zk - zkouška, Za - zápočet, Klz - klas. zápočet, Kol - Kolokvium<br />
                                <strong>Pov:</strong>	P - povinný, PV - povinně volitelný, D - doporučený, V - volitelný
                            </p>
                            <p>
                                Pokud je povoleno zapsat si více předmětů skupiny PV než je požadované minimum, pak úspěšně absolvované předměty, které překročí Min. předmětů (resp. Min.kred, pokud není 0) skupiny PV, budou studentovi zařazeny jako předměty povinnosti Přes jako.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <div id="2018" class="program_year">
                    <div class="b-detail">
                        <div class="">

                            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                                <div class="b-detail__abstract fz-lg">
                                    <p>
                                        <strong>Akademický rok: 2017/2018</strong><br />

                                        Tento studijní plán je určen pro studenty, kteří zahájili studium v ak. r. 2015/16 a později. <br />

                                        Student si musí zapsat nejprve opakované povinné a povinně volitelné předměty. Dále si student
                                        zapisuje předměty povinné a povinně volitelné pro příslušný ročník. Až pak si může zapsat předměty volitelné pro příslušný ročník nebo
                                        z nižších ročníků. Pokud si student pro daný semestr nezapíše všechny povinné a povinně volitelné předměty (vyjma předmětů ITT - Semestrální projekt a IBT - Bakalářská práce), smí si v daném semestru zapsat nejvýše jeden volitelný předmět.
                                    </p>
                                    <table class="table-meta table-subjects">
                                        <tbody>
                                        <h3>
                                            1.Ročník - zimní semestr
                                        </h3>
                                        <tr>
                                            <th>Zkr</th>
                                            <th>P</th>
                                            <th>Kr</th>
                                            <th>Název</th>
                                            <th>Ukon</th>
                                            <th>Garant</th>
                                            <th>Fak</th>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table-meta table-subjects">
                                        <tbody>
                                        <h3>
                                            1.Ročník - letní semestr
                                        </h3>
                                        <tr>
                                            <th>Zkr</th>
                                            <th>P</th>
                                            <th>Kr</th>
                                            <th>Název</th>
                                            <th>Ukon</th>
                                            <th>Garant</th>
                                            <th>Fak</th>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table-meta table-subjects">
                                        <tbody>
                                        <h3>
                                            2.Ročník - zimní semestr
                                        </h3>
                                        <tr>
                                            <th>Zkr</th>
                                            <th>P</th>
                                            <th>Kr</th>
                                            <th>Název</th>
                                            <th>Ukon</th>
                                            <th>Garant</th>
                                            <th>Fak</th>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td>D<a class="table-link" href="../prostudenty/predmet_detail.php">iskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table-meta table-subjects">
                                        <tbody>
                                        <h3>
                                            2.Ročník - letní semestr
                                        </h3>
                                        <tr>
                                            <th>Zkr</th>
                                            <th>P</th>
                                            <th>Kr</th>
                                            <th>Název</th>
                                            <th>Ukon</th>
                                            <th>Garant</th>
                                            <th>Fak</th>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table-meta table-subjects">
                                        <tbody>
                                        <h3>
                                            3.Ročník - zimní semestr
                                        </h3>
                                        <tr>
                                            <th>Zkr</th>
                                            <th>P</th>
                                            <th>Kr</th>
                                            <th>Název</th>
                                            <th>Ukon</th>
                                            <th>Garant</th>
                                            <th>Fak</th>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table-meta table-subjects">
                                        <tbody>
                                        <h3>
                                            3.Ročník - letní semestr
                                        </h3>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        <tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr><tr class="not-needed">
                                            <td>IDA</td>
                                            <td>P</td>
                                            <td>7</td>
                                            <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                            <td>Zk</td>
                                            <td>Kovár Martin</td>
                                            <td>FEKT</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p>
                                        <strong>Ukon:</strong>	Způsob zakončení předmětu: Zk - zkouška, Za - zápočet, Klz - klas. zápočet, Kol - Kolokvium<br />
                                        <strong>Pov:</strong>	P - povinný, PV - povinně volitelný, D - doporučený, V - volitelný
                                    </p>
                                    <p>
                                        Pokud je povoleno zapsat si více předmětů skupiny PV než je požadované minimum, pak úspěšně absolvované předměty, které překročí Min. předmětů (resp. Min.kred, pokud není 0) skupiny PV, budou studentovi zařazeny jako předměty povinnosti Přes jako.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        <div id="2017" class="program_year">
                            <div class="b-detail">
                                <div class="">

                                    <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                                        <div class="b-detail__abstract fz-lg">
                                            <p>
                                                <strong>Akademický rok: 2016/2017</strong><br />

                                                Tento studijní plán je určen pro studenty, kteří zahájili studium v ak. r. 2015/16 a později. <br />

                                                Student si musí zapsat nejprve opakované povinné a povinně volitelné předměty. Dále si student
                                                zapisuje předměty povinné a povinně volitelné pro příslušný ročník. Až pak si může zapsat předměty volitelné pro příslušný ročník nebo
                                                z nižších ročníků. Pokud si student pro daný semestr nezapíše všechny povinné a povinně volitelné předměty (vyjma předmětů ITT - Semestrální projekt a IBT - Bakalářská práce), smí si v daném semestru zapsat nejvýše jeden volitelný předmět.
                                            </p>
                                            <table class="table-meta table-subjects">
                                                <tbody>
                                                <h3>
                                                    1.Ročník - zimní semestr
                                                </h3>
                                                <tr>
                                                    <th>Zkr</th>
                                                    <th>P</th>
                                                    <th>Kr</th>
                                                    <th>Název</th>
                                                    <th>Ukon</th>
                                                    <th>Garant</th>
                                                    <th>Fak</th>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table-meta table-subjects">
                                                <tbody>
                                                <h3>
                                                    1.Ročník - letní semestr
                                                </h3>
                                                <tr>
                                                    <th>Zkr</th>
                                                    <th>P</th>
                                                    <th>Kr</th>
                                                    <th>Název</th>
                                                    <th>Ukon</th>
                                                    <th>Garant</th>
                                                    <th>Fak</th>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table-meta table-subjects">
                                                <tbody>
                                                <h3>
                                                    2.Ročník - zimní semestr
                                                </h3>
                                                <tr>
                                                    <th>Zkr</th>
                                                    <th>P</th>
                                                    <th>Kr</th>
                                                    <th>Název</th>
                                                    <th>Ukon</th>
                                                    <th>Garant</th>
                                                    <th>Fak</th>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td>D<a class="table-link" href="../prostudenty/predmet_detail.php">iskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table-meta table-subjects">
                                                <tbody>
                                                <h3>
                                                    2.Ročník - letní semestr
                                                </h3>
                                                <tr>
                                                    <th>Zkr</th>
                                                    <th>P</th>
                                                    <th>Kr</th>
                                                    <th>Název</th>
                                                    <th>Ukon</th>
                                                    <th>Garant</th>
                                                    <th>Fak</th>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table-meta table-subjects">
                                                <tbody>
                                                <h3>
                                                    3.Ročník - zimní semestr
                                                </h3>
                                                <tr>
                                                    <th>Zkr</th>
                                                    <th>P</th>
                                                    <th>Kr</th>
                                                    <th>Název</th>
                                                    <th>Ukon</th>
                                                    <th>Garant</th>
                                                    <th>Fak</th>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="table-meta table-subjects">
                                                <tbody>
                                                <h3>
                                                    3.Ročník - letní semestr
                                                </h3>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                <tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr><tr class="not-needed">
                                                    <td>IDA</td>
                                                    <td>P</td>
                                                    <td>7</td>
                                                    <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                                    <td>Zk</td>
                                                    <td>Kovár Martin</td>
                                                    <td>FEKT</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <p>
                                                <strong>Ukon:</strong>	Způsob zakončení předmětu: Zk - zkouška, Za - zápočet, Klz - klas. zápočet, Kol - Kolokvium<br />
                                                <strong>Pov:</strong>	P - povinný, PV - povinně volitelný, D - doporučený, V - volitelný
                                            </p>
                                            <p>
                                                Pokud je povoleno zapsat si více předmětů skupiny PV než je požadované minimum, pak úspěšně absolvované předměty, které překročí Min. předmětů (resp. Min.kred, pokud není 0) skupiny PV, budou studentovi zařazeny jako předměty povinnosti Přes jako.
                                            </p>
                                        </div>
                                    </div>
                                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>