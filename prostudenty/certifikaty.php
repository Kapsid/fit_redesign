<?php
include '../header.php';
?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class="">Doplňující certifikáty</h2>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                    <div class="b-detail__abstract fz-lg">
                        <p>
                            Přestože je bakalářský studijní program jednooborový, mohou studenti své odborné zaměření ovlivnit volbou volitelných předmětů.
                            Výpis všech absolvovaných předmětů jim bude předán bezplatně jako dodatek k diplomu. Dále může student po absolvování předepsané skupiny volitelných předmětů požádat o vydání:
                            <ul>
                                <li><strong>Certifikátu o studiu s rozšířenými jazykovými kompetencemi,</strong></li>
                                <li><strong>Certifikátu o studiu se základy manažerských kompetencí</strong> nebo</li>
                                <li>ve spolupráci s FEKT <strong>Osvědčení o pedagogické způsobilosti</strong>.</li>
                            </ul>
                        </p>
                        <p>
                            Kromě toho může předměty zajišťované ve spolupráci se společnostmi Cisco a Microsoft využít jako přípravu pro zisk mezinárodně platného certifikátu <strong>
                                Cisco Certified Network Associate</strong> nebo <strong>Microsoft Certified Professional</strong>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class="b-detail__title">Podmínky pro udělení Certifikátu o studiu s rozšířenými jazykovými kompetencemi
                    </h2>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                    <div class="b-detail__abstract fz-lg">
                        <p>
                            Přestože je bakalářský studijní program jednooborový, mohou studenti své odborné zaměření ovlivnit volbou volitelných předmětů.
                            Výpis všech absolvovaných předmětů jim bude předán bezplatně jako dodatek k diplomu. Dále může student po absolvování předepsané skupiny volitelných předmětů požádat o vydání:
                        <ul>
                            <li>úspěšné absolvování předmětu Angličtina 4: středně pokročilí 2 (BAN4) a<</li>
                            <li>získání nejméně dalších 12 kreditů úspěšným absolvováním předmětů cizích jazyků.</li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class="b-detail__title">Podmínky pro udělení Certifikátu o studiu se základy manažerských kompetencí</h2>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                    <div class="b-detail__abstract fz-lg">
                        <p>
                        <ul>
                            <li>Získání nejméně 15 kreditů za úspěšně absolvované předměty manažerské, ekonomické a podporující podnikání.</li>
                        </ul>
                        </p>
                        <p>
                            Vydávání těchto dvou certifikátů upravuje <a href="http://www.fit.vutbr.cz/info/rd/2008/rd06-080118.pdf">Rozhodnutí děkana
                                FIT č. 6/2008</a> (jím definovaný Certifikát o studiu s rozšířenými základy pro aplikace počítačové grafiky a multimédií vydáván není pro omezenou nabídku vhodných volitelných předmětů).
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>