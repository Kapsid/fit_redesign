<?php
include '../header.php';

$subjects = [
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Diskrétní matematika","credits" => 6, "teacher" => "Kovár Martin, doc. RNDr., Ph.D", "type" => "not-needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "P", "ending" => "ZÁ/ZK"],
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Diskrétní matematika","credits" => 6, "teacher" => "Kovár Martin, doc. RNDr., Ph.D", "type" => "not-needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Diskrétní matematika","credits" => 6, "teacher" => "Kovár Martin, doc. RNDr., Ph.D", "type" => "not-needed", "needed" => "V", "ending" => "ZK"],
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Diskrétní matematika","credits" => 6, "teacher" => "Kovár Martin, doc. RNDr., Ph.D", "type" => "not-needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "V", "ending" => "ZÁ/ZK"],
    ["name" => "Diskrétní matematika","credits" => 6, "teacher" => "Kovár Martin, doc. RNDr., Ph.D", "type" => "not-needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Fukncionální a logické programování","credits" => 7, "teacher" => "Doc. Dr. Ing. Dušan Kolář", "type" => "needed", "needed" => "P", "ending" => "ZK"],
    ["name" => "Diskrétní matematika","credits" => 6, "teacher" => "Kovár Martin, doc. RNDr., Ph.D", "type" => "not-needed", "needed" => "P", "ending" => "ZÁ"],
];


?>
    <main id="main" class="main pt60" role="main">
        <div class="b-intro border-b holder holder--lg">
            <h1 class="b-intro__title">Předměty</h1>

            <form action="?" class="f-subjects">
                <div class="f-subjects__filter f-subjects__filter--full">
                    <p class="inp inp--multiple">
						<span class="inp__fix minw180">
								<label for="degree" class="inp__label inp__label--inside">Stupeň studia</label>
								<select name="degree" id="degree" class="select js-select">
									<option selected disabled placeholder>Stupeň studia</option>
									<option>bakalářský</option>
									<option>magisterský</option>
									<option>doktorský</option>
								</select>
							</span>
                        <span class="inp__fix minw180">
								<label for="semester" class="inp__label inp__label--inside">Semestr</label>
								<select name="semester" id="semester" class="select js-select">
									<option selected disabled placeholder>Semestr</option>
									<option>letní</option>
									<option>zimní</option>
								</select>
							</span>
                        <span class="inp__fix minw180">
								<label for="year" class="inp__label inp__label--inside">Rok</label>
								<select name="year" id="year" class="select js-select">
									<option selected disabled placeholder>Rok</option>
									<option>Hodnota 1</option>
									<option>Hodnota 2</option>
									<option>Hodnota 3</option>
									<option>Hodnota 4</option>
									<option>Hodnota 5</option>
								</select>
							</span>
                        <span class="inp__fix minw180">
								<label for="language" class="inp__label inp__label--inside">Jazyk výuky</label>
								<select name="language" id="language" class="select js-select">
									<option selected disabled placeholder>Jazyk výuky</option>
									<option>česky</option>
									<option>anglicky</option>
								</select>
							</span>
                        <span class="inp__fix">
								<label class="checkbox">
									<input type="checkbox" class="checkbox__control" checked>
									<span class="checkbox__label">
										Pro zahraniční studenty
                                        <span class="icon-svg icon-svg--check checkbox__icon">
                                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <use xlink:href="/img/bg/icons-svg.svg#icon-check" x="0" y="0" width="100%"
                                             height="100%"></use>
                                    </svg>
                                </span>

									</span>
								</label>
							</span>
                    </p>
                </div>

                <div class="f-subjects__search">
                    <p class="inp inp--group-spaced mb0">
							<span class="inp__fix">
								<label for="f-subjects__search" class="inp__label inp__label--inside">Hledat předmět podle názvu nebo zkratky</label>
								<input type="text" class="inp__text" id="f-subjects__search"
                                       placeholder="Hledat předmět podle názvu nebo zkratky">
							</span>
                        <span class="inp__btn">
								<button class="btn btn--secondary btn--block--m" type="submit">
									<span class="btn__text">Hledat</span>
								</button>
							</span>
                    </p>
                </div>
            </form>
        </div>

        <div class="c-subjects holder holder--lg">
            <table class="table-meta table-subjects">
                <tbody>
                <tr>
                    <th>P</th>
                    <th>Kr</th>
                    <th>Název</th>
                    <th>Ukon</th>
                    <th>Garant</th>
                </tr>
                <?php foreach($subjects as $subject)
                    echo "<tr class=\"{$subject['type']}\">
                            <td>{$subject['needed']}</td>
                            <td>{$subject['credits']}</td>
                            <td><a class=\"table-link\" href=\"../prostudenty/predmet_detail.php\">{$subject['name']}</a></td>
                            <td>{$subject['ending']}</td>
                            <td>{$subject['teacher']}</td>
                        </tr>";
                    ?>
                </tbody>
            </table>

        </div>


    </main>
<?php
include '../footer.php'
?>