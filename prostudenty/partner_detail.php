<?php
include '../header.php';

$garants = [
  ['name' => 'Dušan', 'surname' => 'Kolář', 'titles' => 'doc. Dr. Ing']
];

$thesis = [
    ['type'=>'BP','title' => '/Avast/ Metody pro extrakci a detekci vzorů v programovém kódu', 'leader' => 'Milkovič Marek, Ing.', 'availability' => 'volno', 'year' => 2018],
    ['type'=>'BP','title' => '/Avast/ Shluková analýza malware', 'leader' => 'Milkovič Marek, Ing.', 'availability' => 'volno', 'year' => 2018],
    ['type'=>'BP','title' => '/Avast/ Zpětný překlad kódu aplikací - dekompilace', 'leader' => 'Milkovič Marek, Ing.', 'availability' => 'volno', 'year' => 2018],
    ['type'=>'BP','title' => 'Analýza skriptů pro účely shlukové analýzy', 'leader' => 'Křivka Zbyněk, Ing., Ph.D.', 'availability' => 'přihlášeno', 'year' => 2018],
    ['type'=>'BP','title' => 'Rozšíření systému pro shlukovou analýzu souborů', 'leader' => '', 'availability' => 'přihlášeno', 'year' => 2018],
    ['type'=>'BP','title' => '/Avast/ Metody pro extrakci a detekci vzorů v programovém kódu', 'leader' => 'Milkovič Marek, Ing.', 'availability' => 'přihlášeno', 'year' => 2018],
    ['type'=>'BP','title' => 'Vylepšení generování vzorů pro detekci škodlivého kódu', 'leader' => 'Křivka Zbyněk, Ing., Ph.D.', 'availability' => 'přihlášeno', 'year' => 2018],
];

$internships = [
  ['name' => 'Nabídka 1 stáž', 'link' => 'http://www.fit.vutbr.cz/cooperation/partner1.php?id=1998&nabidka=100&file=itsec-plakat.pdf','text' => 'Popis tématu', 'mail' => 'beranv@fit.vutbr.cz']
];

$offers = [
    ['name' => 'Nabídka 2 - místo', 'text' => 'Popis 2', 'person' => 'Ing. Vítězslav Beran, Ph.D.']
];

$grants = [
    ['name' => 'Metody pro extrakci a detekci vzorů v programovém kódu', 'from' => 2017, 'to' => 2021]
];

$partner_detail = [
    'title' => 'Avast software s.r.o. - stříbrný partner FIT',
    'text' => 'Avast je jedním z největších poskytovatelů zabezpečení na světě, který používá technologie nové generace k boji proti
                kybernetickému zločinu v reálném čase. Na rozdíl od jiných společností využíváme naplno potenciál strojového učení v cloudu,
                kde dokážeme zpracovávat nepřetržitý proud dat od našich stovek milionů uživatelů. To nám umožňuje neustále vylepšovat a zdokonalovat
                naše systémy, díky nimž je náš modul umělé inteligence chytřejší a rychlejší než kterýkoli jiný.',
    'garants' => $garants,
    'thesis' => $thesis,
    'offers' => $offers,
    'grants' => $grants,
    'interships' => $internships,
    'link' => 'https://www.avast.com/cs-cz/index#mac',
    'image' => 'avast.png',
]
?>

<main id="main" class="main" role="main">

    <div class="b-intro border-b holder holder--lg">

        <div class="b-detail">
            <h2><?php echo "{$partner_detail['title']}"; ?></h2>
            <p>
                <?php echo "{$partner_detail['text']}"; ?>
            </p>

            <?php echo "<a href=\"{$partner_detail['link']}\"><img class=\"partner-logo\" src=\"../img/logos/{$partner_detail['image']}\" alt=\"partner_logo\"></a>"; ?>


            <h3>
                Garanti spolupráce
            </h3>
            <ul>
                <?php foreach($partner_detail['garants'] as $garant){
                   echo "<li><a href=\"../ofakulte/profil.php\">{$garant['surname']} {$garant['name']}, {$garant['titles']} </a></li>";
                }?>

            </ul>

            <h3>Bakalářské a diplomové projekty</h3>
            <ul>
                <?php foreach($partner_detail['thesis'] as $these){
                    echo "<li>{$these['type']}:<a href=\"../vedavyzkum/projekty_detail.php\"> {$these['title']},</a> 
                                <a href=\"../ofakulte/profil.php\">{$these['leader']}, </a>{$these['availability']}, {$these['year']}</li>";
                }
                ?>
            </ul>

            <h3>
                Stáže
            </h3>

            <div>
                <?php foreach($partner_detail['interships'] as $intership){
                    echo "<a href='{$intership['link']}'>{$intership['name']}</a><p>{$intership['text']}</p><a href=\"mailto:beranv@fit.vutbr.cz\">{$intership['mail']}</a>";
                }
                ?>
            </div>

            <h3>
                Nabídka pracovních pozic
            </h3>

            <div>
                <?php foreach($partner_detail['offers'] as $offer){
                    echo "<h4>{$offer['name']}</h4><p>{$offer['text']}</p><a href=\"../ofakulte/profil.php\">{$offer['person']}</a>";
                }
                ?>
            </div>

            <h3>
                Granty a smluvní výzkum
            </h3>
            <ul>
                <?php foreach($grants as $grant){
                    echo "<li><a href=\"../vedavyzkum/projekty_detail.php\">{$grant['name']}</a>, {$grant['from']}-{$grant['to']}</li>";
                }

                ?>

            </ul>


        </div>

    </div>
</main>
<?php
include '../footer.php';
?>
