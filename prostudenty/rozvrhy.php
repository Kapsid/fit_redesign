<?php
include '../header.php';
?>
<main id="main" class="main" role="main">

    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Rozvrhy</h2>
            </div>

            <div class="grid__cell size--t12-12 holder holder--lg b-detail__head">
                <a href="../ofakulte/rozvrh_ucebna.php">Rozvrh učebny</a>
                <a href="../ofakulte/rozvrh_skupina.php">Rozvrh ročníku</a>
            </div>
        </div>
    </div>
</main>
<?php
include '../footer.php';
?>
