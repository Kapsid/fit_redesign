<?php
include '../header.php';
$lecturers = [
    ['name' => 'Jana', 'surname' => 'Řezáčová', 'titles' => 'Ing.'],
    ['name' => 'Jana', 'surname' => 'Nováková', 'titles' => 'Ing.'],
    ['name' => 'Adam', 'surname' => 'Poláček', 'titles' => 'Ing.'],
];


$practicers = [
    ['name' => 'Adam', 'surname' => 'Novák', 'titles' => 'Mgr.'],
    ['name' => 'Jana', 'surname' => 'Nováková', 'titles' => 'Ing.'],
    ['name' => 'Adam', 'surname' => 'Poláček', 'titles' => 'Ing.'],
];

$goals = [
    'Navrhnout algoritmy jednoduchých úloh',
    'Používat základní prvky programu (proměnné, matematické operace, podmínky, cykly aj.)',
    'Používat iterační i rekurzivní postupy',
    'Používat funkce nejpoužívanějších knihoven',
];

$alongSubjects = [
    ['name' => 'Angličtina v elektrotechnice a informatice', 'description' => 'Přes náročný svou stavební průlomovým oteplováním uvažovat.'],
    ['name' => 'Audio inženýrství', 'description' => 'popis'],
];

$studyPrograms = [
    ['name' => 'Studijní program 1', 'description' => 'Přes náročný svou stavební průlomovým oteplováním uvažovat.'],
    ['name' => 'Studijní program 2', 'description' => 'Studijní program'],
    ['name' => 'Studijní program 3', 'description' => 'Popisu na své poslem rok metry v mládě i velice.'],
];

$studentsRating = 'Studenti musí získat minimálně 50 bodů ze 100 bodů v dílčích aktivitách:<br>
                                1. test: základy programování v Matlabu (min 3 body, max 10 bodů)<br>
                                2. test: návrh algoritmů (min 3 body, max 10 bodů)<br>
                                3. projekt: návrh vlastní knihovny funkcí (min 4 body, max 15 bodů) + technická dokumentace (min 3 body, max 10 bodů)<br>
                                4. závěrečný písemný test: (min 20 bodů, max 55 bodů).<br>
                                Dílčí aktivity mají prověřit schopnosti studenta navrhnout algoritmy pro řešení jednoduchých úloh a prokázat je realizací příslušných programů';

$literature = [
    ['title' => 'Matlab pro začátečníky', 'authors' => 'Zaplatílek K, Doňar B', 'created' => 'Technická literatura BEN', 'place' => 'Praha 2003'],
    ['title' => 'Matlab pro začátečníky', 'authors' => 'Zaplatílek K, Doňar B', 'created' => 'Technická literatura BEN', 'place' => 'Praha 2003'],
];

$lectures = [
    ['order' => '1.', 'name' => 'Úvodní přednáška'],
    ['order' => '2.', 'name' => 'Obshovaná přednáška'],
    ['order' => '3.', 'name' => 'Koncová přednáška'],
];

$subject = [
    'name' => 'Základy programování',
    'type' => 'Povinný',
    'maturity' => 'Bakalářské',
    'year' => '2017/2018',
    'timing' => 'Letní',
    'studentYear' => 1,
    'credits' => 4,
    'description' => 'Předmět je koncipován jako úvod do problematiky algoritmizace a programování. Studenti jsou seznámeni se základními pojmy z oblasti programování, vytváření algoritmů a programů. Je kladen důraz na pochopení návrhu a realizace programů. Jsou požadovány znalosti základních prvků programu a prokázání, že jsou studenti schopni tyto prvky používat. Studenti jsou seznámeni s programovým prostředím Matlab, kde studenti programují skripty a funkce řešící jednoduché úlohy.',
    'garant' => ['name' => 'Jana', 'surname' => 'Kolářová', 'title' => 'doc. Ing. Ph.D.'],
    'lang' => 'Čeština',
    'place' => 'D105,D0206',
    'lecturers' => $lecturers,
    'practicers' => $practicers,
    'goals' => $goals,
    'realization' => '90 % kontaktní výuka, 10 % distančně',
    'ending' => 'Zápočet a zkouška',
    'prerequisities' => 'Jsou požadovány znalosti na úrovni středoškolského studia',
    'alongSubjects' => $alongSubjects,
    'literature' => $literature,
    'studentsRating' => $studentsRating,
    'lectures' => $lectures,
    'studyPrograms' => $studyPrograms,


];
?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head b-detail__head--arrow">
                    <p class="mb20">
                        <span class="tag tag--sm">Detail předmětu</span>
                    </p>
                    <h1 class="b-detail__title"><?php echo "{$subject['name']}" ?></h1>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary">
                    <p class="b-detail__annot mb20">
                        <span class="b-detail__annot-item font-bold"><?php echo "{$subject['name']}" ?> předmět</span>
                        <span class="b-detail__annot-item font-bold"><?php echo "{$subject['maturity']}" ?>
                            studium</span>
                        <span class="b-detail__annot-item font-bold">Ak. rok <?php echo "{$subject['year']}" ?></span>
                        <span class="b-detail__annot-item font-bold"><?php echo "{$subject['timing']}" ?> semestr</span>
                        <span class="b-detail__annot-item font-bold"><?php echo "{$subject['studentYear']}" ?>
                            . ročník</span>
                        <span class="b-detail__annot-item font-bold"><?php echo "{$subject['credits']}" ?>
                            kredity</span>
                    </p>
                    <div class="b-detail__abstract fz-lg">
                        <p>
                            <?php echo "{$subject['description']}" ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="b-detail__body border-t">
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Garant předmětu
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <a href="#"><?php echo "{$subject['garant']['surname']} {$subject['garant']['name']},{$subject['garant']['title']}" ?></a>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Vyučovací jazyk
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php echo "{$subject['lang']}" ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Místo výuky
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php echo "{$subject['place']}" ?>
                            </p>
                        </div>
                    </div>


                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Přednášející
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php
                                foreach ($subject['lecturers'] as $lecturer) {
                                    echo "<a href=\"../ofakulte/profil.php\">{$lecturer['name']} {$lecturer['surname']},{$lecturer['titles']}</a><br>";
                                }
                                ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Cvičící
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php
                                foreach ($subject['practicers'] as $practicer) {
                                    echo "<a href=\"../ofakulte/profil.php\">{$practicer['name']} {$practicer['surname']},{$practicer['titles']}</a><br>";
                                }
                                ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Cíle předmětu
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p class="mb0">
                                Absolvent studia je schopen:
                            </p>
                            <ul>
                                <?php foreach ($subject['goals'] as $goal) {
                                    echo "<li>
                                            {$goal}
                                        </li>";
                                } ?>
                            </ul>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Způsob realizace výuky
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php echo "{$subject['realization']}" ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Prerekvizity
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php echo "{$subject['prerequisities']}" ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Navazující předměty
                        </p>
                    </div>

                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <?php foreach ($subject['alongSubjects'] as $alongSubject) {
                                echo "<p>
                                        <a href=\"../predmet_detail.php\">{$alongSubject['name']}</a><br>
                                        {$alongSubject['description']}
                                    </p>";
                            } ?>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Ukončení
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php echo "{$subject['ending']}" ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Způsob a kritéria hodnocení
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php echo "{$subject['studentsRating']}" ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Doporučená nebo<br>
                            povinná literatura
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php foreach ($subject['literature'] as $literature) {
                                    echo "{$literature['authors']}: {$literature['title']}, {$literature['created']}, {$literature['place']}";
                                }
                                ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Osnova přednášek
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                                <?php foreach ($subject['lectures'] as $lecture) {
                                    echo "{$lecture['order']}. {$lecture['name']}<br />";
                                }
                                ?>
                            </p>
                        </div>
                    </div>

                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            Zařazení předmětu ve<br>
                            studijních programech
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <?php foreach ($subject['studyPrograms'] as $studyProgram) {
                                echo "<p>
                                        <a href=\"#\">{$studyProgram['name']}</a><br>
                                        {$studyProgram['description']}
                                    </p>";
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>