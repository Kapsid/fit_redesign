<?php
include '../header.php';
?>
<main id="main" class="main" role="main">

    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Doktorské studium</h2>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        Popis studia..
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="c-faculties holder holder--lg c-faculties--pattern">

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../studujfit/nabidka-oboru.php#doktorske-studium2" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Obory</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/studijniplan_phd.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Studijní plán</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/disertacni_prace.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Disertační práce</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/formulare.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Formuláře</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/pokyny.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Pokyny</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/oborova_rada.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Oborová rada</h2>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="holder holder--lg pt40--m pb40--m pt40--t pb40--t pt70 pb110 border-t">
        <h2 class="text-center mb32--m mb32--t mb40">Kontakty pro studenty</h2>

        <div class="b-vcard">
            <div class="b-vcard__img">
                <img src="../img/illust/hruska.jpg" width="207" height="306" alt="">
            </div>
            <div class="b-vcard__content">
                <div class="b-vcard__title">
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">prof. Ing.</span>
                    </p>
                    <h3 class="title title--sm title--secondary">
                        <span class="title__item">Tomáš Hruška</span>
                    </h3>
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">CSc.</span>
                    </p>
                </div>
                <div class="b-vcard__text">
                    <p>
                        Zástupce pověřený vedením agendy vědy a výzkumu
                    </p>
                </div>
                <p class="b-vcard__contacts">
                    <a href="mailto:prodekan-vzdelavani@fit.vutbr.cz" class="link-icon">
                        <span class="icon-svg icon-svg--message link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                        prodekan-vzdelavani@fit.vutbr.cz
                    </a><br />
                </p>
            </div>
        </div>
    </div>

    <div class="c-employees">
        <div class="holder holder--lg border-t">
            <div class="c-employees__wrap">
                <ul class="c-employees__list grid grid--60">
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee" style="padding-top: 15px;">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img custom-study-img" style="top: -90px;">
                                    <img src="/img/illust/b-employee--02.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Petr Veigend</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">studijní poradce</p>
                                    <a class="text-center" href="tel:420 54114-1183">+420 54114-1183</a>
                                    <a class="text-center" href="mailto:studijni-poradce@fit.vutbr.cz">studijni-poradce@fit.vutbr.cz</a>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee" style="padding-top: 15px;">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img custom-study-img" style="top: -90px;">
                                    <img src="/img/illust/b-employee--01.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Petra Kůdelová</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">referentka pro magisterské studium </p>
                                    <a class="text-center" href="tel:+541 141 245">541 141 245</a>
                                    <a class="text-center" href="mailto:kudelova@fit.vut.cz">kudelova@fit.vut.cz</a>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</main>
<?php
include '../footer.php';
?>
