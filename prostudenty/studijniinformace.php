<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-hero-header ">
        <div class="b-hero-header__img " style="background-image: url('/img/illust/b-hero-header--13.jpg');"></div>
        <div class="b-hero-header__content holder holder--lg">
            <h1 class="title b-hero-header__title">
                <span class="title__item">Fakulta, která žije IT</span>
            </h1>
            <a href="#content" data-slide="#content" class="b-hero-header__next b-hero-header__next--bleed">
			<span class="icon-svg icon-svg--angle-d ">
                <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="/img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
                </svg>
            </span>

                <span class="vhide">Další</span>
            </a>
        </div>
    </div>

    <div id="content"></div>

    <div class="c-attrs holder holder--lg bg pt40--m pt0 pb60--d">
        <ul class="c-attrs__list c-attrs__list--bleed grid grid--0 grid--bd">
            <li class="c-attrs__item grid__cell grid__cell--grow size--t-4-12">
                <a href="../prostudenty/bakalar.php" class="b-course">
				<span class="icon-svg icon-svg--fekt b-course__icon color-secondary">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-fekt" x="0" y="0" width="100%" height="100%"></use>
                    </svg>
                </span>

                    <h2 class="b-course__title">Bakalářské studium</h2>
                    <div class="b-course__annot">
                        <p>
                            text o bakalářském studiu
                        </p>
                    </div>
                    <p class="b-course__btn">
                        <span class="btn btn--outline btn--sm">
                            <span class="btn__text">Zjistit více</span>
                        </span>
                    </p>
                </a>
            </li>
            <li class="c-attrs__item grid__cell grid__cell--grow size--t-4-12">
                <a href="../prostudenty/magistr.php" class="b-course">
				<span class="icon-svg icon-svg--research b-course__icon color-secondary">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-research" x="0" y="0" width="100%"
                             height="100%"></use>
                    </svg>
                </span>

                    <h2 class="b-course__title">Magisterské studium</h2>
                    <div class="b-course__annot">
                        <p>
                            text o magisterském studiu
                        </p>
                    </div>
                    <p class="b-course__btn">
                    <span class="btn btn--outline btn--sm">
                        <span class="btn__text">Zjistit více</span>
                    </span>
                    </p>
                </a>
            </li>
            <li class="c-attrs__item grid__cell grid__cell--grow size--t-4-12">
                <a href="../prostudenty/doktor.php" class="b-course">
				<span class="icon-svg icon-svg--building b-course__icon color-secondary">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-building" x="0" y="0" width="100%"
                             height="100%"></use>
                    </svg>
                </span>

                    <h2 class="b-course__title">Doktorské studium</h2>
                    <div class="b-course__annot">
                        <p>
                            text o doktorském studiu
                        </p>
                    </div>
                    <p class="b-course__btn">
                    <span class="btn btn--outline btn--sm">
                        <span class="btn__text">Zjistit více</span>
                    </span>
                    </p>
                </a>
            </li>
        </ul>
    </div>

    <div class="c-faculties holder holder--lg pt55--d">

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty ">
                        <h2 class="b-faculty__title h3">Rozvrhy</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../cvt/uvod.php" class="b-faculty ">
                        <h2 class="b-faculty__title h3">CVT</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="http://www.fit.vutbr.cz/study/advisor/" class="b-faculty ">
                        <h2 class="b-faculty__title h3">Web studijního poradenství</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty ">
                        <h2 class="b-faculty__title h3">Stipendia a ocenění</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty ">
                        <h2 class="b-faculty__title h3">Studijní předpisy a směrnice</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty ">
                        <h2 class="b-faculty__title h3">Nabídky Cesa</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty ">
                        <h2 class="b-faculty__title h3">Celoživotní vzdělávání</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="#" class="b-faculty ">
                        <h2 class="b-faculty__title h3">Studium bez bariér</h2>
                        <div class="b-faculty__annot">
                            <p>
                                Popis..
                            </p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="b-contact b-contact--map b-contact--primary holder holder--md">
        <h2 class="b-contact__title">Kontakt - studijní poradci</h2>
        <div class="b-contact__text">
            <p>
                Ing. Miloš Eysselt, CSc. <br />
                Ing. Petr Veigend<br />

                <strong>
                <a href="mailto:studijni-poradce@fit.vutbr.cz">e-mail: studijni-poradce@fit.vutbr.cz</a>
                </strong><br>
                <strong>
                    <a href="http://www.fit.vutbr.cz/study/advisor/">web: www.fit.vutbr.cz/study/advisor/</a>
                </strong><br>

            </p>
        </div>
    </div>

    <div class="c-employees">
        <div class="holder holder--lg">
            <h2 class="c-employees__title">Studijní oddělení</h2>
            <strong style="float: none;margin: 0 auto;display: block;text-align: center;">
                <a href="mailto:studijni@fit.vutbr.cz">e-mail: studijni@fit.vutbr.cz</a>
            </strong>
        </div>
        <div class="holder holder--lg border-t">
            <div class="c-employees__wrap">
                <ul class="c-employees__list grid grid--60">
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img">
                                    <img src="/img/illust/b-employee--01.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Marie Jandová</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">vedoucí</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img">
                                    <img src="/img/illust/b-employee--01.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Zuzana Parasková</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">referentka pro 1. ročník bakalářského studia</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img">
                                    <img src="/img/illust/b-employee--03.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Iva Soušková</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">referentka pro 2. a vyšší ročníky </p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img">
                                    <img src="/img/illust/b-employee--03.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Petra Kůdelová</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">referentka pro magisterské studium</p>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="b-contact b-contact--map holder holder--md">
            <h2 class="b-contact__title">Úřední hodiny</h2>
            <div class="b-contact__text">
                <p>
                    <strong>Pondělí:</strong> 8:00 - 11:00<br />
                    <strong>Středa:</strong> 8:00 - 11:00 a 13:00-14:30<br />
                    <strong>Čtvrtek:</strong> 8:00 - 11:00<br /><br />
                    <strong>Místnost C109</strong>
                </p>
            </div>
        </div>
    </div>

</main>

<?php
include '../footer.php';
?>
