<?php
include '../header.php';
?>

<main id="main" class="main" role="main">

    <div class="c-faculties holder holder--lg pt55--d">
        <div class="">
            <h1>Studijní plány - magisterské studium</h1>
        </div>

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Bezpečnost informačních technologií" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MBS</strong> - Bezpečnost informačních technologií</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Bioinformatika a biocomputing" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MBI</strong> - Bioinformatika a biocomputing</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Informační systémy" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MIS</strong> - Informační systémy</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Inteligentní systémy" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MIN</strong> - Inteligentní systémy</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Management a informační technologie" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MMI</strong> - Management a informační technologie</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Matematické metody v informačních technologiích" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MMM</strong> - Matematické metody v informačních technologiích</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Počítačová grafika a multimédia" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MGM</strong> - Počítačová grafika a multimédia</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Počítačové a vestavěné systémy" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MPV</strong> - Počítačové a vestavěné systémy</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Mgr&spec_name=Počítačové sítě a komunikace" class="b-faculty ">
                        <h2 class="b-faculty__title h3"><strong>MSK</strong> - Počítačové sítě a komunikace</h2>
                    </a>
                </li>
            </ul>
        </div>
    </div>

</main>

<?php
include '../footer.php';
?>
