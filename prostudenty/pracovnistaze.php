<?php
include '../header.php';
?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h1 class="b-detail__title">Pracovní stáže</h1>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                    <div class="b-detail__abstract fz-lg">
                        <p>
                            Nabídka brigád a pracovních stáží Partnerů FIT VUT v Brně. V případě zájmu o zadávání
                            odborných pracovních úkolů pro studenty,
                            doporučujeme našim partnerům využít spíše možnost zadávání studentských projektů. Požadovaná
                            témata nabídnout a konzultovat nejdříve s
                            akademickým pracovníkem na FIT (nejlépe se svojí kontaktní osobou). Studentské projekty lze
                            navázat na výuku jak v předmětech, tak např.
                            jako zadání kvalifikační práce, čímž se snižuje zátěž studentů a udržuje se odborná úroveň
                            pracovní činnosti.<br/>

                            Patrneři FIT VUT v Brně mohou zasílat své nabídky studentských stáží na tento email ve
                            formátu pdf (viz ukázková nabídka), včetně titulku nabídky.
                        </p>
                    </div>
                </div>
            </div>

            <div class="b-detail__body border-t">
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.cadwork.cz/indexL1a.jsp" class="stage-logo"><img
                                        src="../img/stages/cadwork.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/cadwork2018.pdf">C++ Software
                                        Developers with Design Skills, 3D Rendering and GPU Algorithms Developers</a>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.skoda-auto.cz" class="stage-logo"><img src="../img/stages/skodaauto.png"
                                                                                       alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li>
                                    <a href="http://www.fit.vutbr.cz/cooperation/nabidky/skoda2018_ai.pdf">Využití umělé
                                        inteligence při plánování potřeb materiálu</a>
                                </li>
                                <li>
                                    <a href="http://www.fit.vutbr.cz/cooperation/nabidky/skoda2018_iot.pdf">Využití IoT
                                        a RFID v IT systémech pro provoz lisovny</a>
                                </li>
                                <li>
                                    <a href="http://www.fit.vutbr.cz/cooperation/nabidky/skoda2018_predikce.pdf">Prediktivní
                                        analýzy v logistice</a>
                                </li>
                                <li>
                                    <a href="http://www.fit.vutbr.cz/cooperation/nabidky/skoda2018_big.pdf">Analýza
                                        velkých dat výroby</a>
                                </li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.codasip.com" class="stage-logo"><img src="../img/stages/codasip.png"
                                                                                      alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/codasip_2018_1_mozek.pdf">Hledáme
                                        nové mozky - hardware engineer</a></li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/codasip_2018_2_mozek.pdf">Hledáme
                                        nové mozky - verification engineer</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.cesnet.cz" class="stage-logo"><img src="../img/stages/cesnet.png"
                                                                                    alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/cesnet2018.pdf">Výzkum a vývoj
                                        oddělení CESNET</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.redhat.com/en" class="stage-logo"><img src="../img/stages/redhat.png"
                                                                                        alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/redhat2018_c.pdf">Students
                                        programming in C</a></li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/redhat2018_other.pdf">Linux
                                        administration, Java, Ruby or DevOps</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.honeywell.com/worldwide/en-cz" class="stage-logo"><img
                                        src="../img/stages/honeywell.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li>
                                    <a href="http://www.fit.vutbr.cz/cooperation/nabidky/honeywell2018_juniordataanalyst.pdf">Junior
                                        Data Analyst</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.kinali.cz/cs/" class="stage-logo"><img
                                        src="../img/stages/kinalisoft.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/kinali_nabidkastaze2018.pdf">Student
                                        se zájmem o robotizaci</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.fei.com/home/" class="stage-logo"><img
                                        src="../img/stages/thermo-fisher-scientific.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/tfs2018_cpp.pdf">Software
                                        engineer C++ nebo C#</a></li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/tfs2018_net.pdf">Software
                                        engineer C#.NET</a></li>

                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.onsemi.com" class="stage-logo"><img src="../img/stages/onsemi-scg.png"
                                                                                    alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/onsemi2018.pdf">Letní analogová
                                        stáž</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.solarwinds.com" class="stage-logo"><img
                                        src="../img/stages/solarwinds.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/solarwinds2018_java.pdf">Summer
                                        Internship - Junior Developer (JavaScript)</a></li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/solarwinds2018_qa.pdf">Summer
                                        Internship - Automation QA</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.rcesystems.cz" class="stage-logo"><img src="../img/stages/rce.png"
                                                                                       alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/rce2018_c.pdf">C#/ASP.NET
                                        developer</a></li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/rce2018_cv.pdf">C++ & AI &
                                        computer vision developer</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.sewio.net" class="stage-logo"><img src="../img/stages/sewio.png"
                                                                                    alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/sewio2018.pdf">Zpracování
                                        lokačních dat</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.alvao.cz" class="stage-logo"><img src="../img/stages/alvao.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/alvao2018_prog.pdf">Programátor/Analytik</a>
                                </li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/alvao2018_spec.pdf">Specialista
                                        IT/ICT</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.fit.vutbr.cz/cooperation/www.mavenir.com" class="stage-logo"><img
                                        src="../img/stages/mavenir.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/mavenir2018.pdf">Internship -
                                        GUI Developer in Test</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="https://www.tescan.com/en-us/" class="stage-logo"><img
                                        src="../img/stages/tescan.png" alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/tescan2018_cpp.pdf">Vývojář
                                        C++</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.lu.fme.vutbr.cz" class="stage-logo"><img src="../img/stages/fsi.png"
                                                                                         alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li>
                                    <a href="http://www.fit.vutbr.cz/cooperation/nabidky/vut_fsi_inzerat_admin_2018.pdf">Pozice
                                        systémového administrátora</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="grid grid--0">
                    <div class="grid__cell size--t-4-12 holder holder--lg holder--0-r">
                        <p class="b-detail__subtitle font-secondary">
                            <a href="http://www.fit.vutbr.cz" class="stage-logo"><img src="../img/stages/fit.png"
                                                                                      alt=""></a>
                        </p>
                    </div>
                    <div class="grid__cell size--t-8-12 holder holder--lg">
                        <div class="b-detail__content">
                            <p>
                            <ul>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/fit2018-vrasseo_drone.pdf">Asistované
                                        řízení kvadrokoptéry s využitím mapových podkladů</a></li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/fit2018-vrasseo_face.pdf">Upozornění
                                        na výskyt hledané osoby ve video záznamu</a></li>
                                <li><a href="http://www.fit.vutbr.cz/cooperation/nabidky/fit2018-pero.pdf">Zpracování
                                        historických dokumentů</a></li>
                            </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>