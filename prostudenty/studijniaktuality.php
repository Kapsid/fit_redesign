<?php
include '../header.php';

$news = [
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 1"],
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 2"],
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 3"],
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 4"],
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 5"],
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 6"],
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 7"],
        ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 8"],
]
?>

<main id="main" class="main" role="main">
    <div class="c-news holder holder--lg">
        <h1 class="c-news__title">Aktuality</h1>

        <ul class="c-news__list grid grid--50">
            <?php foreach($news as $oneNews){
                echo "<li class=\"c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
                <article class=\"b-news\" role=\"article\">
                    <a href=\"#\" class=\"b-news__link\">
                        <time class=\"b-news__date font-secondary\">{$oneNews['day']}. {$oneNews['month']} {$oneNews['year']}</time>
                        <h2 class=\"b-news__title h3\">{$oneNews['title']}</h2>
                    </a>
                </article>
            </li>";
            }?>

        </ul>

        <nav class="pagination text-center" aria-label="Stránkování" role="navigation">
            <ul class="pagination__list">
                <li class="pagination__item">
                    <a href="#" class="pagination__link" aria-current="page">1</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">2</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">3</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="btn btn--secondary btn--wide btn--icon-only--m">
				<span class="btn__text">
					<span class="hide--m">Další</span>
					<span class="icon-svg icon-svg--angle-r btn__icon hide--t hide--d">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
                    </svg>
                </span>

				</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</main>

<?php
include '../footer.php';
?>
