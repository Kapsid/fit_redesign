<?php
include '../header.php';

$news = [
    ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 1"],
    ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 2"],
    ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 3"],
]
?>

    <div class="b-hero-header b-hero-header--nav">
        <div class="b-hero-header__img" style="background-image: url('/img/illust/b-hero-header--02.jpg');"></div>
        <div class="b-hero-header__content b-hero-header__content--bottom holder holder--lg text-left">
            <p class="mb0">
            </p>
            <h1 class="title title--secondary b-hero-header__title">
                <span class="title__item">Studium a stáže v zahraničí</span>
            </h1>

            <ul class="m-main__list profile-tablist" role="tablist">
                <li class="m-main__item">
                    <a href="#seznam" class="m-main__link" role="tab" aria-controls="seznam"  aria-selected="true">Seznam škol</a>
                </li>
                <li class="m-main__item">
                    <a href="#staze" class="m-main__link" role="tab" aria-controls="staze"">Pracovní stáže</a>
                </li>
                <li class="m-main__item">
                    <a href="#programy" class="m-main__link" role="tab" aria-controls="programy" >Seznam programů</a>
                </li>
                <li class="m-main__item">
                    <a href="#faq" class="m-main__link" role="tab" aria-controls="faq" >FAQ</a>
                </li>
                <li class="m-main__item">
                    <a href="#dalsi" class="m-main__link" role="tab" aria-controls="dalsi" >Další informace</a>
                </li>
                <li class="m-main__item">
                    <a href="#pribehy" class="m-main__link" role="tab" aria-controls="pribehy" >Přiběhy studentů</a>
                </li>
            </ul>
        </div>

    </div>

    <div id="seznam" role="tabpanel">
        <div class="b-detail">
            <div class="grid grid--0">

                <div class="grid__cell size--t-12-12">
                    <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">
                        <h2>Přehled škol, kam je možné vyjet: </h2>
                        <p>
                            Fachhochschule Vorarlberg GmbH<br />
                            Graz University of Technology<br />
                            Katholieke Hogeschool Vives, Campus Brugge, Oostende<br />
                            KU Leuven, Faculty of Engineering Technology (v Oostende, od 9-2017 v Brugge)<br />
                            Technical University of Sofia<br />
                            University of Cyprus, Department of Computer Science<br />
                            Hochschule Furtwangen University, Faculty of Computer Science<br />
                            Technische Universität München<br />
                            Univesrität Paderborn<br />
                            University of Potsdam, Faculty of Mathematics and Natural Sciences<br />
                            Saarland University<br />
                            Universität Siegen, IMT<br />
                            Universität Stuttgart<br />
                            Bauhaus-Universtät Weimar<br />
                            Hochschule RheinMein (Wiesbadaden)<br />
                            Aalborg University, Dpt. of Computer Science<br />
                            Universiy of Southern Denmark<br />
                            Universidad de Granada<br />
                            Universidad Rey Juan Carlos<br />
                            Universidad de Malaga, ETS Ingenieria Informatica<br />
                            Universidad de Sevilla, Faculty of Informatics Engineering<br />
                            Universidade de Valladolid<br />
                            Universidade de Valladolid - Segovia<br />
                            Tallinn University, Institute of Informatics<br />
                            Tallinn University, Haapsalu College<br />
                            ESIEE Amiens<br />
                            Université dÁvignon et des Pays de Vaucluse<br />
                            Ecole Pour Línformatique et les Techniques Avancees (EPITA)<br />
                            Université de Caen Normandie<br />
                            Université Grenoble Alpes (UGA) (Université Joseph Fourier)<br />
                            Université de la Mediteranee (Aix Marseille II)<br />
                            Ecole Supérieure d`Ingénieurs en Électrotechnique (ESIEE Paris)<br />
                            Université Paris-Est, Marne-la-Valée<br />
                            ESIGELEC School of Engineering<br />
                            Université de Bretagne-Sud<br />
                            University of Crete<br />
                            Technological Educational Institute of Crete<br />
                            Aristotle University of Thessaloniki<br />
                            Eötvös Loránd University (ELTE) Faculty of Informatics<br />
                            Széchenyi István University, Information Technology - 1st, 2nd cycle<br />
                            Universitá Della Svizzera Italiana<br />
                            ZHAW - Zürcher Hochschule für Angewandte Wissenschaften<br />
                            Universita´ di Bologna<br />
                            Universitá degli Studi di Palermo<br />
                            Sassari University<br />
                            Dublin Institute of Technology, Faculty of Science, School of Computing<br />
                            University of Reykjavik, School of Computer Science<br />
                            Kaunas University of Technology<br />
                            The University of Malta<br />
                            University of Oslo, Faculty of Mathematics and Natural Sciences, Dpt.of Informatics<br />
                            Norweigian University of Science and Technology (NTNU), campus Trondheim<br />
                            Norweigian University of Science and Technology (NTNU), campus Gjovik<br />
                            Universidade Nnova de Lisboa -Faculdade de Ciencias e Tecnologia<br />
                            Instituto Politecnico do Porto (ESTGF)<br />
                            Universidade de Trás-os-Montes e Alto Douro, School of Science and Technology<br />
                            Uniersity of Lodz, Faculty of Economics and Sociology<br />
                            Rzesow University of Technology<br />
                            Nicolaus Copernicus University in Torun<br />
                            Aalto University, School of Science<br />
                            University of Eastern Finland, UEF, School of Computing (Joensuu campus)<br />
                            Lappeenranta University of Technology<br />
                            Oulu University of Applied Science, School of Engineering<br />
                            Ankara Üniversitesi<br />
                            Anadolu University<br />
                            Yildiz Technical University, Department of Computer Engineering<br />
                            Bangor University, School of Computer Science<br />
                            The University of Warwick<br />
                            University of Surrey<br />
                            The Open University, Knowledge Media Institute<br />
                            University of South Wales<br />

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="staze" role="tabpanel">
        <div class="b-detail">
            <div class="grid grid--0">

                <div class="grid__cell size--t-12-12">
                    <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                        <div class="c-articles-lg ">
                            <div class="holder holder--lg">
                                <h2 class="c-articles-lg__title">Země, ve kterých je možné absolvovat pracovní stáž</h2>
                            </div>
                            <div class="grid__cell size--t-6-12">
                                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">
                                    <h2>Erasmus+ pracovní stáž</h2>
                                    <p>
                                        Země, ve kterých je možné absolvovat pracovní stáž: <a href="https://www.erasmusplus.org.uk/participating-countries#Programme%20Countries">seznam zemí</a>
                                    </p>
                                </div>
                            </div>
                            <div class="grid__cell size--t-6-12">
                                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">
                                    <h2>Fremover</h2>
                                    <p>
                                        Země, ve kterých je možné absolvovat pracovní stáž: libovolná země kromě České republiky</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid__cell size--t-12-12">
                    <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                        <div class="c-articles-lg ">
                            <div class="holder holder--lg">
                                <h2 class="c-articles-lg__title">Instituce/země, ve kterých už studenti absolvovali stáž: </h2>
                            </div>
                            <div class="grid__cell size--t-12-12">
                                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">
                                    <table>
                                        <tr>
                                            <th>Název</th>
                                            <th>Země</th>
                                            <th>Obor</th>
                                            <th>Velikost podniku</th>
                                            <th>Web</th>
                                        </tr>
                                        <tr>
                                            <td>CAS Software AG</td>
                                            <td>Německo</td>
                                            <td>Research and Development</td>
                                            <td>Střední (51-500 zaměstnanců)</td>
                                            <td><a href="http://www.cas.de/start.html">http://www.cas.de/start.html</a></td>
                                        </tr>
                                        <tr>
                                            <td>itestra GmbH</td>
                                            <td>Německo</td>
                                            <td>Information Technology</td>
                                            <td>Malý (1-50 zaměstnanců)</td>
                                            <td><a href="http://www.itestra.de/home/">http://www.itestra.de/home/</a></td>
                                        </tr>
                                        <tr>
                                            <td>Knowledge Media Institute,
                                                The Open University</td>
                                            <td>Velká Británie</td>
                                            <td>Research</td>
                                            <td>Velký (víc jako 500 zaměstnanců)</td>
                                            <td><a href="http://kmi.open.ac.uk">http://kmi.open.ac.uk</a></td>
                                        </tr>
                                        <tr>
                                            <td>B&B Electronics</td>
                                            <td>Irsko</td>
                                            <td>Industrial Communications Equipment
                                                and Machine to Machine Communications</td>
                                            <td>Malý (1-50 zaměstnanců)</td>
                                            <td><a href="http://ww.bb-elec.com">http://www.bb-elec.com</a></td>
                                        </tr>
                                        <tr>
                                            <td>Cleversteam
                                                Smaller Earth Tech Ltd</td>
                                            <td>Velká Británie</td>
                                            <td>Web Development</td>
                                            <td>Malý (1-50 zaměstnanců)</td>
                                            <td><a href="http://www.cleversteam.com">http://www.cleversteam.com</a></td>
                                        </tr>
                                        <tr>
                                            <td>Merlin Business Software</td>
                                            <td>Velká Británie</td>
                                            <td>IT/Software</td>
                                            <td>Střední (51-500 zaměstnanců)</td>
                                            <td><a href="http://www.merlinsw.co.uk">http://www.merlinsw.co.uk</a></td>
                                        </tr>
                                        <tr>
                                            <td>Centre de Morphologie Mathématique
                                                MINES Paristech, PSL - Research University 35, rue Saint Honoré
                                                77300 Fontainebleau</td>
                                            <td>Francie</td>
                                            <td>zejména image processing (v prakticky všech běžných aplikčcních doménách)</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>National University of Ireland, Galway (NUIG)</td>
                                            <td>Irsko</td>
                                            <td>training v obl. zpracování řeči</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>AVG Technologies</td>
                                            <td>Velká Británie</td>
                                            <td>Antivirus technology</td>
                                            <td>Velký (víc jako 500 zaměstnanců)</td>
                                            <td><a href="http://www.avg.com">http://www.avg.com/></td>
                                        </tr>
                                        <tr>
                                            <td>Westhouse Italia Srl</td>
                                            <td>Itálie</td>
                                            <td>Research and Development</td>
                                            <td>Střední (51-500 zaměstnanců)</td>
                                            <td><a href="http://www.cas.de/start.html">http://www.cas.de/start.html</a></td>
                                        </tr>
                                        <tr>
                                            <td>MM Barcoding Ltd.</td>
                                            <td>Velká Británie</td>
                                            <td></td>
                                            <td></td>
                                            <td><a href="http://www.westhouse.it">www.westhouse.it</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="programy" role="tabpanel">
        <div class="b-detail">
            <div class="grid grid--0">

                <div class="grid__cell size--t-12-12">
                    <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                        <div class="c-articles-lg ">
                            <div class="holder holder--lg">
                                <h2 class="c-articles-lg__title">Seznam programů pro výjezd do zahraničí</h2>
                            </div>
                            <div class="grid__cell size--t-6-12">
                                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">
                                    <h2>Studijní pobyty</h2>
                                    <p>
                                        <a href=https://www.vutbr.cz/studenti/staze/pobyty">Studijní pobyty na VUT</a>,
                                        na FIT nejčastěji  <a href="https://www.vutbr.cz/studenti/staze/pobyty/erasmus">Erasmus+ (studijní pobyt)</a> a Freemover.
                                    </p>
                                </div>
                            </div>
                            <div class="grid__cell size--t-6-12">
                                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">
                                    <h2>Praktické stáže</h2>
                                    <p>
                                        <a href="https://www.vutbr.cz/studenti/staze/prakticke ">Praktické stáže na VUT</a>,
                                        na FIT nejčastěji  <a href="https://www.vutbr.cz/studenti/staze/pobyty/erasmus">Erasmus+</a> a Freemover.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="faq" role="tabpanel">
        <div class="b-detail">
            <div class="grid grid--0">

                <div class="grid__cell size--t-12-12">
                    <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                        <div class="c-articles-lg ">
                            <div class="holder holder--lg">
                                <h2 class="c-articles-lg__title">FAQ</h2>
                                <p class="no-margin-text odd-text"><strong>Kde mohu získat dokument “Official grade transcripts for all university studies” , který po mě zahraniční škola vyžaduje?</strong></p>
                                <p class="no-margin-text">nastudijním oddělení, pro vyjíždějící studenty je automaticky vydáváno v angličtině</p><br />
                                <p class="no-margin-text odd-text"><strong>K přihlášce musím přiložit  "A Letter of Recommendation (from a Lecturer or Academic Member of Staff at your current university)". Od koho si mám prosím nechat nasát toto doporučení?</strong></p>
                                <p class="no-margin-text">ideálně od někoho, kdo Vás zná, u koho jste např. zpracovával projekt, bakalářskou práci, ...</p><br />
                                <p class="no-margin-text odd-text"><strong>Kdy se dozvím, jakou částku pro můj pobyt v podobě finanční podpory obdržím?</strong></p>
                                <p class="no-margin-text">výše finanční podpory jsou zveřejněny na webu rektorátu - studium v zahraničí, Erasmus.</p><br />
                                <p class="no-margin-text odd-text"><strong>Co všechno musím podniknout před výjezdem a co po výjezdu na Erasmus?</strong></p>
                                <p class="no-margin-text">přečtěte si informace v dokumentu <a href="">POSTUP PŘI VÝJEZDU</a></p><br />
                                <p class="no-margin-text odd-text"><strong>Jaký je Erasmus kód VUT v Brně?</strong></p>
                                <p class="no-margin-text">CZ BRNO01</p><br />
                                <p class="no-margin-text odd-text"><strong>Koho mám v přihlášce uvést jako "Contact person" (jméno, e-mail, telefon)</strong></p>
                                <p class="no-margin-text">Michaela Studená, studena@fit.vutbr.cz, +420 541 141 265</p><br />
                                <p class="no-margin-text odd-text"><strong>Co znamená u seznamu univerzit číslo ve sloupci měsíce?</strong></p>
                                <p class="no-margin-text">je to celkový počet měsíců, na které daná partnerská instituce příjme naše studenty,
                                    vedlejším sloupci je počet studentů, které můžeme nominovat</p><br />
                                <p class="no-margin-text odd-text"><strong>Dostanu po přezkoušení z angličtiny při výběrovém řízení od školy potvrzení o znalosti AJ na úrovni B2?</strong></p>
                                <p class="no-margin-text">NE, přezkoušení z AJ slouží pouze k účelům rozřazení studentů na požadované
                                    instituce, potřebnou úroveň jazykové kompetence vyžadovanou partnerskou školou (xlsx tabulce a na webu dané instituce) si musíte před výjezdem doplnit sám</p><br />
                                <p class="no-margin-text odd-text"><strong>Myslel jsem si, že bych měl dostat Learning Agreement z partnerské univerzity, ale napsali mi, že Learning Agreement bych měl dostat na naší univerzitě.</strong></p>
                                <p class="no-margin-text">primárně studentům doporučujeme použít pro Learning Agreement formulář přijímající instituce, pokud takový není, nebo na něm na přijímající instituci netrvají, vyplňte formulář LA VUT</p><br />
                                <p class="no-margin-text odd-text"><strong>Co prosím přesně znamená číslo ve sloupci "Měsíce"?</strong></p>
                                <p class="no-margin-text">ve sloupci měsíce je uveden celkový počet měsíců, na které mžeme naše studenty nominovat.</p><br />
                                <p class="no-margin-text odd-text"><strong>Jsou uvedené jazykové požadavky doporučením, nebo nutností?</strong></p>
                                <p class="no-margin-text">Splnění jazykových požadavků je nutností. Někde sice jen doporučením, ale stále častěji si univerzity žádají certifikát o jazyk. kompetencích. Standardním požadavkem pro studium bývá angličtina na úrovni B2.</p><br />
                                <p class="no-margin-text odd-text"><strong>Pokud studuji magistra, předpokládám, že není možné vybrat si jinou univerzitu než u které je zaškrtnuto "Postgraduate"?</strong></p>
                                <p class="no-margin-text">ANO, je to tak</p><br />
                                <p class="no-margin-text odd-text"><strong>Jak to je s registrací předmětů na  FITu, když jsem v dané semestru na Erasmu? Kolik předmětů si mohu zapsat?</strong></p>
                                <p class="no-margin-text">Je povoleno zapsat si předměty o maximální souhrnné kreditové hodnotě do 15 kreditů/semestr.</p><br />
                                <p class="no-margin-text odd-text"><strong>Počítají se předměty přivezené ze zahraničí společně s kredity získanými v daném semestru na FIT do kreditového stropu?</strong></p>
                                <p class="no-margin-text">Při výjezdu do zahraničí se nemusíte zabývat otázkou překročení kreditového stropu, toto bývá tolerováno.</p><br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="dalsi" role="tabpanel">
        <div class="b-detail">
            <div class="grid grid--0">

                <div class="grid__cell size--t-12-12">
                    <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                        <div class="c-articles-lg ">
                            <div class="holder holder--lg">
                                <h2 class="c-articles-lg__title">Další informace</h2>
                                <p>
                                Akce a důležité termíny (cca od září): Erasmus i IRP = freemovers</p>

                                <p>Studenti, kteří se chtějí zúčastnit mobility v rámci programu Erasmus+, musí projít výběrovým řízením, které je vyhlašováno třikrát ročně:
                                    <ul>
                                        <li>1.kolo (pro po výjezdy na zimní semestr nebo celý následující ak. rok) - leden - je avizovaná změna,, bude později</li>
                                        <li>2.kolo (pro výjezdy na letní semestr následujícího ak. roku) - září/říjen</li>
                                        <li>3.kolo (mimořádné, pouze pro výjezdy na praktické stáže) - cca březen</li>
                                        <li>NOVÉ - absolventské stáže - před odevzdáním závěrečné práce - duben/květen</li>
                                    </ul>
                                <a href="http://erasmus-databaze.naep.cz/modules/erasmus/">Databáze závěrečných zpráv studentů</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="pribehy" role="tabpanel">
        <div class="b-detail">
            <div class="grid grid--0 grid--bd">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <div class="b-student">
                        <div class="b-student__wrap">
                            <h2 class="b-student__title">Dita Cihlářová</h2>
                        </div>
                    </div>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__content mb0 pt40--m pb40--m pt40--t pb40--t pt50 pb80 fz-lg js-gallery">
                    <h1>Erasmus+, UK, University of South Wales</h1>
                    <p>
                        Hodně lidí říká: "Nech partnera doma a vyjeď!" Pokud je tohle to, co tě odrazuje od Erasmu, mám pro
                        tebe dobrou zprávu: jde to i ve dvou! My jsme se vydali poznat krásy Velké Británie na University of South Wales.
                        Je to škola plná mezinárodních studentů, kde Erasmáci nestojí mimo, ale jsou začleněni mezi ostatní se vším všudy. Tak se nelekej,
                        až ti omylem přijde mail s pokyny k zaplacení školného. Kromě kampusu s mimořádným zázemím a roztomilými polodivokými králíky si užiješ Cardiff, výlet do Londýna nebo přírodní
                        rezervace, starobylé hrady a surfování (v říjnu!). Taky poznáš jiný vzdělávací systém a možná si (stejně jako my) po návratu budeš ještě více vážit způsobu výuky na VUT FIT i v ČR obecně...
                        Poněkud zvláštní studijní požadavky ale snad vynahradí absence zkouškového a hlavně program, který si uděláš navíc - cestování, sport, zájmové kluby a obohacující rozhovory s lidmi z celého
                        světa, kteří se mohou stát tvými přáteli. A ať už si vybereš Wales nebo jakoukoliv jinou zemi, věř, že tvůj Erasmus bude stát za to - neváhej a jeď!
                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid grid--0 grid--bd">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <div class="b-student">
                        <div class="b-student__wrap">
                            <h2 class="b-student__title">Martin Kocour</h2>
                        </div>
                    </div>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__content mb0 pt40--m pb40--m pt40--t pb40--t pt50 pb80 fz-lg js-gallery">
                    <h1>firma Telefónica, Barcelona, Španělsko - pracovní stáž
                    </h1>
                    <p>
                        Ahoj, volám sa Martin a momentálne som na stáži v Barcelone. Pracujem tu ako research assistant v spoločnosti Telefónica. Mojou úlohou je navrhnúť systém pre spracovanie reči a videa v tzv. “edge” sieti. Ide o relatívne nový trend, kde sa mobilní operátori snažia presunúť výpočet z datacentier čo najbližšie k užívateľovi, s cieľom minimalizovať latenciu.

                        O pracovné stáže som sa počas štúdia nikdy nezaujímal. Myslel som si, že je to pre moju kariéru plytvanie časom. No zlomový moment nastal, keď som zistil, že práca mobilného vývojára, ktorú som v tom čase robil, ma už tak nenapĺňala ako na začiatku. V tom čase ma nesmierne zaujal predmet Spracovanie rečových signálov pod taktovkou pána Doc. Černockého. Preto som sa rozhodol osloviť pána docenta, či náhodou nevie o nejakej práci v tejto oblasti. Práve on mi povedal o možnosti stáže v zahraničí s grantom z programu Erasmus+.

                        Rozhodnutie vystúpiť zo svojej komfortnej zóny vôbec neľutujem. Stretol som tu množstvo zaujímavých ľudí. Naučil som sa kopec nových vecí. Prekonal som komunikačnú bariéru. Práca ma nesmierne baví. Každý deň sa stretávam s novými výzvami. Aj keď cesta sem pre mňa nebola ľahká, hlavne vďaka množstvu administratívnych problémov, s istotou dnes môžem povedať že to všetko stálo za to.

                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid grid--0 grid--bd">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <div class="b-student">
                        <div class="b-student__wrap">
                            <h2 class="b-student__title">Martin Vondráček</h2>
                        </div>
                    </div>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__content mb0 pt40--m pb40--m pt40--t pb40--t pt50 pb80 fz-lg js-gallery">
                    <h1>Erasmus+, Malta, University of Malta</h1>
                    <p>
                        V průběhu bakalářského studia na Fakultě informačních technologií VUT v Brně jsem s nadšením přemýšlel o možnostech studia v zahraničí. Zpětně velmi oceňuji různé prezentační akce, kterými fakulta, univerzita a studentské organizace usilují o informovanost studentů ohledně příležitostí v zahraničí.
                        S přítelkyní, studující také na FIT VUT, jsme chtěli studovat v zahraničí společně a nechtěli jsme prodlužovat studium. Neřešitelná situace? Naopak! Měli jsme opravdu obrovskou radost, když se nám oběma podařilo získat nominaci na University of Malta na šestý semestr bakalářského studia. Většinu studijních povinností jsme zvládli splnit během prvních pěti semestrů, takže naší studijní náplní bylo řešení bakalářských prací pod záštitou obou univerzit.
                        Řešení bakalářské práce na dvou univerzitách zároveň probíhalo formou pravidelných konzultací s maltským vedoucím a videokonferencí s českým vedoucím. V závěru pobytu jsme prezentovali výsledky svých prací vedoucím na University of Malta, následně nás čekala obhajoba v Brně. Přestože jsme se naplno věnovali bakalářské práci a neměli zapsané další předměty, rozhodně jsme nepřišli o maltské studijní prostředí. Univerzita a studentské organizace pořádaly řadu akcí od seznámení s maltskou kulturou po několikadenní výlet na sousední ostrov Gozo.
                        V průběhu čtyř měsíců jsme bydleli v univerzitním penzionu, na koleji a v soukromém studentském podnájmu. Také díky tomu jsme měli příležitost poznat studenty ze všech koutů světa. Například přátelství s korejskými studenty bylo kouzelné velkým kulturním rozdílem. Myslím si, že pobyt na University of Malta byl velmi obohacují zkušenost po studijní stránce, ale také velmi pozitivní setkání přátelských lidí různých kultur a zvyků. Erasmus+ nás tak oslovil, že jsme se s přítelkyní rozhodli vyjet i na magisterském studiu. Všem bych rozhodně tuto zahraniční zkušenost doporučil.
                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid grid--0 grid--bd">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <div class="b-student">
                        <div class="b-student__wrap">
                            <h2 class="b-student__title">Michal Kotoun</h2>
                        </div>
                    </div>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__content mb0 pt40--m pb40--m pt40--t pb40--t pt50 pb80 fz-lg js-gallery">
                    <h1>Island, University of Island</h1>
                    <p>
                        Šestý a závěrečný semestr svého bakalářského studia jsem strávil na pomezí Evropy a Ameriky, v pro nás nejvzdálenější severské zemi, Islandu. Kde jinde si můžete po přednášce zarelaxovat v minutu vzdálené horké lázni na pláži, zatímco teplota vzduchu se drží kolem bodu mrazu?! Island byl pro mě zemí mnoha přírodních krás, nezvyklého nočního života, velmi krátkých i velmi dlouhých dnů, kuchařskou školou, ... Nemá smysl zastírat, že Island je pro našince drahá země a tak na můj pobyt padly i moje úspory a výdělek z dálkové práce v Čechách. Byla to však investice které ani v nejmenším nelituji, a možnou alternativou je najít si práci na místě, což i vzhledem k trvajícímu růstu turismu není problém. Turismus vůbec Island v posledních letech pomalu mění a patřil tak k jednomu ze základních témat v rozhovorech s lidmi v zemi, která je tak trochu i zastávkou mezi Evropou a Severní Amerikou.
                        Celý Erasmus+ pobyt pro mě byl zejména o překonávání sama sebe, vystupování z komfortních zón a komunikaci s lidmi. Umožnil mi nahlédnou na sebe i svou zemi z jiné perspektivy, stejně tak porovnat jak vidím Island já jako cizinec s tím jak ho vidí místní, a obdobně pak Čechy potažmo střední Evropu v očích Islanďanů, či kamarádů z jiných zemí. Snad všichni lidé, s kterými jsem během těch pěti měsíců mluvil, byli velmi přátelští a ochotní, a naučit se oslovovat neznámé lidi kolem mě bylo tak o to jednoduší, že za začátku to byli hlavně studenti jako já. Škola navíc organizovala tzv. Orientation days, kdy jsme měli příležitost se seznámit a poznat základy fungování v Reykjavíku. Pokud jde o jazyk, všichni studenti samozřejmě ovládali angličtinu, i když každý na jiné úrovni. Na Islandu navíc anglicky hovoří i většina populace, podobně jako v jiných severských zemích.
                        Za dobu svého pobytu jsem tak poznal jinak fungující školu, která mě definitivně přesvědčila vstoupit do Studentské unie, získal jsem nerozlučné přátelství při cestě stopem kolem celého ostrova, poznal mnoho věcí o jiné zemi, světě i o sobě a každému bych tento "krok do neznáma" doporučil. Využijte možností, které nám teď svět nabízí. Vlastně, touhle dobou se chystám na další cestu, což bych před 2 lety rozhodně nehádal... A třeba po návratu budete chtít víc prozkoumat i tu naší zemičku.
                    </p>
                </div>
            </div>
        </div>
        <div class="b-detail">
            <div class="grid grid--0 grid--bd">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <div class="b-student">
                        <div class="b-student__wrap">
                            <h2 class="b-student__title">Zuzana Lietavcová</h2>
                        </div>
                    </div>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__content mb0 pt40--m pb40--m pt40--t pb40--t pt50 pb80 fz-lg js-gallery">
                    <h1>Paríž, Francúzsko</h1>
                    <p>
                        Ako študentka magisterského programu Fakulty informačních technologií som sa zúčastnila svojho prvého študijného pobytu v rámci programu Erasmus+
                        na vysokej škole EPITA v Paríži. I keď som sa francúzsky pred odchodom viac-menej vedela dohovoriť, predsa som zvolila štúdium v anglickom jazyku.
                        Moja francúzska škola ponúkala celý študijný program v angličtine, takže sa nejednalo o typicky “erasmové” predmety, výmenných študentov nás na škole bola len hŕstka.
                        To však nezabránilo nájsť si kamarátov (z iných kontinentov a aj medzi miestnymi študentami), s ktorými som mohla spoznávať pôvaby Francúzska. Paríž je typickou turistickou destináciou,
                        avšak stráviť tu pár mesiacov mi umožnilo cítiť sa menej ako len návštevník, pozrieť si pamiatky v menej vyťažených časoch a tiež objavovať zákutia, kde turisti často nezablúdia.
                        Obrovskou výhodou byť študentom v Paríži je vstupné do všetkých štátnych múzeí a pamiatok zdarma (vrátane miest ako Louvre alebo Víťazný oblúk), čo sme aj patrične využívali.
                        Určite odporúčam spraviť si výlety do neďalekej Normandie, ochutnať slané palacinky, crème brûlée a zájsť na syrové fondue. Inak, Parížania podľa mňa vôbec nie sú tak neznesiteľní ľudia,
                        ako sa o nich traduje.
                    </p>
                </div>
            </div>
        </div>

        <div class="b-detail">
            <div class="grid grid--0 grid--bd">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <div class="b-student">
                        <div class="b-student__wrap">
                            <h2 class="b-student__title">Zuzana Lietavcová</h2>
                        </div>
                    </div>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__content mb0 pt40--m pb40--m pt40--t pb40--t pt50 pb80 fz-lg js-gallery">
                    <h1>Hsinchu, Taiwan</h1>
                    <p>
                        Po predošlej skúsenosti so semestrom v Paríží ma lákalo skúsiť štúdium v exotickejšej destinácii a tak som hľadala možnosti. A našla som možnosť študijného pobytu na taiwanskej univerzite NCTU so štipendioum, ktoré som získala z fondov MŠMT. Zrazu som sa ocitla v úplne inom svete s lianami visiacimi zo stromov, miliónmi motoriek, podivnými druhmi ovocia a nerozlúštiteľnými nápismi. Štúdium tu bolo celkom náročné, prekvapilo ma, že najnižšia známka pre úspešné absolvovanie predmetu bola C, ale keďže som si mohla zvoliť predmety, ktoré ma zaujímali, štúdium ma bavilo. Taiwan má neuveriteľné prírodné scenérie na pobreží aj v horách, ľudia sú nesmierne priateľskí a ide o veľmi bezpečnú krajinu. V mestách možno obdivovať početné chrámy a vychutnávať výborný streetfood, ktorý je doslova na každom kroku. Jediný zdroj komplikácií bola neznalosť čínštiny na mojej strane a častá neznalosť angličtiny zo strany domácich, o čo viac sa mi však vždy všetci snažili pomôcť. Jedny z najlepších zážitkov na Taiwane boli cesta okolo ostrova stopom, národný park Taroko, plávanie s korytnačkami, surfovanie a túra na Teapot mountain. Ani za 5 mesiacov som nestihla navštíviť všetky miesta, ktoré treba vidieť a preto sa plánujem vrátiť, aspoň na návštevu.
                    </p>
                </div>
            </div>
        </div>
    </div>



<?php
include '../footer.php';
?>