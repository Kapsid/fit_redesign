<?php
include '../header.php';
?>

    <main id="main" class="main" role="main">
        <div class="b-intro border-b holder holder--lg">
            <h1 class="b-intro__title">Časový plán pro akademický rok 2017/2018</h1>
        </div>

        <div class="c-schedule holder holder--lg">
            <h2 class="title-underlined h1">Všechny ročníky</h2>
            <div class="grid">
                <div class="grid__cell size--t-6-12 mb40--m">
                    <h3 class="c-schedule__subtitle">Zimní semestr</h3>
                    <ul class="c-schedule__list">
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">18.9.2017 - 15.12.2017</time>
                            <p class="c-schedule__label h4">výuka ZS, 13 týdnů (imatrikulace 15.9.)</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">18.12.2017 - 1.1.2018</time>
                            <p class="c-schedule__label h4">vánoční prázdniny</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">2.1.2018 - 2.2.2018</time>
                            <p class="c-schedule__label h4">zkouškové období za ZS</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">19.2.2018</time>
                            <p class="c-schedule__label h4">začátek kontroly studia za ZS</p>
                        </li>
                    </ul>
                </div>

                <div class="grid__cell size--t-6-12 mb20--m">
                    <h3 class="c-schedule__subtitle">Letní semestr</h3>
                    <ul class="c-schedule__list">
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">5.2.2018 - 4.5.2018</time>
                            <p class="c-schedule__label h4">výuka LS, 13 týdnů</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">7.5.2018 - 8.6.2018</time>
                            <p class="c-schedule__label h4">zkouškové období za LS</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">22.6.2018</time>
                            <p class="c-schedule__label h4">konečný termín pro vykonání zkoušek</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">1.7.2018 - 31.8.2018</time>
                            <p class="c-schedule__label h4">letní prázdniny, přeregistrace předmětů</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">25.6.2018</time>
                            <p class="c-schedule__label h4">začátek kontroly studia za LS</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="c-schedule holder holder--lg">
            <h2 class="title-underlined h1">6.semestr bakalářského studijního programu IT</h2>
            <div class="grid">

                <div class="grid__cell size--t-6-12 mb20--m">
                    <h3 class="c-schedule__subtitle">Letní semestr</h3>
                    <ul class="c-schedule__list">
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">12.2.2018</time>
                            <p class="c-schedule__label h4">konečný termín pro přihlášení k SZZ</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">16.5.2018</time>
                            <p class="c-schedule__label h4">odevzdání bakalářských prací</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">11.6.2018 - 14.6.2018</time>
                            <p class="c-schedule__label h4">SZZ, 1 týden</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">26.6.2018 - 27.6.2018</time>
                            <p class="c-schedule__label h4">promoce</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="c-schedule holder holder--lg">
            <h2 class="title-underlined h1">4. semestr magisterského studijního programu IT</h2>
            <div class="grid">

                <div class="grid__cell size--t-6-12 mb20--m">
                    <h3 class="c-schedule__subtitle">Letní semestr</h3>
                    <ul class="c-schedule__list">
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">12.2.2018</time>
                            <p class="c-schedule__label h4">konečný termín pro přihlášení k SZZ</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">23.5.2018</time>
                            <p class="c-schedule__label h4">odevzdání diplomových prací</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">15.6.2018 - 21.6.2018</time>
                            <p class="c-schedule__label h4">SZZ</p>
                        </li>
                        <li class="c-schedule__item">
                            <time class="c-schedule__time font-secondary">28.6.2018 - 29.6.2018</time>
                            <p class="c-schedule__label h4">promoce</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>

<?php
include '../footer.php';
?>