<?php
include '../header.php';

$subjects = [
        "1_win" => $first_winter,
        "1_sum" => $first_summer,
        "2_win" => $second_winter,
        "2_sum" => $second_winter,
        "3_win" => $third_winter,
        "3_sum" => $third_winter,
];
?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class=""><strong>Program: IT-BC-3</strong>, bakalářský, 3-letý</h2>
                    <h3 class=""><strong>Obor:</strong> Informační technologie - BIT (Bc.)</h3>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                    <div class="b-detail__abstract fz-lg">
                        <p>
                            <strong>Akademický rok: 2018/2019</strong><br />

                            Tento studijní plán je určen pro studenty, kteří zahájili studium v ak. r. 2015/16 a později. <br />

                            Student si musí zapsat nejprve opakované povinné a povinně volitelné předměty. Dále si student
                            zapisuje předměty povinné a povinně volitelné pro příslušný ročník. Až pak si může zapsat předměty volitelné pro příslušný ročník nebo
                            z nižších ročníků. Pokud si student pro daný semestr nezapíše všechny povinné a povinně volitelné předměty (vyjma předmětů ITT - Semestrální projekt a IBT - Bakalářská práce), smí si v daném semestru zapsat nejvýše jeden volitelný předmět.
                        </p>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                1.Ročník - zimní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                1.Ročník - letní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                2.Ročník - zimní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td>D<a class="table-link" href="../prostudenty/predmet_detail.php">iskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                2.Ročník - letní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                3.Ročník - zimní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                3.Ročník - letní semestr
                            </h3>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <p>
                            <strong>Ukon:</strong>	Způsob zakončení předmětu: Zk - zkouška, Za - zápočet, Klz - klas. zápočet, Kol - Kolokvium<br />
                            <strong>Pov:</strong>	P - povinný, PV - povinně volitelný, D - doporučený, V - volitelný
                        </p>
                        <p>
                            Pokud je povoleno zapsat si více předmětů skupiny PV než je požadované minimum, pak úspěšně absolvované předměty, které překročí Min. předmětů (resp. Min.kred, pokud není 0) skupiny PV, budou studentovi zařazeny jako předměty povinnosti Přes jako.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>