<?php
include '../header.php';

$subjects = [
    "1_win" => $first_winter,
    "1_sum" => $first_summer,
    "2_win" => $second_winter,
    "2_sum" => $second_winter,
    "3_win" => $third_winter,
    "3_sum" => $third_winter,
];
?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class=""><strong>Program:  IT-MGR-2</strong>, magisterský, 2-letý</h2>
                    <h3 class=""><strong>Obor:</strong> Bezpečnost informačních technologií - MBS</h3>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                    <div class="b-detail__abstract fz-lg">
                        <p>
                            <strong>Akademický rok: 2018/2019</strong><br />
                        </p>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                1.Ročník - zimní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                1.Ročník - letní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                2.Ročník - zimní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td>D<a class="table-link" href="../prostudenty/predmet_detail.php">iskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                2.Ročník - letní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td>D<a class="table-link" href="../prostudenty/predmet_detail.php">iskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                Libovolný ročník - zimní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table-meta table-subjects">
                            <tbody>
                            <h3>
                                Libovolný ročník - letní semestr
                            </h3>
                            <tr>
                                <th>Zkr</th>
                                <th>P</th>
                                <th>Kr</th>
                                <th>Název</th>
                                <th>Ukon</th>
                                <th>Garant</th>
                                <th>Fak</th>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            <tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr><tr class="not-needed">
                                <td>IDA</td>
                                <td>P</td>
                                <td>7</td>
                                <td><a class="table-link" href="../prostudenty/predmet_detail.php">Diskrétní matematika</a></td>
                                <td>Zk</td>
                                <td>Kovár Martin</td>
                                <td>FEKT</td>
                            </tr>
                            </tbody>
                        </table>
                        <p>
                            <strong>Ukon:</strong>	Způsob zakončení předmětu: Zk - zkouška, Za - zápočet, Klz - klas. zápočet, Kol - Kolokvium<br />
                            <strong>Pov:</strong>	P - povinný, PV - povinně volitelný, D - doporučený, V - volitelný
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>