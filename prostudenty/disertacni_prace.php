<?php
include '../header.php';
?>
<main id="main" class="main" role="main">

    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Disertační práce</h2>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        Popis disertačních prací..
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
include '../footer.php';
?>
