<?php
include '../header.php';
?>
<main id="main" class="main" role="main">

    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2 class="">Bakalářské studium</h2>
            </div>

            <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <p>
                        Popis studia..
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="c-faculties holder holder--lg c-faculties--pattern">

        <div class="c-faculties__wrap">
            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/obor.php?spec_type=Bc&spec_name=Informační technologie" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Obor Informační technologie</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../ofakulte/rozvrh_detail.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Rozvrhy</h2>
                    </a>
                </li>
                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                    <a href="../prostudenty/certifikaty.php" class="b-faculty b-faculty--shadow">
                        <h2 class="b-faculty__title h3">Doplňující certifikáty</h2>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="holder holder--lg pt40--m pb40--m pt40--t pb40--t pt70 pb110 border-t">
        <h2 class="text-center mb32--m mb32--t mb40">Kontakty pro studenty</h2>

        <div class="b-vcard">
            <div class="b-vcard__img">
                <img src="../img/illust/dytrych.jpeg" width="207" height="266" alt="">
            </div>
            <div class="b-vcard__content">
                <div class="b-vcard__title">
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">Ing.</span>
                    </p>
                    <h3 class="title title--sm title--secondary">
                        <span class="title__item">Jaroslav Dytrych</span>
                    </h3>
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">Ph.D.</span>
                    </p>
                </div>
                <div class="b-vcard__text">
                    <p>
                        Proděkan pro vzdělávací činnost v bakalářském studiu
                    </p>
                </div>
                <p class="b-vcard__contacts">
                    <a href="tel:731137899" class="link-icon">
                        <span class="icon-svg icon-svg--phone link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                        +420 731 137 899
                    </a><br>
                    <a href="mailto:prodekan-vzdelavani@fit.vutbr.cz" class="link-icon">
                        <span class="icon-svg icon-svg--message link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                        prodekan-vzdelavani@fit.vutbr.cz
                    </a><br />
                    kancelář L221.1
                </p>
            </div>
        </div>
    </div>

    <div class="c-employees">
        <div class="holder holder--lg border-t">
            <div class="c-employees__wrap">
                <ul class="c-employees__list grid grid--60">
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee" style="padding-top: 15px;">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img custom-study-img" style="top: -90px;">
                                    <img src="/img/illust/b-employee--02.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Petr Veigend</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">studijní poradce</p>
                                    <a class="text-center" href="tel:420 54114-1183">+420 54114-1183</a>
                                    <a class="text-center" href="mailto:studijni-poradce@fit.vutbr.cz">studijni-poradce@fit.vutbr.cz</a>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee" style="padding-top: 15px;">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img custom-study-img" style="top: -90px;">
                                    <img src="/img/illust/b-employee--01.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Zuzana Parasková</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">referentka pro 1. ročník bakalářského studia</p>
                                    <a class="text-center" href="tel:+420 54114-1143">+420 54114-1143</a>
                                    <a class="text-center" href="mailto:paraska@fit.vut.cz">paraska@fit.vut.cz</a>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <a href="../ofakulte/profil.php" class="b-employee" style="padding-top: 15px;">
                            <div class="b-employee__wrap">
                                <div class="b-employee__img custom-study-img" style="top: -90px;">
                                    <img src="/img/illust/b-employee--03.jpg" width="100" height="100" alt="">
                                </div>
                                <h3 class="b-employee__name">Iva Soušková</h3>
                                <div class="b-employee__footer">
                                    <p class="b-employee__position font-secondary">referentka pro 2. a vyšší ročníky </p>
                                    <a class="text-center" href="tel:+420 541 141 243">+420 541 141 243</a>
                                    <a class="text-center" href="mailto:souskova@fit.vutbr.cz">souskova@fit.vutbr.cz</a>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</main>
<?php
include '../footer.php';
?>
