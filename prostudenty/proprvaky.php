<?php
include '../header.php';
?>

    <main id="main" class="main pt60 pb60" role="main">
        <div class="holder holder--lg">
            <div class="sg-box">
                <h1 class="sg-box__title first-title">První semestr na FIT? <br/> To zvládneme!</h1>

                <div class="sg-box__item">
                    <div class="sg-box__item-code sg-box__item-code--bleed">
                        <div class="c-news holder holder--lg">
                            <h2 class="c-news__title h1">Na co nesmíte zapomenout</h2>

                            <ul class="c-news__list grid grid--50">
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">XX červen 2018</time>
                                            <h3 class="b-news__title">Zápis</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Splnil/a jste podmínky pro přijetí na FIT? Gratulujeme! Nezapomeňte
                                                    se zapsat - bez toho se nemůžete stát studentem FIT. Jak a co s
                                                    sebou? To zjistíte ZDE
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">Po zápise</time>
                                            <h3 class="b-news__title">Portál VUT a IS FIT</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Pomocí e-přihlášky si aktivujte účet na portálu VUT. Nahrajte do něj
                                                    barevnou fotografii a objednejte si průkaz studenta nebo ISIC.
                                                    Pomocí účtu na portálu VUT na stránce
                                                    https://wis.fit.vutbr.cz/FIT/info získáte heslo do IS FIT.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12 push--t-3-12 push--auto">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">Po zápise</time>
                                            <h3 class="b-news__title">Žádost o ubytování</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Pokud chcete bydlet na kolejích, po zápise nezapomeňte podat žádost
                                                    o ubytování. Více informací najdete ZDE.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">11. září 2018</time>
                                            <h3 class="b-news__title">Zápis volitelných předmětů </h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Všechny povinné předměty se vám zapíší automaticky. Od 11.9. se v
                                                    informačním systému otevře registrace volitelných předmětů.
                                                    Podívejte se do studijního plánu a zkuste si vybrat. Spěchat s tím
                                                    nemusíte, bude otevřena až do 24.9. a v seminářích je míst dostatek.
                                                    Nezapomeňte na registraci cvičení k povinným předmětům, která začne
                                                    14.9.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">14. září 2018</time>
                                            <h3 class="b-news__title">Imatrikulace</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Imatrikulace, která probíhá poslední pátek před zahájením výuky v
                                                    semestru, je slavnostní akademický obřad přijetí studenta ke studiu.
                                                    To si nenechte ujít! Po imatrikulaci následují školení, která vám
                                                    pomohou zorientovat se v informačním systému VUT a FIT a s
                                                    pochopením registrací předmětů a cvičení.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12 push--t-3-12 push--auto">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">14. září 2018</time>
                                            <h3 class="b-news__title">Registrace cvičení</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    V pátek večer začínají registrace cvičení z ISC, ISM, IFS i
                                                    Diskrétní matematiky a Základů programování. Končí v neděli večer,
                                                    v pondělí ráno už začíná výuka. Ujistěte se, že máte registrované
                                                    vše, co chcete, a nemáte rozvrhové kolize. Pozdější změny již budou
                                                    obtížné.
                                                    Vše vám vysvětlíme na školeních po imatrikulaci.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12 push--t-3-12 push--auto">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">15. - 16. září 2018</time>
                                            <h3 class="b-news__title">Start@FIT</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Bojíte se, že se na FIT nevyznáte? Tahle uvítací akce pro prváky vám
                                                    pomůže zorientovat se. Setkáte se svými novými spolužáky i učiteli,
                                                    prohlédnete si fakultu, zjistíte, jak to na FIT chodí, ve
                                                    studentském klubu si můžete zahrát deskovky nebo si dát pivo a večer
                                                    zajít na koncert.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">17. září 2018</time>
                                            <h3 class="b-news__title">Start semestru</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    V pondělí 17.9. začíná první vyučovací týden zimního semestru.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">První týden semestru</time>
                                            <h3 class="b-news__title">Přeregistrace předmětů</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Zrušení předmětů, které nevyhovují - registrace nových předmětů
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12 push--t-3-12 push--auto">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">30. listopadu 2018</time>
                                            <h3 class="b-news__title">Rozřazovací test z Angličtiny</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Na letní semestr je vhodné si zaregistrovat angličtinu. Rozřazovací
                                                    test z angličtiny ve STUDISu musíte absolvovat do 30. 11. 2018,
                                                    jinak v prvním roce nebudete mít možnost registrace angličtiny. Kdo
                                                    chce uznat angličtinu (FCE, CAE, ...), postupuje dle vyhlášky FEKT.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <time class="b-news__date font-secondary">15. prosince 2018</time>
                                            <h3 class="b-news__title">Konec semestru</h3>
                                            <div class="b-news__perex">
                                                <p>
                                                    Končí výuka zimního semestru.
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="sg-box__item">
                    <div class="sg-box__item-code sg-box__item-code--bleed">
                        <div class="c-programs holder holder--lg pb75--d">
                            <div class="row-main row-main--md">
                                <h1 class="c-programs__title">Kde a co řešit?</h1>

                                <div class="c-programs__wrap">
                                    <ul class="c-programs__list grid grid--t-40 grid--80 js-macy">
                                        <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                            <div class="b-program b-program--center">
                                                <h2 class="b-program__title h3">Studijní oddělení</h2>
                                                <div class="b-program__content pt30 pb50">
                                                    <p>
                                                        Když potřebujete potvrzení o studiu, vydání nebo revalidaci
                                                        průkazu, podepsat žádost, ukončit studium.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                            <div class="b-program b-program--center">
                                                <h2 class="b-program__title h3">Studijní poradenství</h2>
                                                <div class="b-program__content pt30 pb50">
                                                    <p>
                                                        Když máte nějaký problém, nevyznáte se, chcete poradit s
                                                        předměty.
                                                    </p>
                                                    <p class="mt30">
                                                        <a href="http://www.fit.vutbr.cz/study/advisor/"
                                                           class="btn btn--outline btn--secondary btn--sm">
                                                            <span class="btn__text">Více informací</span>
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                            <div class="b-program b-program--center">
                                                <h2 class="b-program__title h3">Studentská Unie</h2>
                                                <div class="b-program__content pt30 pb50">
                                                    <p>
                                                        Když potřebujete něco řešit s vedením školy, řešení systémových
                                                        problémů. Když se chceš zapojit do života na FIT.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                            <div class="b-program b-program--center">
                                                <h2 class="b-program__title h3">Pedagogové</h2>
                                                <div class="b-program__content pt30 pb50">
                                                    <p>
                                                        Nerozumíte látce? Nemůžete na zkoušku? Máte nějaký jiný problém
                                                        související s předmětem? Nebojte se napsat vyučujícímu nebo
                                                        garantovi předmětu.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>

<?php
include '../footer.php'
?>