<?php
include '../header.php';

$internships = [
    ['name' => 'Nabídka 1 stáž', 'partnerName' => 'Avast Software s.r.o.', 'link' => 'http://www.fit.vutbr.cz/cooperation/partner1.php?id=1998&nabidka=100&file=itsec-plakat.pdf','text' => 'Popis tématu', 'mail' => 'beranv@fit.vutbr.cz']
];

$offers = [
  ['link' => 'http://www.cadwork.ch/indexL1a.jsp', 'partnerName' => 'Cadwork Informatik CI AG', 'title' => 'Pozice programátor C/C++', 'text' => 'tady bude popis', 'contact' => 'neco@nekde'],
    ['link' => 'https://www.avast.com/cs-cz/index#mac', 'partnerName' => 'Avast Software s.r.o.', 'title' => 'Nabídka 2 - Místo', 'text' => 'popis 2', 'contact' => 'beranv@fit.vutbr.cz'],
];
?>

<div class="b-detail">
    <div class="">
        <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
            <h2 class="">Stáže a nabídky pracovních pozic</h2>
        </div>
        <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
            <h3>Stáže</h3>
            <p>
                Stáž je krátkodobější honorovaná spolupráce s průmyslovým Partnerem (tým pracující na vývoji produktů nebo aplikování technologií do produktů)
                nebo s FIT (týmem pracujícím na vývoji v grantovém projektu nebo smluvním výzkumu, výzkumná skupina provádějící výzkum nových technologií a postupů,
                experimentální činnosti v laboratořích apod.). Ideální období realizace je období červen-září. Stáž je vhodná k získání odborných praktických zkušeností.
                <strogn>Primárním cílem stáže je rozvoj studenta: získání zkušeností z různých pracovních prostředí, osvojení si měkkých dovedností prací v týmu a získání zkušeností z řešení reálných problémů.</strogn>
            </p>
            <ul>
                <?php foreach($internships as $internship){
                  echo "<li><a href='../prostudenty/partner_detail.php'>{$internship['partnerName']}</a>, <a href='{$internship['link']}'>{$internship['name']}</a>
                            <p>{$internship['text']}</p>
                            Kontakt: <a href='mailto:{$internship['mail']}'>{$internship['mail']}</a>
                        </li>";
                }
                ?>
            </ul>
        </div>
        <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
            <h3>Nabídky pracovních pozic</h3>
            <p>
                Nabídka pracovních pozic slouží jak absolventům v IT oborech, tak širší odborné veřejnosti a odborníkům v praxi,
                kteří hledají uplatnění v technologicky zajímavých firmách, kde mohou profesionálně lépe růst, využít svých dosavadních zkušeností a být <strong>přínosem pro dlouhodobý rozvoj firmy</strong>.
            </p>
            <ul>
                <?php foreach($offers as $offer){
                    echo "<li><a href='{$offer['link']}'>{$offer['partnerName']}</a>, {$offer['title']}
                            <p>{$offer['text']}</p>
                            Kontakt: <a href='mailto:{$offer['contact']}'>{$offer['contact']}</a>
                        </li>";
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<?php
include '../footer.php';
?>
