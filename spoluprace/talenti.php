<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-hero-header">
        <div class="b-hero-header__img" style="background-image: url('/img/illust/b-hero-header--14.jpg');"></div>
        <div class="b-hero-header__content b-hero-header__content--bottom holder holder--lg text-left">
            <p class="mb0">
                <a href="../spoluprace/firemnispoluprace.php" class="backlink">
				<span class="icon-svg icon-svg--angle-l backlink__icon">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-angle-l" x="0" y="0" width="100%"
                             height="100%"></use>
                    </svg>
                </span>

                    Spolupráce
                </a>
            </p>
            <h1 class="title b-hero-header__title">
                <span class="title__item">Najděte</span><span class="title__item">si</span><span class="title__item">kolegy</span><br><span
                        class="title__item">a</span><span class="title__item">zaměstnance</span><span
                        class="title__item">na</span><span class="title__item">FITu</span>
            </h1>
        </div>
    </div>

    <div class="b-annot">
        <div class="grid grid--0">
            <div class="grid__cell size--t-7-12 holder holder--lg b-annot__content fz-lg">
                <p>
                    Fakulta informačních technologií je vybavena nejnovějšími technologiemi, špičkovým výzkumem a
                    vyučujícími, kteří patří mezi uznávané experty ve svém oboru.
                    Důraz klademe nejen na kvalitní teoretickou přípravu, ale také na důležitost provázání s praxí.
                    Proto spolupracujeme s klíčovými podniky v oboru. I díky
                    těmto znalostem patří naši studenti a absolventi k nejžádanějším na trhu práce. Studuje u nás ~2400
                    studentů ve všech programech a formách studia. Staňte se Partnery FIT a podchyťte si budoucí
                    zaměstnance už při studiu. </p>
                <p>
                    <a href="#" class="btn btn--sm btn--secondary btn--outline">
                        <span class="btn__text">Možnosti solupráce</span>
                    </a>
                </p>
            </div>

            <div class="grid__cell size--t-5-12 holder holder--lg b-annot__content b-annot__content--pattern">
                <p class="b-annot__brief">
                    Naši absolventi patří<br>
                    díky znalostem ke<br>
                    špičce ve svém oboru
                </p>
            </div>
        </div>
    </div>

    <div class="c-highlights border-t border-b">
        <ul class="c-highlights__list grid grid--bd grid--0">
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">~530</p>
                    <div class="b-highlight__text">
                        <p>studentů každý rok úspěšně dokončí studium</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">79 %</p>
                    <div class="b-highlight__text">
                        <p>studentů získá praktické zkušenosti v oboru již při studiu</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">99 %</p>
                    <div class="b-highlight__text">
                        <p>absolventů má zajištěnou práci do tří měsíců od ukončení studia</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">33 000 Kč</p>
                    <div class="b-highlight__text">
                        <p>Průměrný nástupní plat absoventů</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="c-fields holder holder--lg">
        <h2 class="c-fields__title mb55--d">V jakých oborech vychováváme budoucí hvězdy</h2>

        <div class="grid grid--80 mb15--m">
            <div class="grid__cell size--t-6-12 size--4-12">
                <ul class="c-fields__list">
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Řečové technologie
                        </h3>
                    </li>
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Umělá inteligence
                        </h3>
                    </li>
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Robotika
                        </h3>
                    </li>
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Počítačové systémy
                        </h3>
                    </li>
                </ul>
            </div>

            <div class="grid__cell size--t-6-12 size--4-12">
                <ul class="c-fields__list">
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Bezpečnost IT
                        </h3>
                    </li>
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Evoluční algoritmy
                        </h3>
                    </li>
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Vestavěná zařízení
                        </h3>
                    </li>
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Ultrazvukové rekonstrukce
                        </h3>
                    </li>
                </ul>
            </div>

            <div class="grid__cell size--t-6-12 size--4-12">
                <ul class="c-fields__list">
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Formální analýza a verifikace
                        </h3>
                    </li>
                    <li class="c-fields__item">
                        <h3 class="c-fields__name font-primary">
                            Superpočítačové technologie
                        </h3>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="c-fields holder holder--lg">
        <h2 class="c-fields__title mb55--d">Partnerství s FIT</h2>

        <a href="#" class="btn btn--secondary btn--xs btn--outline"
           style="float: none; margin: 0 auto; display: block; width: 300px;">
            <span class="btn__text">Staňte se Partnerem FIT</span>
        </a>
    </div>

    <div class="c-programs holder holder--lg pt90--d pb90--d border-t">
        <div class="row-main row-main--md">
            <h2 class="c-programs__title">Možnosti spolupráce pro Partnery FIT</h2>

            <div class="c-programs__wrap">
                <ul class="c-programs__list grid grid--t-40 grid--80 js-macy">
                    <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <div class="b-program b-program--center">
                            <h3 class="b-program__title">Vedení závěrečné práce</h3>
                            <div class="b-program__content">
                                <p>Zadejte našim studentům téma bakalářské či diplomové práce.</p>
                            </div>
                            <p class="b-program__btn">
                                <a href="../spoluprace/detail_spoluprace.php?type=zaverecnePrace" class="btn btn--outline btn--secondary btn--sm">
                                    <span class="btn__text">Zjistit více</span>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <div class="b-program b-program--center">
                            <h3 class="b-program__title">Projekty se studenty</h3>
                            <div class="b-program__content">
                                <p>Získejte kontakt na šikovné studenty a zapojte je do spolupráce na projektu.</p>
                            </div>
                            <p class="b-program__btn">
                                <a href="../spoluprace/detail_spoluprace.php?type=projekty" class="btn btn--outline btn--secondary btn--sm">
                                    <span class="btn__text">Zjistit více</span>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <div class="b-program b-program--center">
                            <h3 class="b-program__title">Účast na výuce</h3>
                            <div class="b-program__content">
                                <p>Cítíte potřebu vzdělat studenty v určitém konkrétním oboru? Zapojte se do výuky.</p>
                            </div>
                            <p class="b-program__btn">
                                <a href="../spoluprace/detail_spoluprace.php?type=vyuka" class="btn btn--outline btn--secondary btn--sm">
                                    <span class="btn__text">Zjistit více</span>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <div class="b-program b-program--center">
                            <h3 class="b-program__title">Odborné semináře</h3>
                            <div class="b-program__content">
                                <p>Prezentujte studentům moderní technologie, kterými se ve firmě zabýváte.</p>
                            </div>
                            <p class="b-program__btn">
                                <a href="../spoluprace/detail_spoluprace.php?type=seminare" class="btn btn--outline btn--secondary btn--sm">
                                    <span class="btn__text">Zjistit více</span>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <div class="b-program b-program--center">
                            <h3 class="b-program__title">Podpora konferencí a soutěží</h3>
                            <div class="b-program__content">
                                <p>Chcete dostat vaši firmu do povědomí studentů? Podpořte sponzorstvím některou z námi
                                    pořádaných akcí.</p>
                            </div>
                            <p class="b-program__btn">
                                <a href="../spoluprace/detail_spoluprace.php?type=konference" class="btn btn--outline btn--secondary btn--sm">
                                    <span class="btn__text">Zjistit více</span>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <div class="b-program b-program--center">
                            <h3 class="b-program__title">Nabídka pracovních stáží</h3>
                            <div class="b-program__content">
                                <p>Chcete studentům ukázat, jak to chodí v IT průmyslu a vaší firmě? Nabídněte jim
                                    pracovní stáž.</p>
                            </div>
                            <p class="b-program__btn">
                                <a href="../spoluprace/detail_spoluprace.php?type=staze" class="btn btn--outline btn--secondary btn--sm">
                                    <span class="btn__text">Zjistit více</span>
                                </a>
                            </p>
                        </div>
                    </li>
                    <li class="c-programs__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                        <div class="b-program b-program--center">
                            <h3 class="b-program__title">Pořádání soutěží a hackatonů</h3>
                            <div class="b-program__content">
                                <p>Máte nápad na téma soutěže pro studenty?</p>
                            </div>
                            <p class="b-program__btn">
                                <a href="../spoluprace/detail_spoluprace.php?type=hackathon" class="btn btn--outline btn--secondary btn--sm">
                                    <span class="btn__text">Zjistit více</span>
                                </a>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="holder holder--lg pt40--m pb40--m pt40--t pb40--t pt70 pb110 border-t">
        <h2 class="text-center mb32--m mb32--t mb40">Nemůžete si vybrat? Kontaktujte nás</h2>

        <div class="b-vcard">
            <div class="b-vcard__img">
                <img src="../img/illust/beran.jpeg" width="207" height="266" alt="">
            </div>
            <div class="b-vcard__content">
                <div class="b-vcard__title">
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">Ing.</span>
                    </p>
                    <h3 class="title title--sm title--secondary">
                        <span class="title__item">Vítězslav Beran</span>
                    </h3>
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">Ph.D.</span>
                    </p>
                </div>
                <div class="b-vcard__text">
                    <p>
                        Proděkan pro vnější vztahy
                    </p>
                </div>
                <p class="b-vcard__contacts">
                    <a href="tel:+420777286075" class="link-icon">
                        <span class="icon-svg icon-svg--phone link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                        +420 777 286 075
                    </a><br>
                    <a href="mailto:beranv@fit.vutbr.cz" class="link-icon">
                        <span class="icon-svg icon-svg--message link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                        beranv@fit.vutbr.cz
                    </a>
                </p>
            </div>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
