<?php
include '../header.php';
?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class="">Středoškolská odborná činnost</h2>
                </div>

                <div class="grid__cell size--t-8-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                    <div class="b-detail__abstract fz-lg">
                        <p>
                            Ve spolupráci s Jihomoravským centrem pro mezinárodní mobilitu je VUT v Brně zapojeno do projektu Středoškolské odborné činnosti (SOČ).
                            Podstatou je nabídka témat odborných prací určená studentům středních škol. Ti se díky zpracování takové práce mají možnost dostat do kontaktu s
                            odborným školitelem, podívat se do moderně vybavených laboratoří na univerzitě a dostat se k potřebným nástrojům, vzorkům a zařízením. Na vypracování
                            své odborné práce získají i finanční příspěvek. Ti nejúspěšnější pak mohou být u vybraných fakult zvýhodněni při přijímacím řízení.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>