<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-hero-header ">
        <div class="b-hero-header__img " style="background-image: url('/img/illust/b-hero-header--13.jpg');"></div>
        <div class="b-hero-header__content holder holder--lg">
            <h1 class="title b-hero-header__title">
                <span class="title__item">Jsme</span><span class="title__item">otevřeni</span><span class="title__item">spolupráci</span><br><span
                        class="title__item">s</span><span class="title__item">firemním</span><span class="title__item">sektorem</span>
            </h1>
            <a href="#content" data-slide="#content" class="b-hero-header__next b-hero-header__next--bleed">
			<span class="icon-svg icon-svg--angle-d ">
                <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="/img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
                </svg>
            </span>

                <span class="vhide">Další</span>
            </a>
        </div>
    </div>

    <div id="content"></div>

    <div class="c-attrs holder holder--lg bg pt40--m pt0 pb60--d">
        <ul class="c-attrs__list c-attrs__list--bleed grid grid--0 grid--bd">
            <li class="c-attrs__item grid__cell grid__cell--grow size--t-4-12">
                <a href="../spoluprace/talenti.php" class="b-course">
				<span class="icon-svg icon-svg--fekt b-course__icon color-secondary">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-fekt" x="0" y="0" width="100%" height="100%"></use>
                    </svg>
                </span>

                    <h2 class="b-course__title">Podchyťte si talenty</h2>
                    <div class="b-course__annot">
                        <p>
                            Zapojte se do výuky, vedení diplomových prací nebo se zúčastněte některé z našich akcí.
                            Buďte na VUT vidět, ať o vás naši studenti ví.
                        </p>
                    </div>
                    <p class="b-course__btn">
								<span class="btn btn--outline btn--sm">
									<span class="btn__text">Zjistit více</span>
								</span>
                    </p>
                </a>
            </li>
            <li class="c-attrs__item grid__cell grid__cell--grow size--t-4-12">
                <a href="../spoluprace/partnerstvi_vyzkum.php" class="b-course">
				<span class="icon-svg icon-svg--research b-course__icon color-secondary">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-research" x="0" y="0" width="100%"
                             height="100%"></use>
                    </svg>
                </span>

                    <h2 class="b-course__title">Partnerství ve výzkumu</h2>
                    <div class="b-course__annot">
                        <p>
                            Naše laboratoře, výzkumná centra i špičkové vědecké týmy vám mohou pomoci posunout vaše
                            podnikání, testovat výrobky nebo aplikovat do praxe některé z našich poznatků.
                        </p>
                    </div>
                    <p class="b-course__btn">
                    <span class="btn btn--outline btn--sm">
                        <span class="btn__text">Zjistit více</span>
                    </span>
                    </p>
                </a>
            </li>
            <li class="c-attrs__item grid__cell grid__cell--grow size--t-4-12">
                <a href="#" class="b-course">
				<span class="icon-svg icon-svg--building b-course__icon color-secondary">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-building" x="0" y="0" width="100%"
                             height="100%"></use>
                    </svg>
                </span>

                    <h2 class="b-course__title">Služby fakulty</h2>
                    <div class="b-course__annot">
                        <p>
                            Ať už hledáte pronájmový prostor pro konání konference nebo školení, ubytovací kapacity nebo
                            pronájem sportovišť, z naší nabídky si vyberete.
                        </p>
                    </div>
                    <p class="b-course__btn">
                    <span class="btn btn--outline btn--sm">
                        <span class="btn__text">Zjistit více</span>
                    </span>
                    </p>
                </a>
            </li>
        </ul>
    </div>

    <div class="c-highlights border-t border-b">
        <div class="holder holder--lg border-b pt40">
            <h2 class="c-highlights__title">FIT je tou správnou volbou</h2>
        </div>

        <ul class="c-highlights__list grid grid--bd grid--0">
            <li class="c-highlights__item grid__cell size--t-6-12 size--2-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">43</p>
                    <div class="b-highlight__text">
                        <p>průmyslových partnerů</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--2-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">50</p>
                    <div class="b-highlight__text">
                        <p>projektů v rámci smluvního výzkumu</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">60</p>
                    <div class="b-highlight__text">
                        <p>obhájených studentských kvalifikačních prací ve spolupráci s partnery</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">30 mil. Kč</p>
                    <div class="b-highlight__text">
                        <p>je objem za projekty smluvního výzkumu</p>
                    </div>
                </div>
            </li>
            <li class="c-highlights__item grid__cell size--t-6-12 size--2-12">
                <div class="b-highlight holder">
                    <p class="b-highlight__title font-secondary">~500</p>
                    <div class="b-highlight__text">
                        <p>studentů každý rok úspěšně dokončí studium (Bc., Ing., Ph.D.)</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="c-fields holder holder--lg">
        <h2 class="c-fields__title mb55--d">Partnerství s FIT</h2>

        <a href="#" class="btn btn--secondary btn--xs btn--outline"
           style="float: none; margin: 0 auto; display: block; width: 300px;">
            <span class="btn__text">Staňte se Partnerem FIT</span>
        </a>
    </div>

    <div class="c-articles holder holder--lg">
        <div class="row-main row-main--md">
            <h2 class="c-articles__title h3">Partneři FIT</h2>
            <div class="c-articles__wrap">
                <div class="logos">
                    <ul class="grid grid grid--1">
                        <li class="helper grid__cell size--t-2-12 size--3-12">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img"
                                                                                 src="../img/logos/avast.png"
                                                                                 alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-3-12">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img"
                                                                              src="../img/logos/cadwork.png" alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-2-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img" src="../img/logos/camea.png"
                                                                   alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-2-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img" src="../img/logos/Cesnet.gif"
                                                                 alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-2-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img" src="../img/logos/codasip.png"
                                                                   alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-2-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img" src="../img/logos/cz.nic.png" alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-3-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img"
                                                                                     src="../img/logos/Honeywell_Primary_Logo_RGB.jpg"
                                                                                     alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-2-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img"
                                                                        src="../img/logos/PHONEXIA.png" alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-2-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img" src="../img/logos/RED HAT.png"
                                                                     alt=""></a>
                        </li>
                        <li class="helper grid__cell size--t-2-12 border-t--t">
                            <a href="../prostudenty/partner_detail.php"><img class="partner-img" src="../img/logos/VR Group.png"
                                                              alt=""></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="holder holder--lg pt40--m pb40--m pt40--t pb40--t pt70 pb110 border-t">
        <h2 class="text-center mb32--m mb32--t mb40">Veďte diplomku na FITu. Kontaktujte nás.</h2>

        <div class="b-vcard">
            <div class="b-vcard__img">
                <img src="../img/illust/beran.jpeg" width="207" height="266" alt="">
            </div>
            <div class="b-vcard__content">
                <div class="b-vcard__title">
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">Ing.</span>
                    </p>
                    <h3 class="title title--sm title--secondary">
                        <span class="title__item">Vítězslav Beran</span>
                    </h3>
                    <p class="title title--xs title--secondary-darken">
                        <span class="title__item">Ph.D.</span>
                    </p>
                </div>
                <div class="b-vcard__text">
                    <p>
                        Proděkan pro vnější vztahy
                    </p>
                </div>
                <p class="b-vcard__contacts">
                    <a href="tel:+420777286075" class="link-icon">
                        <span class="icon-svg icon-svg--phone link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                        +420 777 286 075
                    </a><br>
                    <a href="mailto:beranv@fit.vutbr.cz" class="link-icon">
                        <span class="icon-svg icon-svg--message link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                        beranv@fit.vutbr.cz
                    </a>
                </p>
            </div>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
