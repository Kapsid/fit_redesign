<?php
include '../header.php';
?>
    <main id="main" class="main" role="main">
        <div class="b-detail">
            <div class="">
                <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                    <h2 class="">Střední školy a FIT</h2>
                </div>


                <div class="c-articles-lg ">
                    <div class="holder holder--lg">
                        <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                            <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                <a href="../ofakulte/kalendar.php" class="b-faculty b-faculty--shadow">
                                    <h2 class="b-faculty__title h3">Kurzy pro studenty SŠ</h2>
                                </a>
                            </li>
                            <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                <a href="http://holky.fit.vutbr.cz/ " class="b-faculty b-faculty--shadow">
                                    <h2 class="b-faculty__title h3">Letní škola (F)IT pro holky</h2>
                                </a>
                            </li>
                            <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                <a href="../spoluprace/stredoskolskacinnost.php" class="b-faculty b-faculty--shadow">
                                    <h2 class="b-faculty__title h3">Středoškolská odborná činnost</h2>
                                </a>
                            </li>
                            <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                <a href="../ofakulte/akce_detail.php" class="b-faculty b-faculty--shadow">
                                    <h2 class="b-faculty__title h3">Excel@FIT</h2>
                                </a>
                            </li>
                            <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                <a href="../studujfit/den-otevrenych-dveri.php" class="b-faculty b-faculty--shadow">
                                    <h2 class="b-faculty__title h3">Dny otevřených dveří</h2>
                                </a>
                            </li>
                            <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                <a href="https://www.vutbr.cz/junior" class="b-faculty b-faculty--shadow">
                                    <h2 class="b-faculty__title h3">VUT Junior</h2>
                                </a>
                            </li>
                            <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                <a href="../ofakulte/akce_detail.php" class="b-faculty b-faculty--shadow">
                                    <h2 class="b-faculty__title h3">Pro učitele</h2>
                                </a>
                            </li>
                        </ul>
                    </div>
            </div>
        </div>
        <div class="holder holder--lg pt40--m pb40--m pt40--t pb40--t pt70 pb110 border-t">
            <h2 class="text-center mb32--m mb32--t mb40">Veďte diplomku na FITu. Kontaktujte nás.</h2>

            <div class="b-vcard">
                <div class="b-vcard__img">
                    <img src="" width="207" height="266" alt="">
                </div>
                <div class="b-vcard__content">
                    <div class="b-vcard__title">
                        <p class="title title--xs title--secondary-darken">
                            <span class="title__item">Ing.</span>
                        </p>
                        <h3 class="title title--sm title--secondary">
                            <span class="title__item">Jaroslav Rozman</span>
                        </h3>
                        <p class="title title--xs title--secondary-darken">
                            <span class="title__item">Ph.D.</span>
                        </p>
                    </div>
                    <div class="b-vcard__text">
                        <p>
                            Odborný asistent
                        </p>
                    </div>
                    <p class="b-vcard__contacts">
                        <a href="tel:+420 54114-1190" class="link-icon">
                    <span class="icon-svg icon-svg--phone link-icon__icon">
                        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%"
                                 height="100%"></use>
                        </svg>
                    </span>

                            +420 54114-1190
                        </a><br>
                        <a href="mailto:rozmanj@fit.vutbr.cz" class="link-icon">
                    <span class="icon-svg icon-svg--message link-icon__icon">
                        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%"
                                 height="100%"></use>
                        </svg>
                    </span>

                            rozmanj@fit.vutbr.cz
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </main>
<?php
include '../footer.php'
?>