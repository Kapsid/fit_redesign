<?php
include '../header.php';

$thesis = [
   'heading' => 'VEDENÍ ZÁVĚREČNÝCH PRACÍ',
    'text' => 'Zadejte našim studentům takové téma závěrečné práce, které pomůže posunout vaši firmu na lepší technologickou úroveň. Práci můžete vést ve spolupráci s akademickým pracovníkem. Studenti rádi pracují na tématech s reálným dopadem do praxe. Navíc tak získají možnost poznat osobně prostředí vaší firmy a třeba se z nich v budoucnu stanou vaši kolegové.',
    'perex' => 'Navažte se studenty spolupráci a vytipujte si budoucí zaměstnance.',
    'first_heading' => 'První schůzka',
    'first_text' => 'Partneři FIT, kontaktujte svého odborného garanta na FIT a domluvte si schůzku. Na schůzce zjistíte možnosti vypisování témat diplomových a bakalářských prací a jak na ně získat šikovné studenty.',
    'second_heading' => 'Vytvoření zadání',
    'second_text' => 'Pošlete nám zadání včetně stručné anotace, jaký problém se má řešit a jaké mají být výstupy řešení práce. Zadání po přidělení na vhodný garantující ústav vypisujeme do informačního systému, kde jej vidí studenti a mohou si ho vybrat.',
    'third_heading' => 'Přidělení studenta',
    'third_text' => 'Student, který si téma vybere, vás bude kontaktovat. Následně již bude řešit samotný problém včetně docházení do vaší společnosti ať na konzultace, tak na práci ve vašich laboratořích a provozech.',
];

$projects = [
    'heading' => 'PROJEKTY SE STUDENTY',
    'text' => 'FIT nabízí spolupráci na společných výzkumných projektech nebo zakázkách, kde členy výzkumného týmu mohou být vybraní talentovaní studenti ze všech úrovní studia. Studenti pracují v rámci týmu na vybraném dílčím výzkumném úkolu. Velkou motivací je pak práce na reálném zadání s možností si přivydělat formou mimořádných stipendií již během studia!',
    'perex' => 'Vyzkoušejte si spolupráci se studenty přímo v praxi.',
    'first_heading' => 'První schůzka',
    'first_text' => 'Partneři FIT, kontaktujte svého odborného garanta na FIT a domluvte si schůzku. Na schůzce zjistíte možnosti spolupráce ve výzkumu a jak do něj zapojit vhodné studenty.',
    'second_heading' => 'Vytvoření zadání',
    'second_text' => 'Definuje se jasné zadání projektu, čímž jsme schopni vytipovat jednotlivé členy výzkumného týmu a oslovíme je, zda mají o spolupráci zájem.',
    'third_heading' => 'Uzavření smlouvy',
    'third_text' => 'Obě strany podepíší smlouvu na výzkumný projekt či zakázku a už nic nebrání tomu zahájit práce na projektu.',
];

$lessons = [
    'heading' => 'ÚČAST NA VÝUCE',
    'text' => 'Zajímá nás váš názor na odborný profil našich absolventů. Proto máte možnost se aktivně podílet na výuce vybraných odborných předmětů. Navrhněte témata pro odborné profilování našich studentů nebo se přímo zapojte do výuky formou zvaných přednášek, odborných seminářů nebo workshopů.',
    'perex' => 'Přispějte k tomu, aby se vaše odborné požadavky dostaly do výuky.',
    'first_heading' => 'První schůzka',
    'first_text' => 'Partneři FIT, kontaktujte svého odborného garanta na FIT a domluvte si schůzku. Zjistíte na ní, jaké jsou možnosti spolupráce na výuce pro konkrétní obor, který vás zajímá.',
    'second_heading' => 'Definice tématu',
    'second_text' => 'Navrhněte témata a požadavky na znalosti našich absolventů. Společně nalezneme nejvhodnější způsob, jak danou problematiku prezentovat studentům.',
    'third_heading' => 'Zavedení do výuky',
    'third_text' => 'Ve vzájemné kooperaci předneseme vaše témata studentům, ať už formou přednášek nebo cvičení ve vybraných předmětech, nebo v rámci speciálních workshopů.',
];

$additionalLessons = [
    'heading' => 'ODBORNÉ SEMINÁŘE',
    'text' => 'Jaké jsou odborné výzvy a potřeby v různých IT oblastech, které ve firmě řešíte? Jaké technologie a zařízení k tomu používáte? Představte studentům FIT aktuální odborné problémy IT průmyslu, které vnímáte dnes či s přesahem do budoucna.',
    'perex' => 'Ukažte studentům, co obnáší IT průmysl.',
    'first_heading' => 'Nabídka tématu',
    'first_text' => 'Partneři FIT, kontaktujte svého odborného garanta na FIT a domluvte si schůzku. Na schůzce zjistíte možnosti spolupráce na výuce pro konkrétní obor, který vás zajímá.',
    'second_heading' => 'Definice tématu',
    'second_text' => 'Navrhněte témata a požadavky na znalosti našich absolventů. Společně nalezneme nejvhodnější způsob, jak danou problematiku prezentovat studentům.',
    'third_heading' => 'Zavedení do výuky',
    'third_text' => 'Ve vzájemné kooperaci předneseme vaše témata našim studentům, ať už formou přednášek nebo cvičení ve vybraných předmětech,  nebo v rámci speciálních workshopů.',
];

$conferences = [
    'heading' => 'PODPORA KONFERENCÍ A SOUTĚŽÍ',
    'text' => 'Podpořte tvůrčí rozvoj našich studentů a staňte se sponzorem studentských konferencí, soutěží nebo workshopů, které na fakultě každoročně pořádáme. Získáte přehled o tom, co naši studenti řeší, jaké mají znalosti a budete si moci vybírat své potenciální zaměstnance z těch nejlepších, které na fakultě máme!',
    'perex' => 'Učiňte vaši firmu viditelnější pro naše studenty.',
    'first_heading' => 'První schůzka',
    'first_text' => 'Partneři FIT, kontaktujte proděkana pro vnější vztahy a domluvte si schůzku. Na schůzce zjistíte, jaké studentské konference, soutěže nebo workshopy pořádáme a jak se do nich můžete zapojit.',
    'second_heading' => 'Způsob zapojení do akce',
    'second_text' => 'Probereme spolu možnosti vaší účasti na vybrané akci tak, abyste si vybrali tu nejvíce vyhovující variantu.',
    'third_heading' => 'Plánování akce',
    'third_text' => 'Vyřešíme všechny papírové formality a můžete začít plánovat, koho na akci vyšlete nebo jak se na ní budete prezentovat.',
];

$works = [
    'heading' => 'NABÍDKA PRACOVNÍCH STÁŽÍ',
    'text' => 'Jak to chodí v IT průmyslu? Jaká je kultura vaší firmy? Jaké to je řešit odborné problémy nebo inovativně tvořit v týmu? Podpořte tvůrčí rozvoj našich studentů a nabídněte jim možnost vidět, jak to u vás ve firmě chodí. Nabídněte jim odbornou pracovní stáž nebo letní brigádu. Získáte přehled o našich studentech a budete si moci vybírat své potenciální zaměstnance.',
    'perex' => 'Ukažte studentům FIT, co a jak ve firmě děláte.',
    'first_heading' => 'Téma a termín stáže',
    'first_text' => 'Partneři FIT, zkonzultujte se svým odborným garantem na FIT téma pracovní stáže a vhodný termín pro studenty.',
    'second_heading' => 'Zveřejnění nabídky',
    'second_text' => 'Svoji nabídku pracovní stáže nebo letní brigády zašlete proděkanovi pro vnější vztahy. Vaše nabídka bude zveřejněna na portálu Nabídky pracovních stáží našich Partnerů.',
    'third_heading' => 'Spolupráce se studentem',
    'third_text' => 'Zájemci o stáž z řad studentů FIT vás budou kontaktovat přímo. Domluvíte se na detailech spolupráce se zájemci.',
];

$hackathons = [
    'heading' => 'POŘÁDÁNÍ SOUTĚŽÍ A HACKATONŮ',
    'text' => 'Máte nápad na téma soutěže pro studenty? Máte ve firmě téma, které by mohly studentské týmy ve 24/48-ti hodinách zkusit co nejlépe vyřešit? Máte ve firmě zajímavou technologii, kterou chcete studentům představit? Zorganizujte soutěž. Rádi Vám pomůžeme s organizací i propagací.',
    'perex' => 'Dejte studentům FIT do rukou IT “hračku” + 24 hodin = nechte se překvapit.',
    'first_heading' => 'Představení tématu',
    'first_text' => 'Partneři FIT, kontaktujte svého odborného garanta na FIT a domluvte si schůzku. Konzultujte odborné téma navržené soutěže a vhodné struktury realizace. Dále společně dohodneme organizační detaily: prostory, termín, způsob akce, rozsah zapojení obou stran, propagaci apod.',
    'second_heading' => 'Příprava akce',
    'second_text' => 'Příprava úloh(y) soutěže se zapojením garanta na FIT dle dohody a Vašich potřeb. Zajištění vybavení, prerekvizit, datových sad, využití infrastruktury FIT apod. Probíhá propagace akce na FIT. Vyřešení papírových formalit.',
    'third_heading' => 'Realizace akce',
    'third_text' => 'Realizace akce na FIT (v případě nutnosti spec. vybavení na půdě firmy), podpora organizace, vyhodnocení, tisková zpráva, medializace.',
];

$data = [
        'zaverecnePrace' => $thesis,
        'projekty' => $projects,
        'vyuka' => $lessons,
        'seminare' => $additionalLessons,
        'konference' => $conferences,
        'staze' => $works,
        'hackathon' => $hackathons
];

$type = $_GET['type'];
?>

    <main id="main" class="main" role="main">
        <div class="b-hero-header">
            <div class="b-hero-header__img" style="background-image: url('/img/illust/b-hero-header--14.jpg');"></div>
            <div class="b-hero-header__content b-hero-header__content--bottom holder holder--lg text-left">
                <p class="mb0">
                    <a href="../spoluprace/talenti.php" class="backlink">
                        <span class="icon-svg icon-svg--angle-l backlink__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-angle-l" x="0" y="0" width="100%" height="100%"></use>
                            </svg>
                        </span>

                        Výběr spolupráce
                    </a>
                </p>
                <h1 class="title b-hero-header__title">
                    <span class="title__item"><?php echo "{$data[$type]['heading']}"; ?></span>
                </h1>
            </div>
        </div>

        <div class="b-annot">
            <div class="grid grid--0">
                <div class="grid__cell size--t-7-12 holder holder--lg b-annot__content fz-lg">
                    <p>
                        <?php echo "{$data[$type]['text']}"; ?>
                    </p>
                </div>

                <div class="grid__cell size--t-5-12 holder holder--lg b-annot__content b-annot__content--pattern">
                    <p class="b-annot__brief">
                        <?php echo "{$data[$type]['perex']}"; ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="c-steps border-t border-b">
            <ul class="c-steps__list grid grid--bd">
                <li class="c-steps__item grid__cell size--t-4-12">
                    <div class="b-step holder holder--lg">
                        <h2 class="b-step__title">
                            <span class="h6">První krok</span>
                            <span class="h3"><?php echo "{$data[$type]['first_heading']}"; ?></span>
                        </h2>
                        <div class="b-step__content">
                            <p>
                                <?php echo "{$data[$type]['first_text']}"; ?>
                            </p>
                        </div>
                    </div>
                </li>
                <li class="c-steps__item grid__cell size--t-4-12">
                    <div class="b-step holder holder--lg">
                        <h2 class="b-step__title">
                            <span class="h6">Druhý krok</span>
                            <span class="h3"><?php echo "{$data[$type]['second_heading']}"; ?></span>
                        </h2>
                        <div class="b-step__content">
                            <p>
                                <?php echo "{$data[$type]['second_text']}"; ?>
                            </p>
                        </div>
                    </div>
                </li>
                <li class="c-steps__item grid__cell size--t-4-12">
                    <div class="b-step holder holder--lg">
                        <h2 class="b-step__title">
                            <span class="h6">Třetí krok</span>
                            <span class="h3"><?php echo "{$data[$type]['third_heading']}"; ?></span>
                        </h2>
                        <div class="b-step__content">
                            <p>
                                <?php echo "{$data[$type]['third_text']}"; ?>
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>


        <div class="holder holder--lg pt40--m pb40--m pt40--t pb40--t pt70 pb110 border-t">
            <h2 class="text-center mb32--m mb32--t mb40">Nemůžete si vybrat? Kontaktujte nás</h2>

            <div class="b-vcard">
                <div class="b-vcard__img">
                    <img src="../img/illust/beran.jpeg" width="207" height="266" alt="">
                </div>
                <div class="b-vcard__content">
                    <div class="b-vcard__title">
                        <p class="title title--xs title--secondary-darken">
                            <span class="title__item">Ing.</span>
                        </p>
                        <h3 class="title title--sm title--secondary">
                            <span class="title__item">Vítězslav Beran</span>
                        </h3>
                        <p class="title title--xs title--secondary-darken">
                            <span class="title__item">Ph.D.</span>
                        </p>
                    </div>
                    <div class="b-vcard__text">
                        <p>
                            Proděkan pro vnější vztahy
                        </p>
                    </div>
                    <p class="b-vcard__contacts">
                        <a href="tel:+420777286075" class="link-icon">
                        <span class="icon-svg icon-svg--phone link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                            +420 777 286 075
                        </a><br>
                        <a href="mailto:beranv@fit.vutbr.cz" class="link-icon">
                        <span class="icon-svg icon-svg--message link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                            beranv@fit.vutbr.cz
                        </a>
                    </p>
                </div>
            </div>
        </div>

    </main>

<?php
include '../footer.php';
?>