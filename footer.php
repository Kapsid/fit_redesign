<footer class="footer" role="contentinfo">
    <div class="footer__group pt30--m pb30--m pt40--t pb40--t pt60 pb60 holder holder--lg">
        <form action="?" class="f-newsletter">
            <div class="grid grid--middle">
                <div class="grid__cell size--6-12">
                    <h2 class="f-newsletter__title h3">
                        <label for="f-newsletter__email">
                            Buďte v obraze! Dostávejte aktuality z FIT přímo na mail a už nic nepropásnete
                        </label>
                    </h2>
                </div>

                <div class="grid__cell size--6-12">
                    <p class="inp inp--group mb0">
									<span class="inp__fix">
										<label for="f-newsletter__email" class="inp__label inp__label--inside">Tvoje nejlepší e-mailová adresa</label>
										<input type="text" class="inp__text" id="f-newsletter__email"
                                               placeholder="Tvoje nejlepší e-mailová adresa">
									</span>
                        <span class="inp__btn">
										<button class="btn btn--secondary btn--block--m" type="submit">
											<span class="btn__text">Chci aktuality do mailu</span>
										</button>
									</span>
                    </p>
                </div>
            </div>
        </form>
    </div>
    <div class="footer__group pt0--m pb0--m pt40--t pt55 pb40 holder holder--lg">
        <div class="grid">
            <div class="grid__cell size--t-4-12 size--2-12 mb40--t" data-toggle>
                <h2 class="footer__title js-toggle">
                    Studuj FIT
                    <span class="icon-svg icon-svg--angle-d footer__title-icon">
        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
        </svg>
        </span>

                </h2>
                <div class="footer__more" data-toggleable>
                    <ul class="footer__list">
                        <li class="footer__item">
                            <a href="#" class="footer__link">Pojď na FIT</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Nabídka oborů</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Přijímačky</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Přípravné kurzy</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Studuj FIT</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Den otevřených dveří</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Kontakty</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">E-PŘIHLÁŠKA</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="grid__cell size--t-4-12 size--2-12 mb40--t" data-toggle>
                <h2 class="footer__title js-toggle">
                    Pro studenty
                    <span class="icon-svg icon-svg--angle-d footer__title-icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                </h2>
                <div class="footer__more" data-toggleable>
                    <ul class="footer__list">
                        <li class="footer__item">
                            <a href="#" class="footer__link">Studijní oddělení</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Pro čerstvé prváky</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Časový plán studia</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Kalendář akcí</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Studium a stáže v zahraničí</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Studentské spolky</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Předměty</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Pracovní nabídky</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Stipendia</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Směrnice a předpisy</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="grid__cell size--t-4-12 size--2-12 mb40--t" data-toggle>
                <h2 class="footer__title js-toggle">
                    Věda a výzkum
                    <span class="icon-svg icon-svg--angle-d footer__title-icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                </h2>
                <div class="footer__more" data-toggleable>
                    <ul class="footer__list">
                        <li class="footer__item">
                            <a href="#" class="footer__link">Věda a výzkum na FIT</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Výzkumné skupiny</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Úspěchy</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Projekty</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Publikace</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Konference</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Předměty</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Pracovní nabídky</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Stipendia</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Směrnice a předpisy</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="grid__cell size--t-4-12 size--2-12" data-toggle>
                <h2 class="footer__title js-toggle">
                    Spolupráce s FIT
                    <span class="icon-svg icon-svg--angle-d footer__title-icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                </h2>
                <div class="footer__more" data-toggleable>
                    <ul class="footer__list">
                        <li class="footer__item">
                            <a href="#" class="footer__link">Firemní spolupráce</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Zahraniční spolupráce</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Střední školy a VUT</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Služby fakulty</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="grid__cell size--t-4-12 size--3-12" data-toggle>
                <h2 class="footer__title js-toggle">
                    Fakulta
                    <span class="icon-svg icon-svg--angle-d footer__title-icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%"
                                     height="100%"></use>
                            </svg>
                        </span>

                </h2>
                <div class="footer__more" data-toggleable>
                    <ul class="footer__list">
                        <li class="footer__item">
                            <a href="#" class="footer__link">Plány budov</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Aktuality</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Kalendář akcí</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Organizační struktura</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Ocenění a uznání</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Absolventi</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Knihovna</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Muzeum</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Lidé</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Úřední deska</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Pro média</a>
                        </li>
                        <li class="footer__item">
                            <a href="#" class="footer__link">Kontakty</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="footer__group pt20--m pb40--m pt40 pb50 holder holder--lg">
        <div class="grid grid--middle">
            <div class="grid__cell size--t-6-12 size--4-12">
                <p class="footer__logo">
                    <a href="#">
									<span class="icon-svg icon-svg--logo ">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-logo" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                        <span class="vhide">Vysoké učení technické v Brně</span>
                    </a>
                </p>
            </div>

            <div class="grid__cell size--t-6-12 size--4-12">
                <div class="address footer__info">
                    <p class="address__title font-secondary">Vysoké učení technické v Brně</p>
                    <div class="address__wrap">
                        <p class="address__item">
                            Antonínská 548/1<br>
                            601 90 Brno
                        </p>
                        <p class="address__item">
                            <a href="#" class="font-bold">www.vutbr.cz</a><br>
                            <a href="mailto:navut@vutbr.cz" class="font-bold">navut@vutbr.cz</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="grid__cell size--t-6-12 size--4-12 push--t-6-12 push--auto">
                <div class="c-socials footer__socials">
                    <ul class="c-socials__list">
                        <li class="c-socials__item">
                            <a href="#" class="c-socials__link">
											<span class="icon-svg icon-svg--facebook c-socials__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-facebook" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                <span class="vhide">Facebook</span>
                            </a>
                        </li>
                        <li class="c-socials__item">
                            <a href="#" class="c-socials__link">
											<span class="icon-svg icon-svg--twitter c-socials__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-twitter" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                <span class="vhide">Twitter</span>
                            </a>
                        </li>
                        <li class="c-socials__item">
                            <a href="#" class="c-socials__link">
											<span class="icon-svg icon-svg--instagram c-socials__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-instagram" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                <span class="vhide">Instagram</span>
                            </a>
                        </li>
                        <li class="c-socials__item">
                            <a href="#" class="c-socials__link">
											<span class="icon-svg icon-svg--youtube c-socials__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-youtube" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

                                <span class="vhide">YouTube</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__group pt20 pb20 holder holder--lg">
        <div class="grid">
            <p class="grid__cell size--t-6-12 mb0 fz-sm color-gray">
                Copyright &copy; 2018 VUT v Brně
            </p>
            <p class="grid__cell size--t-6-12 mb0 fz-sm color-gray text-left--m text-right">
                30.12.2017 01:09:18; php 0.51s, sql 0.40s; shreck2, cdbx1
            </p>
        </div>
    </div>
</footer>

<a href="#" class="to-top js-top">
    <span class="vhide">Nahoru</span>
    <span class="icon-svg icon-svg--angle-u to-top__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-u" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

</a>
</div>

<script src="../js/svg4everybody.min.js"></script>
<script>svg4everybody();</script>
<script src="../js/jquery-2.2.4.min.js"></script>
<script src="//cdn.polyfill.io/v2/polyfill.min.js?features=default,Array.prototype.find,Array.prototype.includes"></script>
<script src="../js/style-guide.js"></script>
<script src="../js/app.js"></script>
<script>
    App.run({
        basePath: '/',
        iconsPath: '/img/bg/'
    })
</script>
</body>
</html>