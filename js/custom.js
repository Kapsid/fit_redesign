$(document).ready(function(){
    $('#2019').show();
    $('.select').on('change', function() {
        var year = this.value.substring(5,9);
        var yearId = "#" + year;
        $('.program_year').fadeOut();
        $(yearId).fadeIn();
    });
});

