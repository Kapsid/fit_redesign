<!DOCTYPE html>
<html lang="cs" class="no-js">
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta name="description"
          content="Vysoké učení technické v Brně je česká vysoká škola s více než stoletou tradicí. VUT v Brně se řadí ke třem procentům nejlepších světových univerzit a neustále posiluje svou pozici špičkové vědecko-výzkumné instituce. Na osmi fakultách a dvou vysokoškolských ústavech Vysokého učení technického v Brně můžete získat strukturované vysokoškolské vzdělání v oborech technických, ekonomických, přírodovědných a uměleckých.">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Hlavička FIT | VUT">
    <meta name="twitter:description"
          content="Vysoké učení technické v Brně je česká vysoká škola s více než stoletou tradicí. VUT v Brně se řadí ke třem procentům nejlepších světových univerzit a neustále posiluje svou pozici špičkové vědecko-výzkumné instituce. Na osmi fakultách a dvou vysokoškolských ústavech Vysokého učení technického v Brně můžete získat strukturované vysokoškolské vzdělání v oborech technických, ekonomických, přírodovědných a uměleckých.">
    <meta name="twitter:image" content="https://www.vutbr.cz/img/bg/share/twitter.png">

    <meta property="og:title" content="Hlavička FIT | VUT">
    <meta property="og:description"
          content="Vysoké učení technické v Brně je česká vysoká škola s více než stoletou tradicí. VUT v Brně se řadí ke třem procentům nejlepších světových univerzit a neustále posiluje svou pozici špičkové vědecko-výzkumné instituce. Na osmi fakultách a dvou vysokoškolských ústavech Vysokého učení technického v Brně můžete získat strukturované vysokoškolské vzdělání v oborech technických, ekonomických, přírodovědných a uměleckých.">
    <meta property="og:image" content="https://www.vutbr.cz/img/bg/share/facebook.png">
    <meta property="og:site_name" content="Vysoké učení technické v Brně">
    <meta property="og:url" content="https://www.vutbr.cz/">
    <meta property="og:type" content="website">

    <title>FIT | VUT</title>

    <link rel="stylesheet" href="../css/style.css" media="screen">
    <link rel="stylesheet" href="../css/print.css" media="print">
    <link rel="stylesheet" href="../css/style-guide.css" media="screen">
    <link rel="stylesheet" href="../css/custom.css" media="screen">


    <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../img/favicon/manifest.json">
    <link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#e4002b">
    <link rel="shortcut icon" href="../img/favicon/favicon.ico">
    <meta name="msapplication-config" content="../img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <script src="../js/cookies.js"></script>
    <script>
        (function () {
            var className = document.documentElement.className;
            className = className.replace('no-js', 'js');

            if (window.Cookies.get('fontsLoaded') === 'true') {
                className += ' fonts-loaded';
            }

            document.documentElement.className = className;
        }());
    </script>

    <script src="../js/modernizr.min.js"></script>
    <script src="../js/jquery-2.2.4.min.js"></script>
    <script src="../js/custom.js"></script>


</head>
<body>
<div class="mother">
    <nav class="m-accessibility" id="m-accessibility" role="navigation">
        <p class="vhide">Přístupnostní navigace</p>
        <a title="Přejít k obsahu (Klávesová zkratka: Alt + 2)" accesskey="2" href="#main">Přejít k obsahu</a>
        <span class="hide">|</span>
        <a href="#m-main">Přejít k hlavnímu menu</a>
        <span class="hide">|</span>
        <a href="#f-search-trigger">Přejít k vyhledávání</a>
    </nav>

    <header class="header header--lg header--fit" role="banner">
        <h1 class="header__logo">
						<span class="icon-svg icon-svg--symbol ">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-symbol" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

            <span class="icon-svg icon-svg--sign-fit header__logo-sign">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-sign-fit" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

            <span class="header__logo-main" data-timeout="3000">
							<span class="icon-svg icon-svg--logo-fit--lg ">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-logo-fit--lg" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

	</span>
            <span class="vhide">VUT Brno</span>
        </h1>

        <nav id="m-main" class="m-main m-main--header m-main--header-lg header__menu" aria-label="Hlavní menu"
             role="navigation">
            <a href="#" class="m-main__toggle">
						<span class="icon-svg icon-svg--menu m-main__toggle-icon m-main__toggle-icon--open">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-menu" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

                <span class="icon-svg icon-svg--close--sm m-main__toggle-icon m-main__toggle-icon--close">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-close--sm" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

                <span class="m-main__toggle-label">Menu</span>
            </a>

            <ul class="m-main__list m-main__list--main">
                <li class="m-main__item" data-toggle>
                    <a href="#" class="m-main__link">Studujte na FIT</a>
                    <a href="#" class="m-main__toggle-sub js-toggle">
								<span class="icon-svg icon-svg--angle-d m-main__toggle-sub-icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

                        <span class="vhide">Submenu</span>
                    </a>
                    <div class="m-main__submenu" data-toggleable>
                        <div class="m-sub">
                            <div class="m-sub__col">
                                <ul class="m-sub__list">
                                    <li class="m-sub__item">
                                        <a href="../studujfit/pojd-na-fit.php" class="m-sub__link">Pojďte na FIT</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../studujfit/prijimacky.php" class="m-sub__link">Přijímačky / jak se
                                            dostat na FIT</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../studujfit/den-otevrenych-dveri.php" class="m-sub__link">Den
                                            otevřených dveří</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../studujfit/kontakt.php" class="m-sub__link">Kontakt</a>
                                    </li>

                                    <li class="m-sub__item">
                                        <a href="../studujfit/nabidka-oboru.php" class="m-sub__link">Obory</a>
                                    </li>
                                </ul>
                                <p>
                                    <a href="#" class="btn btn--sm">
                                        <span class="btn__text">E-přihláška</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="m-main__item" data-toggle>
                    <a href="#" class="m-main__link">Pro studenty</a>
                    <a href="#" class="m-main__toggle-sub js-toggle">

            <span class="icon-svg icon-svg--angle-d m-main__toggle-sub-icon">
                <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
	</svg>
    </span>

                        <span class="vhide">Submenu</span>
                    </a>
                    <div class="m-main__submenu m-main__submenu--lg" data-toggleable>
                        <div class="m-sub">
                            <div class="m-sub__col">
                                <ul class="m-sub__list">
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/studijniaktuality.php" class="m-sub__link">Studijní aktuality</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/studijniinformace.php" class="m-sub__link">Studijní informace</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/proprvaky.php" class="m-sub__link">Pro prváky</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/casovyplan.php" class="m-sub__link">Časový a studijní plán</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/predmety.php" class="m-sub__link">Předměty</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/studentskaunie.php" class="m-sub__link">Studentská
                                            unie</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/studiumastaze.php#seznam" class="m-sub__link">Studium a stáže v zahraničí</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../prostudenty/stazeanabidky.php" class="m-sub__link">Nabídky pracovních/odborných stáží</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="m-sub__col m-sub__col--highlighted">
                                <div class="m-highlights">
                                    <h2 class="m-highlights__title">Studijní oddělení</h2>
                                    <ul class="m-highlights__list">
                                        <li class="m-highlights__item">
                                            <h2 class="m-highlights__title">
                                                Otevírací hodiny
                                            </h2>
                                            <p class="m-highlights__item"><strong>Pondělí: </strong>12:00 - 15:00</p>
                                            <p class="m-highlights__item"><strong>Středa: </strong>8:00 - 11:00, 12:00 -
                                                14:00</p>
                                            <p class="m-highlights__item"><strong>Čtvrtek: </strong>8:00 - 11:00</p>
                                        </li>
                                    </ul>
                                    <p>
                                        <a href="#" class="btn btn--secondary btn--xs btn--outline">
                                            <span class="btn__text">Více o studijním oddělení</span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="m-sub__col m-sub__col--highlighted">
                                <div class="m-highlights">
                                    <h2 class="m-highlights__title">Nepropásni</h2>
                                    <ul class="m-highlights__list">
                                        <li class="m-highlights__item">
                                            <time class="m-highlights__tag tag tag--outline tag--xxs"
                                                  datetime="2016-11-19">19. listopad 2016
                                            </time>
                                            <h3 class="m-highlights__title">
                                                <a href="#" class="m-highlights__link">Merkur perFEKT Challenge&nbsp;2017</a>
                                            </h3>
                                        </li>
                                    </ul>
                                    <p>
                                        <a href="#" class="btn btn--secondary btn--xs btn--outline">
                                            <span class="btn__text">Kalendář akcí</span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="m-main__item" data-toggle>
                    <a href="#" class="m-main__link">Věda a&nbsp;výzkum</a>
                    <a href="#" class="m-main__toggle-sub js-toggle">
									<span class="icon-svg icon-svg--angle-d m-main__toggle-sub-icon">
		<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
		</svg>
		</span>

                        <span class="vhide">Submenu</span>
                    </a>
                    <div class="m-main__submenu m-main__submenu--sm" data-toggleable>
                        <div class="m-sub">
                            <div class="m-sub__col">
                                <ul class="m-sub__list">
                                    <li class="m-sub__item">
                                        <a href="../vedavyzkum/vedavyzkum.php" class="m-sub__link">Věda a výzkum na
                                            FIT</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../vedavyzkum/vyzkumneskupiny.php" class="m-sub__link">Výzkumné
                                            skupiny</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../vedavyzkum/uspechy.php" class="m-sub__link">Úspěchy</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../vedavyzkum/projekty.php" class="m-sub__link">Projekty</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../vedavyzkum/publikace.php" class="m-sub__link">Publikace</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="#" class="m-sub__link">Koncerence</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="m-main__item" data-toggle>
                    <a href="#" class="m-main__link">Spolupráce s&nbsp;FIT</a>
                    <a href="#" class="m-main__toggle-sub js-toggle">
									<span class="icon-svg icon-svg--angle-d m-main__toggle-sub-icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

                        <span class="vhide">Submenu</span>
                    </a>
                    <div class="m-main__submenu" data-toggleable>
                        <div class="m-sub">
                            <div class="m-sub__col">
                                <ul class="m-sub__list">
                                    <li class="m-sub__item">
                                        <a href="../spoluprace/firemnispoluprace.php" class="m-sub__link">Firemní
                                            spolupráce</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="#" class="m-sub__link">Zahraniční spolupráce</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../spoluprace/stredniskoly.php" class="m-sub__link">Spolupráce se školami</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="#" class="m-sub__link">Služby fakulty</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="m-main__item" data-toggle>
                    <a href="#" class="m-main__link">Fakulta</a>
                    <a href="#" class="m-main__toggle-sub js-toggle">
									<span class="icon-svg icon-svg--angle-d m-main__toggle-sub-icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-angle-d" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

                        <span class="vhide">Submenu</span>
                    </a>
                    <div class="m-main__submenu m-main__submenu--lg" data-toggleable>
                        <div class="m-sub">
                            <div class="m-sub__col">
                                <ul class="m-sub__list">
                                    <li class="m-sub__item">
                                        <a href="#" class="m-sub__link">Mapa areálu</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/aktuality.php" class="m-sub__link">Aktuality</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/kalendar.php" class="m-sub__link">Kalendář akcí</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/organizacni_struktura.php" class="m-sub__link">Organizační
                                            struktura</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/oceneni.php" class="m-sub__link">Ocenění</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/proabsolventy.php" class="m-sub__link">Pro absolventy</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/knihovna.php#oknihovne" class="m-sub__link">Knihovna</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/muzeum.php" class="m-sub__link">Muzeum</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/lide.php" class="m-sub__link">Lidé</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/urednideska.php" class="m-sub__link">Úřední deska</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/promedia.php" class="m-sub__link">Pro média</a>
                                    </li>
                                    <li class="m-sub__item">
                                        <a href="../ofakulte/kontakty.php" class="m-sub__link">Kontakty</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="m-sub__col m-sub__col--highlighted">
                                <div class="m-highlights">
                                    <h2 class="m-highlights__title">Nenechte si ujít</h2>
                                    <ul class="m-highlights__list">
                                        <li class="m-highlights__item">
                                            <time class="m-highlights__tag tag tag--outline tag--xxs"
                                                  datetime="2016-11-19">19. listopad 2016
                                            </time>
                                            <h3 class="m-highlights__title">
                                                <a href="#" class="m-highlights__link">Merkur perFEKT Challenge&nbsp;2017</a>
                                            </h3>
                                        </li>
                                    </ul>
                                    <p>
                                        <a href="#" class="btn btn--secondary btn--xs btn--outline">
                                            <span class="btn__text">Kalendář akcí</span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="m-main__item hide--d">
                    <a href="#" class="m-main__link m-main__link--lang">EN</a>
                </li>
            </ul>

            <ul class="m-main__list m-main__list--side">
                <li class="m-main__item">
                    <a href="#" class="m-main__link m-main__link--icon">
								<span class="icon-svg icon-svg--user m-main__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-user" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

                        <span class="vhide">Přihlásit se</span>
                    </a>
                </li>
                <li class="m-main__item">
                    <a href="#f-search" class="m-main__link m-main__link--icon js-toggle" id="f-search-trigger">
								<span class="icon-svg icon-svg--search m-main__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-search" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

                        <span class="vhide">Hledat</span>
                    </a>
                </li>
                <li class="m-main__item hide--m hide--t">
                    <a href="#" class="m-main__link m-main__link--lang">
                        EN
                    </a>
                </li>
            </ul>
        </nav>

        <div class="header__search">
            <form action="?" id="f-search" class="f-search f-search--lg" role="search">
                <p class="inp inp--group inp--group--m mb0">
                    <label for="f-search__search" class="vhide">Vyhledávání</label>
                    <span class="inp__fix inp__fix--btn-icon">
								<input type="text" class="inp__text inp__text--lg inp__text--flat" id="f-search__search"
                                       name="f-search__search" placeholder="Hledat" data-focus>
								<button class="btn btn--lg btn--icon-only btn--blank inp__btn-icon" type="submit">
									<span class="btn__text">
										<span class="vhide">Vyhledat</span>
										<span class="icon-svg icon-svg--search btn__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-search" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

									</span>
								</button>
							</span>
                    <span class="inp__btn">
								<a href="#f-search" class="btn btn--lg btn--secondary btn--icon-only js-toggle">
									<span class="btn__text">
										<span class="vhide">Zavřít</span>
										<span class="icon-svg icon-svg--close btn__icon">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="../img/bg/icons-svg.svg#icon-close" x="0" y="0" width="100%" height="100%"></use>
	</svg>
	</span>

									</span>
								</a>
							</span>
                </p>
            </form>
        </div>