<?php
include '../header.php';

$people = [
  ["degree" => "Mgr.", "name" => "Hana", "surname" => "Nečasová", "function" => "PR & marketing FIT VUT", "email" => "necasovah@fit.vut.cz", "phone" => "+420 54114 1153", "mobile" => "+420 607 007 550", "photo" => "b-vcard--03.jpg"],
  ["degree" => "Mgr.", "name" => "Radana", "surname" => "Kolčavová", "function" => "tisková mluvčí VUT", "email" => "kolcavova@ro.vutbr.cz", "phone" => "541 145 146", "mobile" => "730 545 330", "photo" => "b-vcard--03.jpg"],
];

$publicMessages = [
  ["day" => "22", "month" => "listopad", "year" => "2016", "name" => "Brněnská technika spouští VUT Junior — technickou univerzitu pro děti"],
    ["day" => "5", "month" => "říjen", "year" => "2016", "name" => "Brněnská technika spouští VUT Junior — technickou univerzitu pro děti"],
    ["day" => "12", "month" => "leden", "year" => "2017", "name" => "Brněnská technika spouští VUT Junior — technickou univerzitu pro děti"],
    ["day" => "22", "month" => "listopad", "year" => "2017", "name" => "Brněnská technika spouští VUT Junior — technickou univerzitu pro děti"],
];
?>

<div class="c-vcards holder holder--lg">
    <h1 class="c-vcards__title">Pro média</h1>

    <ul class="c-vcards__list grid grid--30">
        <?php foreach($people as $person){
           echo "<li class=\"c-vcards__item grid__cell grid__cell--grow size--6-12\">
            <div class=\"b-vcard\">
                <div class=\"b-vcard__img\">
                    <img src=\"/img/illust/{$person['photo']}\" width=\"207\" height=\"266\" alt=\"\">
                </div>
                <div class=\"b-vcard__content\">
                    <div class=\"b-vcard__title\">
                        <p class=\"title title--xs title--secondary-darken\">
                            <span class=\"title__item\">{$person['degree']}</span>
                        </p>
                        <h2 class=\"title title--sm title--secondary h3\">
                            <span class=\"title__item\">{$person['name']}</span><br><span class=\"title__item\">{$person['surname']}</span>
                        </h2>
                    </div>
                    <p class=\"b-vcard__position font-secondary\">{$person['function']}</p>
                    <p class=\"b-vcard__contacts\">
                        <a href=\"tel:{$person['mobile']}\" class=\"link-icon\">
						<span class=\"icon-svg icon-svg--phone link-icon__icon\">
                        <svg class=\"icon-svg__svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                            <use xlink:href=\"/img/bg/icons-svg.svg#icon-phone\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\"></use>
                        </svg>
                        </span>
                            {$person['mobile']}
                        </a>

                        <a href=\"tel:{$person['phone']}\" class=\"link-icon\">
						<span class=\"icon-svg icon-svg--phone link-icon__icon\">
                        <svg class=\"icon-svg__svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                            <use xlink:href=\"/img/bg/icons-svg.svg#icon-phone\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\"></use>
                        </svg>
                        </span>
                            {$person['phone']}
                        </a>

                        <br>
                        <a href=\"mailto:{$person['email']}\" class=\"link-icon text-overflow\">
                        <span class=\"icon-svg icon-svg--message link-icon__icon\">
                            <svg class=\"icon-svg__svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                                <use xlink:href=\"/img/bg/icons-svg.svg#icon-message\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\"></use>
                            </svg>
                        </span>

                            {$person['email']}
                        </a>
                    </p>
                </div>
            </div>
        </li>";
        }?>

    </ul>
</div>

<div class="c-media holder holder--lg">
    <div class="row-main row-main--sm">
        <h2 class="c-media__title">Tiskové zprávy</h2>

        <ul class="c-media__list">
            <?php foreach($publicMessages as $publicMessage){
                echo "<li class=\"c-media__item\">
                <a href=\"#\" class=\"c-media__link\">
                    <time class=\"c-media__link-date font-secondary\">{$publicMessage['day']}. {$publicMessage['month']} {$publicMessage['year']}</time>
                    <span class=\"c-media__link-title\">
                       {$publicMessage['name']}
                        <span class=\"icon-svg icon-svg--angle-r c-media__link-icon\">
                            <svg class=\"icon-svg__svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                                <use xlink:href=\"/img/bg/icons-svg.svg#icon-angle-r\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\"></use>
                            </svg>
                        </span>
                    </span>
                </a>
            </li>";
            }
            ?>

        </ul>

    </div>
</div>


<?php
include '../footer.php';
?>
