<?php
include '../header.php';

$events = [
  ['name' => 'Akce 1', 'day' => 11, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 2', 'day' => 11, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 3', 'day' => 18, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 4', 'day' => 11, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 5', 'day' => 22, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 6', 'day' => 23, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 4', 'day' => 11, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 5', 'day' => 22, 'month' => 'září', 'year' => 2018],
    ['name' => 'Akce 6', 'day' => 23, 'month' => 'září', 'year' => 2018],
];
?>

<main id="main" class="main" role="main">
    <div class="b-intro border-b holder holder--lg">
        <h1 class="b-intro__title">Kalendář akcí</h1>

        <form action="?" class="f-subjects">
            <p class="inp inp--multiple">
            <span class="inp__fix inp__fix--icon-after minw260">
                <label for="date" class="inp__label inp__label--inside">Datum od-do</label>
                <input type="text" class="inp__text inp__text--bold js-datepicker" name="date" id="date" placeholder="Datum od-do" data-options='[{
                    "labels": {
                        "hours": "hod",
                        "minutes": "min"
                    },
                    "options": {
                        "enableTime": true,
                        "altFormat": "j. F Y, H:i"
                    }
                }]'>
                <span class="icon-svg icon-svg--calendar inp__icon">
                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="/img/bg/icons-svg.svg#icon-calendar" x="0" y="0" width="100%" height="100%"></use>
                    </svg>
                </span>

            </span>
            <span class="inp__fix minw180">
                <label for="type" class="inp__label inp__label--inside">Typ akce</label>
                <select name="type" id="type" class="select js-select">
                    <option selected disabled placeholder>Typ akce</option>
                    <option>Hodnota 1</option>
                    <option>Hodnota 2</option>
                    <option>Hodnota 3</option>
                    <option>Hodnota 4</option>
                    <option>Hodnota 5</option>
                </select>
            </span>
            </p>
        </form>
    </div>

    <div class="c-events">
        <ul class="c-events__list grid grid--bd">
            <?php foreach($events as $event){
                echo "<li class=\"c-events__item grid__cell grid__cell--grow border-b holder
							size--4-12 c-events__item--sm holder--md
					\">
                <a href=\"../ofakulte/akce_detail.php\" class=\"b-term
								b-term--sm
								b-term--img
						c-events__term\">
                    <div class=\"b-term__img\">
                    </div>
                    <div class=\"b-term__wrap\">
                        <time class=\"b-term__date date font-secondary\">
                            <span class=\"date__day\">{$event['day']}</span>
                            <span class=\"date__month\">{$event['month']}</span>
                            <span class=\"date__year\">{$event['year']}</span>
                        </time>
                        <h2 class=\"b-term__title h3\">{$event['name']}</h2>
                    </div>
                </a>
            </li>";
            }?>

        </ul>

        <div class="holder holder--lg">
            <nav class="pagination text-center" aria-label="Stránkování" role="navigation">
                <ul class="pagination__list">
                    <li class="pagination__item">
                        <a href="#" class="pagination__link" aria-current="page">1</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">3</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="btn btn--secondary btn--wide btn--icon-only--m">
				<span class="btn__text">
					<span class="hide--m">Další</span>
					<span class="icon-svg icon-svg--angle-r btn__icon hide--t hide--d">
                        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
                        </svg>
                    </span>

				</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</main>


<?php
include '../footer.php'
?>
