<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-intro border-b holder holder--lg">
        <h1 class="b-intro__title">Informační tabule</h1>

        <form action="?" class="f-subjects mb30--m mb40">
            <div class="f-subjects__search mb0">
                <p class="inp inp--group mb0">
							<span class="inp__fix">
								<label for="f-subjects__search" class="inp__label inp__label--inside">Hledat v úřední desce</label>
								<input type="text" class="inp__text" id="f-subjects__search" placeholder="Hledat v úřední desce">
							</span>
                    <span class="inp__btn">
								<button class="btn btn--secondary btn--block--m" type="submit">
									<span class="btn__text">Hledat</span>
								</button>
							</span>
                </p>
            </div>
        </form>

        <div class="grid grid--bd bg-white border">
            <div class="grid__cell size--t-4-12 holder holder--md pt20--m pb20--m p20--t pt40 pb40">
                <ul class="list-links list-links--blank">
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/predpisy/" class="list-links__link font-secondary">Vnitřní předpisy</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/smernice/" class="list-links__link font-secondary">Směrnice</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/rd/" class="list-links__link font-secondary">Rozhodnutí děkana</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/rt/" class="list-links__link font-secondary">Rozhodnutí tajemníka</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/pd/" class="list-links__link font-secondary">Pokyny děkana</a>
                    </li>
                </ul>
            </div>

            <div class="grid__cell size--t-4-12 holder holder--md pt20--m pb20--m p20--t pt40 pb40">
                <ul class="list-links list-links--blank">
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/vyhlasky/" class="list-links__link font-secondary">Vyhlášky proděkanů</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/FIT/dz/" class="list-links__link font-secondary">Strategický záměr</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/FIT/vz/" class="list-links__link font-secondary">Výroční zprávy FIT</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="c-structure holder holder--lg">
        <h2 class="title-underlined c-structure__title">Výběrová řízení</h2>
        <div class="grid">
            <div class="grid__cell size--t-6-12 mb10--m">
                <ul class="list-links">
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/vyber/" class="list-links__link font-secondary">Výběrová řízení</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="c-structure holder holder--lg">
        <h2 class="title-underlined c-structure__title">Řízení</h2>
        <div class="grid">
            <div class="grid__cell size--t-6-12 mb10--m">
                <ul class="list-links">
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/dd/" class="list-links__link font-secondary">Doktorská řízení</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/hd/" class="list-links__link font-secondary">Habilitační řízení</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/hp/" class="list-links__link font-secondary">Profesorská řízení</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="c-structure holder holder--lg">
        <h2 class="title-underlined c-structure__title">Pro studenty</h2>
        <div class="grid">
            <div class="grid__cell size--t-6-12 mb10--m">
                <ul class="list-links">
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/rizeni/" class="list-links__link font-secondary">Přijímací řízení</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/plan/" class="list-links__link font-secondary">Časový plán</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/szz/" class="list-links__link font-secondary">Státní závěrečné zkoušky a diplomové práce</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/poplatky/" class="list-links__link font-secondary">Poplatky a úhrady</a>
                    </li>
                    <li class="list-links__item">
                        <a href="#" class="list-links__link font-secondary">Zákony, vyhlášky a pravidla pro studenty</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>

    <div class="c-structure holder holder--lg">
        <h2 class="title-underlined c-structure__title">Kolegia děkana</h2>
        <div class="grid">
            <div class="grid__cell size--t-6-12 mb10--m">
                <ul class="list-links">
                    <li class="list-links__item">
                        <a href="https://www.fit.vutbr.cz/info/zkd/" class="list-links__link font-secondary">Kolegia děkana</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="c-structure holder holder--lg">
        <h2 class="title-underlined c-structure__title">Orgány FIT</h2>
        <div class="grid">
            <div class="grid__cell size--t-6-12 mb10--m">
                <ul class="list-links">
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/AS/" class="list-links__link font-secondary">Akademický senát</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/VR/" class="list-links__link font-secondary">Vědecká rada</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/info/dk/" class="list-links__link font-secondary">Disciplinární komise</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.fit.vutbr.cz/FIT/PR/" class="list-links__link font-secondary">Průmyslová rada</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="c-structure holder holder--lg">
        <h2 class="title-underlined c-structure__title">Informace VUT</h2>
        <div class="grid">
            <div class="grid__cell size--t-6-12 mb10--m">
                <ul class="list-links">
                    <li class="list-links__item">
                        <a href="https://www.vutbr.cz/uredni-deska" class="list-links__link font-secondary">Úřední deska VUT</a>
                    </li>
                    <li class="list-links__item">
                        <a href="https://www.vutbr.cz/uredni-deska/vnitrni-predpisy-a-dokumenty" class="list-links__link font-secondary">Vnitřní předpisy VUT</a>
                    </li>
                    <li class="list-links__item">
                        <a href="http://www.kam.vutbr.cz/" class="list-links__link font-secondary">Koleje a menzy VUT</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</main>


<?php
include '../footer.php';
?>
