<?php
include '../header.php';
?>
<main id="main" class="main" role="main">

    <div class="b-detail">
        <div class="">
            <div class="grid__cell size--t-4-12 holder holder--lg b-detail__head">
                <h2>Rozvrhy</h2>
                <div class="sg-box__item">
                    <div class="sg-box__item-code sg-box__item-code--bleed">
                        <div class="b-admission holder holder--lg pb60--d">
                            <div class="">
                                <form action="?" class="f-subjects b-admission__filter">
                                    <div class="f-subjects__filter f-subjects__filter--full">
                                        <p class="inp inp--multiple">
                                    <span class="inp__fix minw440">
                                        <label for="field1" class="inp__label inp__label--inside">Přednášková skupina</label>
                                        <select name="group-select" class="select js-select">
                                            <option>1BIT</option>
                                            <option>2BIT</option>
                                            <option>3BIT</option>
                                            <option>1MIT</option>
                                            <option>2MIT</option>
                                        </select>
                                    </span>
                                        </p>
                                    </div>
                                </form>
                            </div><br />
                            <div class="">
                                <form action="?" class="f-subjects b-admission__filter">
                                    <div class="f-subjects__filter f-subjects__filter--full">
                                        <p class="inp inp--multiple">
                                    <span class="inp__fix minw440">
                                        <label for="field1" class="inp__label inp__label--inside">Místnost</label>
                                        <select name="place-select" class="select js-select">
                                            <option>D105</option>
                                            <option>D0206</option>
                                            <option>E112</option>
                                        </select>
                                    </span>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__cell size--t-12-12 holder holder--lg b-detail__summary" style="padding-top: 0px;">
                <div class="b-detail__abstract fz-lg">
                    <table>
                        <tr class="table-header">
                            <td>Den</td>
                            <td>Týden</td>
                            <td>8:00</td>
                            <td>9:00</td>
                            <td>10:00</td>
                            <td>11:00</td>
                            <td>12:00</td>
                            <td>13:00</td>
                            <td>14:00</td>
                            <td>15:00</td>
                            <td>16:00</td>
                            <td>17:00</td>
                            <td>18:00</td>
                            <td>19:00</td>
                        </tr>
                        <tr class="table-header">
                            <td></td>
                            <td></td>
                            <td>8:50</td>
                            <td>9:50</td>
                            <td>10:50</td>
                            <td>11:50</td>
                            <td>12:50</td>
                            <td>13:50</td>
                            <td>14:50</td>
                            <td>15:50</td>
                            <td>16:50</td>
                            <td>17:50</td>
                            <td>18:50</td>
                            <td>19:50</td>
                        </tr>
                        <tr>
                            <td>Po</td>
                            <td>L</td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Po</td>
                            <td>S</td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-lecture" colspan="3"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        </tr>
                        <tr>
                            <td>Út</td>
                            <td>L</td>
                            <td></td>
                            <td class="table-lecture" colspan="3"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php"1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        </tr>
                        <tr>
                            <td>Út</td>
                            <td>S</td>
                            <td class="table-exercise" colspan="3"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-lecture" colspan="3"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        <tr>
                            <td>St</td>
                            <td>L</td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-lecture" colspan="3"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="3"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        </tr>
                        <tr>
                            <td>St</td>
                            <td>S</td>
                            <td></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="3"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        </tr>
                        <tr>
                            <td>Čt</td>
                            <td>L</td>
                            <td></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        </tr>
                        <tr>
                            <td>Čt</td>
                            <td>S</td>
                            <td></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        </tr>
                        <tr>
                            <td>Pá</td>
                            <td>L</td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">ITW</a> pv <a href="../ofakulte/rozvrh_skupina.php"1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITY</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                        </tr>
                        <tr>
                            <td>Pá</td>
                            <td>L</td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ISA</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">IMS</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-exercise" colspan="2"><a href="predmet_detail.php">IPR</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td class="table-lecture" colspan="2"><a href="predmet_detail.php">ITU</a> pv <a href="../ofakulte/rozvrh_skupina.php">1BIA</a></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
include '../footer.php';
?>
