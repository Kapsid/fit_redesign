<?php
include '../header.php';

$openingHours = [
    ["day" => "Pondělí" , "time" => "14 - 16"],
    ["day" => "Úterý" , "time" => "14 - 16"],
    ["day" => "Středa" , "time" => "14 - 18"],
    ["day" => "Čtvrtek" , "time" => "14 - 16"],
    ["day" => "Pátek" , "time" => "15 - 19"],
];

$organPeople = [
    ["name" => "Doupovec Miroslav", "title" => "Ing.", "function" => "Vodoucí", "image" => "b-employee--01.jpg"],
    ["name" => "Peterka Pavel", "title" => "Mgr.", "function" => "Zástupce", "image" => "b-employee--02.jpg"],
    ["name" => "Doupovec Miroslav", "title" => "Ing.", "function" => "Vodoucí", "image" => "b-employee--01.jpg"],
];

$organ = [
    "name" => "Studijní oddělení",
    "address" => "Božetěchova 2",
    "phone" => "+420 776 202 635",
    "mail" => "vut@vutbr.cz",
    "openingHours" => $openingHours,
    "people" => $organPeople,
];
?>

<div class="b-intro b-intro--pattern holder holder--lg pt50--d pb60--d">
    <p class="mb0">
        <a href="../ofakulte/organizacni_struktura.php" class="backlink">
		<span class="icon-svg icon-svg--angle-l backlink__icon">
            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <use xlink:href="/img/bg/icons-svg.svg#icon-angle-l" x="0" y="0" width="100%" height="100%"></use>
            </svg>
        </span>
            Organizační struktura
        </a>
    </p>
    <h1 class="title mb0">
        <span class="title__item"><?php echo "{$organ['name']}";?></span>
    </h1>
</div>

<div class="grid grid--bd border-b">
    <div class="grid__cell size--t-12-12">
        <div class="grid grid--bd">
            <div class="grid__cell size--3-12">
                <div class="b-contact holder holder--md pt40--m pt40--t pt75 pb40">
                    <h2 class="b-contact__title">Kontaktujte nás</h2>
                    <div class="b-contact__text">
                        <p>
                            <?php echo "{$organ['address']}";?>
                        </p>
                        <p>
                            <a href="tel:<?php echo "{$organ['phone']}";?>" class="link-icon">
						<span class="icon-svg icon-svg--phone link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%" height="100%"></use>
                            </svg>
                        </span>
                                <?php echo "{$organ['phone']}";?>
                            </a><br>
                            <a href="mailto:vut@vutbr.cz" class="link-icon">
						<span class="icon-svg icon-svg--message link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%" height="100%"></use>
                            </svg>
                        </span>
                                <?php echo "{$organ['mail']}";?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="grid__cell size--3-12">
                <div class="b-contact holder holder--md pt40--m pt40--t pt75 pb40">
                    <h2 class="b-contact__title">Otevírací hodiny</h2>
                    <div class="b-contact__text">
                        <?php foreach ($openingHours as $openingHour){
                          echo "<p style='text-align: left; margin-bottom: 5px;'>
                                <strong>{$openingHour['day']}: </strong>{$openingHour['time']}
                            </p>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="grid__cell size--6-12 border-t--t">
                <div class="b-gmap">
                    <div class="b-gmap__holder b-gmap__holder--sm" data-address="Fakulta informačních technologií VUT v Brně"></div>
                </div>
            </div>
            <div class="grid__cell">

            </div>
        </div>
    </div>
</div>

<div class="c-employees">
    <div class="holder holder--lg">
        <h2 class="c-employees__title">Zaměstanci</h2>
    </div>
    <div class="holder holder--lg border-t">
        <div class="c-employees__wrap">
            <ul class="c-employees__list grid grid--60">
                <?php foreach($organ["people"] as $person){
                    echo "<li class=\"c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
                    <a href=\"#\" class=\"b-employee\">
                        <div class=\"b-employee__wrap\">
                            <div class=\"b-employee__img\">
                                <img src=\"/img/illust/{$person['image']}\" width=\"100\" height=\"100\" alt=\"\">
                            </div>
                            <h3 class=\"b-employee__name\">{$person['name']}, {$person['title']}</h3>
                            <div class=\"b-employee__footer\">
                                <p class=\"b-employee__position font-secondary\">{$person['function']}</p>
                            </div>
                        </div>
                    </a>
                </li>";
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<?php
include '../footer.php';
?>