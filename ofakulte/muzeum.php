<?php
include '../header.php';
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="holder holder--lg b-detail__head">
            <p class="mb20">
            </p>
            <h1 class="b-detail__title">Muzeum výpočetní techniky</h1>
            <p class="b-detail__annot">
                <span class="b-detail__annot-item b-detail__annot-item--label"><strong>Otevírací doba:</strong></span>
                <span class="b-detail__annot-item">pro veřejnost jsou sbírky přístupné každou první středu v měsíci od 13:00 do 17:00</span>
            </p>
            <p class="b-detail__annot">
                <span class="b-detail__annot-item b-detail__annot-item--label"><strong>Adresa:</strong></span>
                <span class="b-detail__annot-item">FIT VUT, Božetěchova 2 Brno, přízemí budovy A, místnost 118, vstup z nádvoří </span>
            </p>
            <div class="b-detail__abstract b-detail__abstract--md fz-lg">
                <p>
                    V muzeu se nyní nachází téměř padesát různých (převážně osobních) počítačů, doplňuje je asi třicítka dobových periferních zařízení, kromě toho expozice zahrnují dalších asi 60 drobných exponátů.
                    Expozice počítačových pamětí ukazuje vývoj technologie od pamětí prvních počítačů, které u nás vznikaly koncem 50. let, prakticky až do současnosti.
                </p>
            </div>

            <div class="sg-box__item">
                <div class="sg-box__item-code">
                    <div class="js-gallery">
                        <div class="c-gallery">
                            <ul class="c-gallery__list grid grid--10">
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum1.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum1.jpg');">
                                            <img src="../img/museum/museum1.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum2.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum2.jpg');">
                                            <img src="../img/museum/museum2.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum3.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum3.jpg');">
                                            <img src="../img/museum/museum3.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum4.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum4.jpg');">
                                            <img src="../img/museum/museum4.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum5.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum5.jpg');">
                                            <img src="../img/museum/museum5.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum6.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum6.jpg');">
                                            <img src="../img/museum/museum6.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum7.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum7.jpg');">
                                            <img src="../img/museum/museum7.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum8.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum8.jpg');">
                                            <img src="../img/museum/museum8.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum9.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum9.jpg');">
                                            <img src="../img/museum/museum9.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                                <li class="c-gallery__item grid__cell size--t-4-12">
                                    <a href="../img/museum/museum10.jpg" data-gallery class="c-gallery__link">
                                        <div class="c-gallery__img" style="background-image: url('../img/museum/museum10.jpg');">
                                            <img src="../img/museum/museum10.jpg" width="280" height="190" alt="">
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>

<?php
include '../footer.php';
?>