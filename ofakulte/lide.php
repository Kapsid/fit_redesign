<?php
include '../header.php';

$aPeople = [
    ['name' => 'Renata', 'surname' => 'Adamcová', 'titles' => 'Prof. RNDr., CSc., dr. h. c.', 'role' => 'role zaměstnance'],
    ['name' => 'Petr', 'surname' => 'Adámek', 'titles' => 'Ing.', 'role' => 'role zaměstnance'],
];

$bPeople = [
    ['name' => 'Petr', 'surname' => 'Bečka', 'titles' => 'Ing.', 'role' => 'role zaměstnance'],
    ['name' => 'Jan', 'surname' => 'Buvol', 'titles' => 'Ing.', 'role' => 'role zaměstnance'],
];

$iPeople = [
    ['name' => 'Petr', 'surname' => 'Ital', 'titles' => 'Ing.', 'role' => 'role zaměstnance'],
];


$peopleArray = [
        'A' => $aPeople,
        'B' => $bPeople,
        'C' => [],
        'D' => [],
        'E' => [],
        'F' => [],
        'G' => [],
        'H' => [],
        'I' => $iPeople,
        'J' => [],
        'K' => [],
        'L' => [],
        'M' => [],
        'N' => [],
        'O' => [],
        'P' => [],
        'Q' => [],
        'R' => [],
        'S' => [],
        'T' => [],
        'U' => [],
        'V' => [],
        'W' => [],
        'X' => [],
        'Y' => [],
        'Z' => [],
]
?>
	<main id="main" class="main pt60 pb60" role="main" style="margin: 105px 0px 0px 0px;">
		<div class="holder holder--lg">
			<div class="sg-box">
				<h1 class="c-attrs__title h2">Lidé na FIT</h1>
				<div class="sg-box__item">
					<div class="sg-box__item-annot">
						<h2 class="sg-box__item-title">Hledat zaměstnance</h2>
					</div>
					<div class="sg-box__item-code">
						<form action="?" class="f-subjects">
							<div class="f-subjects__search">
								<p class="inp inp--group mb0">
									<span class="inp__fix">
										<label for="f-subjects__search11" class="inp__label inp__label--inside">Hledat zaměstnance</label>
										<input type="text" class="inp__text" id="f-subjects__search11" placeholder="Hledat zaměstnance">
									</span>
									<span class="inp__btn">
										<button class="btn btn--secondary btn--block--m" type="submit">
											<span class="btn__text">Hledat</span>
										</button>
									</span>
								</p>
							</div>
						</form>
					</div>
				</div>
				<div class="sg-box__item-code">
					<div class="c-employees holder holder--lg pt60--d">
						<nav class="pagination pagination--alphabet" role="navigation">
							<ul class="pagination__list">
                                 <?php foreach($peopleArray as $personKey => $personValue) {
                                    if (!empty($personValue)) {
                                        echo "<li class=\"pagination__item\" >
                                                <a href = \"#{$personKey}\" data-slide = \"#{$personKey}\" class=\"pagination__link\" > {$personKey}</a>
                                            </li >";
                                        }
                                }
                                ?>
							</ul>
						</nav>

                        <?php foreach($peopleArray as $personKey => $personValue) {
                            if (!empty($personValue)) {
                                echo "<div class=\"c-employees__wrap\" id=\"{$personKey}\">
                                    <p class=\"c-employees__letter-wrap\">
                                        <span class=\"c-employees__letter\">{$personKey}</span>
                                    </p>
                                    <ul class=\"c-employees__list grid grid--60\">";
                                        foreach($personValue as $personMember){
                                            echo "<li class=\"c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
                                            <a href=\"../ofakulte/profil.php\" class=\"b-employee\">
                                                <div class=\"b-employee__wrap\">
                                                    <div class=\"b-employee__img\">
                                                        <img src=\"../img/illust/b-employee--01.jpg\" width=\"100\" height=\"100\" alt=\"\">
                                                    </div>
                                                    <h2 class=\"b-employee__name h3\">{$personMember['name']} {$personMember['surname']}, {$personMember['titles']}</h2>
                                                    <div class=\"b-employee__footer\">
                                                        <p class=\"b-employee__position font-secondary\">{$personMember['role']}</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>";
                                        }
                                        echo "</ul>
                                    </div>";
                                }
                            }
                        ?>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php
include '../footer.php'
?>