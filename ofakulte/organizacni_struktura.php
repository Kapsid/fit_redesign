<?php
include '../header.php';
?>

<div class="b-intro border-b holder holder--lg">
    <h1 class="b-intro__title">Organizační struktura FIT</h1>

    <form action="?" class="f-subjects">
        <div class="f-subjects__search">
            <p class="inp inp--group mb0">
							<span class="inp__fix">
								<label for="f-subjects__search" class="inp__label inp__label--inside">Hledat orgán</label>
								<input type="text" class="inp__text" id="f-subjects__search" placeholder="Hledat orgán">
							</span>
                <span class="inp__btn">
								<button class="btn btn--secondary btn--block--m" type="submit">
									<span class="btn__text">Hledat</span>
								</button>
							</span>
            </p>
        </div>
    </form>
</div>

<div class="c-structure holder holder--lg">
    <div class="grid">
        <div class="grid__cell size--t-6-12 mb10--m">
            <ul class="list-links">
                <li class="list-links__item">
                    <a href="../ofakulte/organ_detail.php" class="list-links__link font-secondary">Děkanát a oddělení děkanátu</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Knihovna</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Správa areálu</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Ústav počítačových systémů</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Ústav infočmačních systémů</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Ústav inteligentních systémů</a>
                </li>
            </ul>
        </div>

        <div class="grid__cell size--t-6-12">
            <ul class="list-links">
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Ústav počítačové grafiky a multimédií</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Výzkumné centrum informačních technologií</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Centrum výpočetní techniky</a>
                </li>
                <li class="list-links__item">
                    <a href="#" class="list-links__link font-secondary">Hledat zaměstnance a studenty</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php
include '../footer.php';
?>
