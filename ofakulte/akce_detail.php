<?php
include '../header.php';

$beginDate = [
    'day' => 12,
    'month' => 'září',
    'year' => 2018,
    'time' => '18:00'
];

$endDate = [
    'day' => 10,
    'month' => 'listopad',
    'year' => 2018,
    'time' => '14:00'
];

$otherEvents = [
    ['day' => 10, 'month' => 'září', 'year' => 2018, 'title' => 'Název akce 2'],
    ['day' => 15, 'month' => 'září', 'year' => 2018, 'title' => 'Název akce 2'],
    ['day' => 22, 'month' => 'říjen', 'year' => 2018, 'title' => 'Název akce 2'],
];

$contactPerson = [
  'surname' => 'Hermannová',
  'name' => 'Renata',
  'degree' => 'Ing.',
    'phone' => '+420 541 146 120',
    'mail' => 'steffan@feec.vutbr.cz'
];

$event = [
    'name' => 'Akce 1',
    'place' => 'Aula, Technická 12, 616 00 Brno',
    'begin' => $beginDate,
    'end' => $endDate,
    'description' => 'Fakulta elektrotechniky a komunikačních technologií (FEKT), VUT v Brně pořádá 5. ročník velmi úspěšné soutěže Merkur 
                    perFEKT Challenge určenou čtyřčlenným středoškolským týmům. Týmy řeší zadání z oblasti elektroniky a elektrotechniky s využitím populární stavebnice Merkur.',
    'contactPerson' => $contactPerson,
];
?>

<main id="main" class="main" role="main">
    <div class="b-detail">
        <div class="holder holder--lg b-detail__head">
            <p class="mb20">
                <span class="tag tag--sm">Detail akce</span>
            </p>
            <h1 class="b-detail__title"><?php echo "{$event['name']}" ?></h1>
            <p class="b-detail__annot">
                <span class="b-detail__annot-item b-detail__annot-item--label">Místo konání:</span>
                <span class="b-detail__annot-item"><?php echo "{$event['place']}" ?></span>
            </p>
            <p class="b-detail__annot mb40">
                <span class="b-detail__annot-item b-detail__annot-item--label">Začátek:</span>
                <span class="b-detail__annot-item font-bold"><time><?php echo "{$event['begin']['day']}. {$event['begin']['month']} {$event['begin']['year']} ve {$event['begin']['time']}"?></time></span>
                <span class="b-detail__annot-item">Konec: <time datetime="2018-08-14 12:00" class="font-bold"><time><?php echo "{$event['begin']['day']}. {$event['begin']['month']} {$event['begin']['year']} ve {$event['begin']['time']}"?></time></span>
            </p>
            <div class="b-detail__abstract b-detail__abstract--md fz-lg">
                <p>
                    <?php echo "{$event['description']}" ?>
                </p>
            </div>
        </div>
    </div>

    <div class="c-employees pt0">
        <div class="holder holder--lg">
            <h2 class="c-employees__title c-employees__title--underlined">Kontaktní osoba</h2>

            <div class="c-employees__wrap">
                <ul class="c-employees__list grid grid--60 pt30">
                    <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--3-12">
                        <div class="b-employee">
                            <div class="b-employee__wrap">
                                <a href="#" class="b-employee__link">
                                    <div class="b-employee__img">
                                        <img src="/img/illust/b-employee--01.jpg" width="100" height="100" alt="">
                                    </div>
                                    <h3 class="b-employee__name"><?php echo "{$event['contactPerson']['name']} {$event['contactPerson']['surname']}, {$event['contactPerson']['degree']}" ?></h3>
                                </a>
                                <div class="b-employee__footer">
                                    <p>
                                        <?php echo "<a href=\"tel:{$event['contactPerson']['phone']}\">{$event['contactPerson']['phone']}</a><br>
                                        <a href=\"mailto:{$event['contactPerson']['mail']}\">{$event['contactPerson']['mail']}</a>"; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="c-events border-t pt40 pb40">
        <div class="holder holder--lg">
            <h2 class="c-events__title">Další akce</h2>
        </div>

        <ul class="c-events__list grid grid--bd border-t mb40">
            <?php foreach($otherEvents as $otherEvent){
                echo "<li class=\"c-events__item grid__cell grid__cell--grow border-b holder
							size--4-12 c-events__item--sm holder--md
					\">
                <a href=\"../ofakulte/akce_detail.php\" class=\"b-term
								b-term--sm
								b-term--img
						c-events__term\">
                    <div class=\"b-term__img\">
                        <img src=\"/img/illust/b-term--sm--01.jpg\" width=\"400\" height=\"280\" alt=\"\">
                    </div>
                    <div class=\"b-term__wrap\">
                        <time class=\"b-term__date date font-secondary\" datetime=\"2017-11-17\">
                            <span class=\"date__day\">{$otherEvent['day']}</span>
                            <span class=\"date__month\">{$otherEvent['month']}</span>
                            <span class=\"date__year\">{$otherEvent['year']}</span>
                        </time>
                        <h3 class=\"b-term__title\">{$otherEvent['title']}</h3>
                    </div>
                </a>
            </li>";
            } ?>

        </ul>

        <div class="holder holder--lg">
            <p class="text-center mb0">
                <a href="../ofakulte/kalendar.php" class="btn btn--sm btn--secondary btn--outline">
                    <span class="btn__text">Všechny akce</span>
                </a>
            </p>
        </div>
    </div>
</main>

<?php
include '../footer.php';
?>
