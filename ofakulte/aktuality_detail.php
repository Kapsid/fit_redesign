<?php
include '../header.php';

$actuality = [
    "heading" => "Registrace na inženýrskou soutěž EBEC", "text" => "Na systému podlah pro provozy s vysokou zátěží pracuje tým, jehož součástí je i Vít Černý z Fakulty stavební VUT. Unikátní řešení pro sklady či továrny má vyplnit díru na trhu a nabídnout komplexní podlahový systém s vysokou mechanickou a chemickou odolností. Během příštího roku plánují výzkumníci testovat systém v reálných podmínkách. Pokud uspěje, mohl by se do roka dostat k běžným zákazníkům.
        Nosu nešťastná, divoké brání špičky vystřídalo šetrnost ty u věc ta druhy úřadu měla boží nízké odsouzeni s dochází nejznámějším
        signálem první. Péče i světu zprávy pohodlí tanec množství — vážil oteplování řadu avšak. Zesílilo prohlubování zastupujete
        čemu průmyslově něm tím k velkým ráj ohřívání přispěly internetová. Dvou chemical tu liška jednoho vyděšených úplně
        velice nepoužil z nejen z ty."
];

$otherActualities = [
    ["title" => "Registrace na inženýrskou soutěž EBEC", "day" => "4", "month" => "červen", "year" => "2012"],
    ["title" => "Zápisy ke studiu na ak. rok 2018/2019", "day" => "29", "month" => "září", "year" => "2015"],
    ["title" => "Brigáda pro studenty", "day" => "15", "month" => "červen", "year" => "2012"],
];
?>

<div class="b-detail">
    <div class="grid grid--bd grid--0">
        <div class="grid__cell size--t-12-12 size--12-12 holder holder--lg fz-lg">
            <div id="detail-01" class="b-detail__body pt50--d js-gallery" role="tabpanel">
                <h1><?php echo "{$actuality['heading']}";?></h1>
                <p>
                    <?php echo "{$actuality['text']}";?>
                </p>
                <p class="inline-btns">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=#url" target="_blank" class="btn btn--outline btn--sm btn--secondary btn--icon-l">
                    <span class="btn__text">
                        <span class="icon-svg icon-svg--facebook btn__icon">
                        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <use xlink:href="/img/bg/icons-svg.svg#icon-facebook" x="0" y="0" width="100%" height="100%"></use>
                        </svg>
                    </span>

                        Sdílet článek
                    </span>
                    </a>
                    <a href="https://twitter.com/intent/tweet" class="btn btn--outline btn--sm btn--twitter btn--icon-l">
                    <span class="btn__text">
                        <span class="icon-svg icon-svg--twitter btn__icon">
                        <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <use xlink:href="/img/bg/icons-svg.svg#icon-twitter" x="0" y="0" width="100%" height="100%"></use>
                        </svg>
                    </span>

                        Tweetnout
                    </span>
                    </a>
                </p>
                <div class="sg-box__item">
                    <div class="sg-box__item-code sg-box__item-code--bleed">
                        <div class="c-news holder holder--lg">
                            <h2 class="c-news__title h1">Další aktuality</h2>

                            <ul class="c-news__list grid grid--50">
                                <?php foreach($otherActualities as $otherActuality){
                                   echo "<li class=\"c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
                                    <article class=\"b-news\" role=\"article\">
                                        <a href=\"../ofakulte/aktuality_detail.php\" class=\"b-news__link\">
                                            <time class=\"b-news__date font-secondary\">{$otherActuality['day']}. {$otherActuality['month']} {$otherActuality['year']}</time>
                                            <h3 class=\"b-news__title\">{$otherActuality['title']}</h3>
                                        </a>
                                    </article>
                                </li>";
                                }?>
                            </ul>

                            <p class="c-news__btn mb0">
                                <a href="../ofakulte/aktuality.php" class="btn btn--sm btn--outline btn--secondary" style="margin-top: 25px;">
                                    <span class="btn__text">Více aktualit</span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<?php
include '../footer.php';
?>
