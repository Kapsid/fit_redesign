<?php
include '../header.php';

$actualities = [
    ["title" => "Registrace na inženýrskou soutěž EBEC", "date" => "22. červen 2018", "dateFormat" => "2018-06-22"],
    ["title" => "Registrace na inženýrskou soutěž EBEC", "date" => "22. červen 2018", "dateFormat" => "2018-06-22"],
    ["title" => "Zápisy ke studiu", "date" => "4. srpen 2018", "dateFormat" => "2018-08-04"],
    ["title" => "Registrace na inženýrskou soutěž EBEC", "date" => "22. červen 2018", "dateFormat" => "2018-06-22"],
    ["title" => "Registrace na inženýrskou soutěž EBEC", "date" => "22. červen 2018", "dateFormat" => "2018-06-22"],
    ["title" => "Zápisy ke studiu", "date" => "4. srpen 2018", "dateFormat" => "2018-08-04"],
    ["title" => "Registrace na inženýrskou soutěž EBEC", "date" => "22. červen 2018", "dateFormat" => "2018-06-22"],
    ["title" => "Zápisy ke studiu", "date" => "4. srpen 2018", "dateFormat" => "2018-08-04"],
];

$actualitiesBut = [
    [
      "day" => "11", "month" => "srpen", "year" => "2015", "image" => "b-article-lg--03.jpg", "title" => "Chytrá zařízení Easycon změří klima i energie. Budoucnost vidí v aluminiu" ,
        "text" => "Klima v místnosti či spotřebu domácnosti hlídá zařízení Easycon, které vymysleli Radek Sysel a Pavel Vejnar. Absolventi Fakulty elektrotechniky a komunikačních technologií VUT spojili znalosti ze studií a přišli s chytrým zařízením
        "
    ],
    [
    "day" => "11", "month" => "září", "year" => "2017", "image" => "b-article-lg--02.jpg",  "title" => "Robot z VUT umí najít člověka pod sutinami či lavinou" ,
    "text" => " Unikátní robot RUDA z laboratoří Fakulty informačních technologií VUT v Brně získal Zlatou medaili na letošním ročníku Mezinárodního strojírenského veletrhu. Zvítězil v kategorii pro automatizační, měřicí a řídicí techniku
    "
    ],
];
?>

<div class="c-news holder holder--lg">
    <h1 class="c-news__title">Aktuality</h1>

    <ul class="c-news__list grid grid--50">
        <?php foreach($actualities as $actuality){
            echo "<li class=\"c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
            <article class=\"b-news\" role=\"article\">
                <a href=\"../ofakulte/aktuality_detail.php\" class=\"b-news__link\">
                    <time class=\"b-news__date font-secondary\" datetime=\"{$actuality['dateFormat']}\">{$actuality['date']}</time>
                    <h2 class=\"b-news__title h3\">{$actuality['title']}</h2>
                </a >
            </article >
        </li>";
        }?>

    </ul>

    <nav class="pagination text-center" aria-label="Stránkování" role="navigation">
        <ul class="pagination__list">
            <li class="pagination__item">
                <a href="#" class="pagination__link" aria-current="page">1</a>
            </li>
            <li class="pagination__item">
                <a href="#" class="pagination__link">2</a>
            </li>
            <li class="pagination__item">
                <a href="#" class="pagination__link">3</a>
            </li>
            <li class="pagination__item">
                <a href="#" class="btn btn--secondary btn--wide btn--icon-only--m">
				<span class="btn__text">
					<span class="hide--m">Další</span>
					<span class="icon-svg icon-svg--angle-r btn__icon hide--t hide--d">
	<svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
	</svg>
</span>

				</span>
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="c-articles-lg ">
    <div class="holder holder--lg">
        <h2 class="c-articles-lg__title">Zprávy <a href="#">zVUT.cz</a></h2>
    </div>
    <div class="c-articles-lg__wrap border-b">
        <ul class="c-articles-lg__list grid grid--0">
            <?php foreach($actualitiesBut as $actualityBut){
             echo "<li class=\"c-articles-lg__item grid__cell size--t-6-12\">
                <article class=\"b-article-lg holder holder--lg\" role=\"article\">
                    <a href=\"../ofakulte/aktuality_detail.php\" class=\"b-article-lg__link\">
                        <div class=\"b-article-lg__img\">
                            <div class=\"b-article-lg__img-bg\" style=\"background-image: url('/img/illust/{$actualityBut['image']}');\"></div>
                        </div>
                        <div class=\"b-article-lg__head\">
                            <time class=\"b-article-lg__date date font-secondary\">
                                <span class=\"date__day\">{$actualityBut['day']}</span>
                                <span class=\"date__month\">{$actualityBut['month']}</span>
                                <span class=\"date__year\">{$actualityBut['year']}</span>
                            </time>
                            <h3 class=\"b-article-lg__title\">{$actualityBut['title']}</h3>
                        </div>
                        <div class=\"b-article-lg__content\">
                            <p>
                                {$actualityBut['text']}
                            </p>
                        </div>
                    </a>
                </article>
            </li>";
            }
            ?>

        </ul>
    </div>
    <div class="holder holder--lg pt40 pb40">
        <p class="text-center mb0">
            <a href="#" class="btn btn--sm btn--secondary btn--outline">
                <span class="btn__text">Více zpráv na zVUT.cz</span>
            </a>
        </p>
    </div>
</div>

<?php
include '../footer.php';
?>