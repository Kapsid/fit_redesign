<?php
include '../header.php';

// Array for base info about person
$baseInfo = [
        "name" => "Kateřina", "surname" => "Dokoupilová", "mainPosition" => "PROREKTOR PRO ROZVOJ VĚDECKÉ A VÝZKUMNÉ ČINNOSTI", "phone" => "+420 541 142 206", "email" => "pazderkova@fit.vutbr.cz", "number" => 21250
];

// Array for projects
$projects = [
    ["year" => 2014, "description" => "Moderní metody aplikované matematiky pro využití v technických vědách, zahájení: 01.01.2014, ukončení: 31.12.2016"],
    ["year" => 2013, "description" => "Moderní metody aplikované matematiky pro využití v technických vědách, zahájení: 01.01.2014, ukončení: 31.12.2016"],
    ["year" => 2012, "description" => "Moderní metody aplikované matematiky pro využití v technických vědách, zahájení: 01.01.2014, ukončení: 31.12.2016"],
];
// Array for publications
$publications = [
    ["year" => 2015, "cite" => "DOUPOVEC, M.; KUREŠ, M. Some geometric constructions on Frobenius Weil bundles. DIFFERENTIAL GEOMETRY AND ITS APPLICATIONS, 2014, roč. 35, č. S, s. 143-149. ISSN: 0926- 2245."],
    ["year" => 2013, "cite" => "DOUPOVEC, M.; MIKULSKI, W. On symmetrization of higher order jets. Miskolc Mathematical Notes, 2013, roč. 14, č. 2, s. 495-502. ISSN: 1787- 2405."],
    ["year" => 2010, "cite" => "DOUPOVEC, M.; KUREŠ, M. Some geometric constructions on Frobenius Weil bundles. DIFFERENTIAL GEOMETRY AND ITS APPLICATIONS, 2014, roč. 35, č. S, s. 143-149. ISSN: 0926- 2245."]
];

// Arrays from backend - fake data
$inputGarantSubjects = [
        ["name" => "Diferenciální geometrie", "shortName" => "SDG", "language" => "Česky", "faculty" => "FIT", "semester" => "letní", "institute" => "UPSI"],
        ["name" => "Matematika 1", "shortName" => "1MA-A", "language" => "Anglicky", "faculty" => "FIT", "semester" => "zimní",  "institute" => "UPSI"]
];


$inputTeachingSubjects = [
    ["name" => "Diferenciální geometrie", "shortName" => "SDG", "language" => "Česky", "faculty" => "FIT", "semester" => "letní", "institute" => "UPSI"],
    ["name" => "Matematika 1", "shortName" => "1MA-A", "language" => "Anglicky", "faculty" => "FIT", "semester" => "zimní",  "institute" => "UPSI"],
    ["name" => "Matematika 2", "shortName" => "2MA-A", "language" => "Anglicky", "faculty" => "FIT", "semester" => "letní",  "institute" => "UPSI"],
    ["name" => "Matematika 3", "shortName" => "3MA-A", "language" => "Anglicky", "faculty" => "FSI", "semester" => "zimní",  "institute" => "UPSI"]
];

$phdGrads = [
        ["name" => "Mgr. Irena Hinterleitner, Ph.D.", "year" => 2005, "thesisName" => "Některá speciální zobrazení a transformace Riemannových prostorů"],
        ["name" => "Mgr. Petr Vašík, Ph.D.", "year" => 2005, "thesisName" => "Konexe na hlavních prodlouženích vyššího řádu"],
];

$workCareer = [
    ["years" => "1984", "position" => "Tesla Brno, asistent"],
    ["years" => "1984-1986", "position" => "Šmeralovy závody Brno, analytik vývoje IS"],
    ["years" => "1986-dosud", "position" => "Fakulta strojního inženýrství VUT v Brně"],
    ["years" => "1986-1998", "position" => "asistent, od roku 1988 odborný asistent, VUT v Brně"],
    ["years" => "1998-2010", "position" => "docent, VUT v Brně"],
    ["years" => "od 05/2010", "position" => "profesor, VUT v Brně"],
];

$schoolCareer = [
    ["years" => "1979-1984", "position" => "studium matematické analýzy na Přírodovědecké fakultě Masarykovy university v Brně"],
    ["years" => "1984", "position" => "titul RNDr., PřF MU v Brně"],
    ["years" => "1988-1991", "position" => "externí vědecká aspirantura v Matematickém ústavu ČSAV"],
    ["years" => "1991", "position" => "titul CSc. v oboru geometrie a topologie, MFF UK Praha"],
    ["years" => "1998", "position" => "titul doc. pro obor matematika, VUT v Brně, habilitační práce Některé geometrické konstrukce v diferenciální geometrii"],
    ["years" => "2010", "position" => "titul prof. pro obor aplikovaná matematika, VUT v Brně"],
    ["years" => "2012", "position" => "dr. h. c., Státní technická univerzita v Iževsku, Rusko"],
    ["years" => "2017", "position" => "dr. h. c., Technická univerzita v Košicích, Slovensko"],
];

$schoolActivities = [
    "Bakalářské studium: Matematika, numerické metody","Magisterské studium, studijní obor Matematické inženýrství: Diferenciální geometrie, tensorový počet",
        "Vedení diplomových prací z diferenciální geometrie a jejich aplikací","Školitel doktorského studia z diferenciální geometrie"
];

$workFunctions = [
        ["organisation" => "Vysoké účení technické v Brně", "suborganization" => "Komise k oblasti vzdělávací činnosti", "function" => "předseda"],
        ["organisation" => "Vysoké účení technické v Brně", "suborganization" => "Disciplinární komise", "function" => "předseda"],
        ["organisation" => "Vysoké účení technické v Brně", "suborganization" => "Komise k oblasti vzdělávací činnosti", "function" => "předseda"],
        ["organisation" => "Vysoké účení technické v Brně", "suborganization" => "Disciplinární komise", "function" => "předseda"]
];

$contacts = [
        ["name" => "Centrum výpočetních a informačních služeb", "organisation" => "CVIS Oddělení vývoje", "position" => "vedoucí", "address" => "Antonínská 548/1, 601 90, Brno, Česká republika",
            "email" => "prorektor-vyzkum@ro.vutbr.cz", "phone" => "+420 541 145 449", "place" => "Místnost 100"],
        ["name" => "Rektorát", "organisation" => "Odbor studijních záležitostí", "position" => "vedoucí", "address" => "Antonínská 548/1, 601 90, Brno, Česká republika",
        "email" => "prorektor-vyzkum@ro.vutbr.cz", "phone" => "+420 541 145 449", "place" => "Místnost 114"],
];

// Adding to whole person structure
$personArray = [
        "baseInfo" => $baseInfo,
        "titlesBefore" => "Ing. arch.",
        "titlesAfter" => "Ph.D., MBA, LL.M.",
        "contacts" => $contacts,
        "projects" => $projects,
        "publications" => $publications,
        "garantSubjects" => $inputGarantSubjects,
        "teachingSubjects" => $inputTeachingSubjects,
        "phdGrads" => $phdGrads,
        "workCareer" => $workCareer,
        "schoolCareer" => $schoolCareer,
        "schoolActivities" => $schoolActivities,
        "workFunctions" => $workFunctions,
        "consultingHours" => "Konzultace na základě telefonické domluvy, telefon 603 151 427,středa, 7:00 - 8:00, U1 (Technická 2 - A5)",
        "activitiesText" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id
         ante dignissim, congue justo id, suscipit dolor. Integer feugiat quam eget tellus imperdiet efficitur. Fusce mollis aliquet rutrum. Morbi
          rutrum pulvinar libero, et sollicitudin mauris porta eget. Aenean vitae mi nec eros tempor posuere. Donec tempus leo nec arcu hendrerit dapibus. 
          Aliquam arcu nulla, rutrum sed nibh eu, ornare pellentesque lectus. Vivamus aliquam justo tellus, ac maximus urna dapibus id. Ut nec dolor viverra,
           dignissim felis quis, malesuada quam. Nulla eros arcu, volutpat nec metus vel, placerat ullamcorper enim. Aliquam convallis a nibh at pharetra.
            Curabitur vitae pellentesque diam, id lobortis elit. Quisque ultricies mauris placerat massa malesuada porta. In rutrum, mauris et viverra accumsan,
             lorem tellus porttitor nibh, id consectetur leo metus eget felis.",
        "productsText" => "Ut eget vehicula nunc. Ut leo mauris, egestas placerat risus a, interdum consectetur est. Fusce lorem risus, feugiat sed dictum sed, suscipit at
         leo. Etiam bibendum rutrum velit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ante orci, facilisis eu augue porta, lacinia consectetur risus. 
         In vel vulputate dolor, imperdiet lobortis dui. Etiam felis dui, finibus a quam ac, cursus dapibus lorem. Duis eget est ac magna faucibus aliquam sit amet a dui. 
         Sed consequat pretium purus eget luctus. Donec interdum dui id velit euismod tincidunt. Nullam consectetur pellentesque neque. Duis gravida mi vitae massa semper facilisis. 
         Aliquam mauris quam, lobortis a tortor vel, volutpat faucibus diam. Nunc ligula eros, rhoncus quis cursus ut, sollicitudin ac magna."
]


?>

	<main id="main" class="main" role="main" style="margin: 105px 0px 0px 0px;">
		<div class="holder holder--lg">
				<h1 class="text-center">Profil zaměstnance</h1>
				<div class="">
					<div class="sg-box__item-code sg-box__item-code--bleed">
						<div class="b-profile">
							<div class="b-profile__head">
								<div class="b-profile__info holder holder--lg">
									<div class="b-profile__title">
										<p class="title title--xs title--secondary-darken">
											<span class="title__item"><?php echo $personArray["titlesBefore"]?></span>
										</p>
										<h1 class="title title--secondary">
											<span class="title__item"><?php echo $personArray["baseInfo"]["name"]?></span>
											<span class="title__item"><?php echo $personArray["baseInfo"]["surname"]?></span>
										</h1>
										<p class="title title--xs title--secondary-darken">
											<span class="title__item"><?php echo $personArray["titlesAfter"]?></span>
										</p>
									</div>
									<p class="b-profile__position font-secondary"><?php echo $personArray["baseInfo"]["mainPosition"]?></p>
									<p class="b-profile__contact">
										<a href="tel:<?php echo $personArray["baseInfo"]["phone"]?>"><?php echo $personArray["baseInfo"]["phone"]?></a><br>
										<a href="mailto:<?php echo $personArray["baseInfo"]["email"]?>"><?php echo $personArray["baseInfo"]["email"]?></a><br>
										<span class="b-profile__id"><?php echo $personArray["baseInfo"]["number"]?><span class="color-gray">/osobní číslo VUT</span></span>
									</p>
									<p>
										<a href="#" class="btn btn--sm btn--outline btn--white">
											<span class="btn__text">Odeslat VUT zprávu</span>
										</a>
									</p>
								</div>

								<div class="b-profile__img holder holder--lg b-profile__img--inner" style="background-image: url('../img/illust/b-profile--02.jpg');">
									<img src="../img/illust/b-profile__img-inner.jpg" width="354" height="452" alt="">
								</div>
							</div>

							<div class="b-profile__nav">
								<nav class="m-main m-main--sub m-main--bd" aria-label="Submenu" role="navigation">
									<div class="m-main__wrap">
										<ul class="m-main__list profile-tablist" role="tablist">
											<li class="m-main__item">
												<a href="#kontakty" class="m-main__link" role="tab" aria-controls="kontakty"  aria-selected="true">Kontakty</a>
											</li>
											<li class="m-main__item">
												<a href="#funkce" class="m-main__link" role="tab" aria-controls="funkce" >Funkce</a>
											</li>
											<li class="m-main__item">
												<a href="#zivotopis" class="m-main__link" role="tab" aria-controls="zivotopis" >Životopis</a>
											</li>
											<li class="m-main__item">
												<a href="#vyuka" class="m-main__link" role="tab" aria-controls="vyuka" >Výuka</a>
											</li>
											<li class="m-main__item">
												<a href="#projekty" class="m-main__link" role="tab" aria-controls="projekty" >Projekty</a>
											</li>
											<li class="m-main__item">
												<a href="#publikace" class="m-main__link" role="tab" aria-controls="publikace" >Publikace</a>
											</li>
											<li class="m-main__item">
												<a href="#tvurci-aktivity" class="m-main__link" role="tab" aria-controls="tvurci-aktivity" >Tvůrčí aktivity</a>
											</li>
											<li class="m-main__item">
												<a href="#produkty" class="m-main__link" role="tab" aria-controls="produkty" >Produkty</a>
											</li>
										</ul>
									</div>
								</nav>
							</div>

							<div class="b-profile__body">
								<div id="kontakty" role="tabpanel">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Kontakty</h2>
											<table class="table-meta">
												<tbody>
												<tr>
													<th>Osobní číslo VUT</th>
													<td><?php echo $personArray["baseInfo"]["number"]?></td>
												</tr>
												</tbody>
											</table>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
                                            <?php foreach ($personArray["contacts"] as $contacts){
											echo "<h3>{$contacts["name"]}</h3>
											<p class=\"profile-large font-secondary\">{$contacts["organisation"]}<span class=\"color-gray\">, {$contacts["position"]}</span></p>
											<table class=\"table-blank\">
												<colgroup>
													<col style=\"width: 15%;\">
													<col style=\"width: 85%;\">
												</colgroup>
												<tbody>
												<tr>
													<th>Adresa</th>
													<td>{$contacts["address"]}</td>
												</tr>
												<tr>
													<th>E-mail</th>
													<td><a href=\"mailto:prorektor-vyzkum@ro.vutbr.cz\">{$contacts["email"]}</a></td>
												</tr>
												<tr>
													<th>Telefon</th>
													<td>{$contacts["phone"]}</td>
												</tr>
												<tr>
													<th>Místnost</th>
													<td>{$contacts["place"]}</td>
												</tr>
												</tbody>
											</table>";
                                            }?>
										</div>
									</div>
								</div>

								<div id="funkce" role="tabpanel" aria-hidden="true">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Funkce</h2>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
                                            <?php foreach ($personArray['workFunctions'] as $function){
											echo "<h3>{$function['organisation']}</h3>";
											echo "<p>
												<a href=\"#\" class=\"link-ext color-secondary font-bold\">{$function['suborganization']}</a> - {$function['function']}
											</p>"; }?>
										</div>
									</div>
								</div>

								<div id="zivotopis" role="tabpanel" aria-hidden="true">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Životopis</h2>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
											<h3>Vzdělání a akademická kvalifikace</h3>
											<ul class="list-timeline">
                                                <?php foreach ($personArray['schoolCareer'] as $schoolCareer){
												echo "<li class=\"list-timeline__item\">
													<p class=\"list-timeline__date font-secondary\">{$schoolCareer['years']}</p>
													<p>
														{$schoolCareer['position']}
													</p>
												</li>";}?>
											</ul>

											<h3>Přehled zaměstnání</h3>
											<ul class="list-timeline">
                                                <?php foreach ($personArray['workCareer'] as $workCareer){
                                                    echo "<li class=\"list-timeline__item\">
													<p class=\"list-timeline__date font-secondary\">{$workCareer['years']}</p>
													<p>
														{$workCareer['position']}
													</p>
												</li>";}?>
											</ul>

											<h3>Pedagogická činnost</h3>
											<ul class="list-timeline">
                                                <?php foreach ($personArray['schoolActivities'] as $schoolActivity){
												echo "<li class=\"list-timeline__item\">
													<p>
														{$schoolActivity}
													</p>
												</li>";}?>
											</ul>
											<p class="profile-small">
												Pokud je v údajích nesrovnalost, podívejte se do <a href="#" class="color-secondary">častých otázek k vizitkám</a>.
											</p>
										</div>
									</div>
								</div>

								<div id="vyuka" role="tabpanel" aria-hidden="true">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Výuka</h2>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
											<h3>Konzultační hodiny</h3>
											<p>
												<?php $personArray['consultingHours'] ?>
											</p>

											<h3>Garantované předměty</h3>
											<table class="table-blank">
												<colgroup>
													<col style="width: 5%;">
													<col style="width: 95%;">
												</colgroup>
												<tbody>
                                                <?php
                                                    foreach ($personArray["garantSubjects"] as $garantSubject){
                                                        echo "<tr><td>{$garantSubject["shortName"]}</td><td>
                                                        <a href=\"#\" class=\"link-ext color-secondary font-bold\">{$garantSubject["name"]}</a>
                                                        <br>{$garantSubject["language"]}, {$garantSubject["semester"]}, 
                                                        <a href=\"#\" class=\"color-secondary\">{$garantSubject["faculty"]}</a>, <a href=\"#\" class=\"color-secondary\">{$garantSubject["institute"]}</a>
													    </td></tr>";
                                                    }
                                                ?>
												</tbody>
											</table>
											<p class="font-italic profile-small">
												* Údaje platné pro aktuální akademický rok 2017/2018
											</p>

											<h3>Vyučované předměty</h3>
											<table class="table-blank">
												<colgroup>
													<col style="width: 5%;">
													<col style="width: 95%;">
												</colgroup>
												<tbody>
                                                <?php
                                                foreach ($personArray["teachingSubjects"] as $teachingSubject){
                                                    echo "<tr><td>{$teachingSubject["shortName"]}</td><td>
                                                    <strong><a href=\"#\" class=\"link-ext color-secondary\">{$teachingSubject["name"]}</a></strong>
                                                    <br>
                                                    {$teachingSubject["language"]}, {$teachingSubject["semester"]}, 
                                                    <a href=\"#\" class=\"color-secondary\">{$teachingSubject["faculty"]}</a>, <a href=\"#\" class=\"color-secondary\">{$teachingSubject["institute"]}</a>
                                                    </td></tr>";
                                                }
                                                ?>
												</tbody>
											</table>
											<p class="font-italic profile-small">
												* Údaje platné pro aktuální akademický rok 2017/2018
											</p>

											<h3>Absolventi doktorského studia</h3>
                                            <?php foreach($personArray['phdGrads'] as $phdGrad){
                                              echo "<p>
												<strong><a href=\"#\" class=\"link-ext color-secondary\">{$phdGrad['name']}</a></strong> ({$phdGrad['year']})<br>
												téma dizertační práce: {$phdGrad['thesisName']}
											</p>";}?>

										</div>
									</div>
								</div>

								<div id="projekty" role="tabpanel" aria-hidden="true">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Projekty</h2>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
											<ul class="list-timeline">
                                                <?php foreach($personArray['projects'] as $project){
												echo"<li class=\"list-timeline__item\">
													<p class=\"list-timeline__date font-secondary\">{$project["year"]}</p>
													<p>
														{$project['description']}<br> <a href=\"../vedavyzkum/projekty_detail.php\" class=\"color-secondary font-bold\">Detail</a>
													</p>
												</li>";}?>
											</ul>

											<p class="profile-small">
												Pokud je v údajích nesrovnalost, podívejte se do <a href="#" class="color-secondary">častých otázek k vizitkám</a>.
											</p>
										</div>
									</div>
								</div>

								<div id="publikace" role="tabpanel" aria-hidden="true">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Publikace</h2>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
											<ul class="list-timeline">
                                                <?php foreach($personArray['publications'] as $publication){
												echo "<li class=\"list-timeline__item\">
													<p class=\"list-timeline__date font-secondary\">{$publication['year']}</p>
													<p>
														{$publication['cite']}<br> <a href=\"#\" class=\"color-secondary font-bold\">Detail</a>
													</p>
												</li>";
												}?>
											</ul>

											<p class="profile-small">
												Pokud je v údajích nesrovnalost, podívejte se do <a href="#" class="color-secondary">častých otázek k vizitkám</a>.
											</p>
										</div>
									</div>
								</div>

								<div id="tvurci-aktivity" role="tabpanel" aria-hidden="true">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Tvůrčí aktivity</h2>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
											<p>
                                                <?php echo $personArray['activitiesText'] ?>
											</p>
										</div>
									</div>
								</div>

								<div id="produkty" role="tabpanel" aria-hidden="true">
									<div class="grid grid--bd">
										<div class="grid__cell size--t-5-12 size--4-12 holder holder--lg b-profile__content">
											<h2 class="h1 text-uppercase">Produkty</h2>
										</div>
										<div class="grid__cell size--t-7-12 size--8-12 holder holder--lg b-profile__content">
											<p>
                                                <?php echo $personArray['productsText'] ?>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
	</main>

<?php
include '../footer.php'
?>