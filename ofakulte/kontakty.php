<?php
include '../header.php';
?>

<div class="b-intro border-b holder holder--lg">

</div>

<div class="b-gmap">
    <div class="grid grid--0">
        <div class="grid__cell size--t-7-12 size--9-12">
            <div class="b-gmap__holder" data-address="Božetěchova 1/2, 612 00 Brno-Královo Pole"></div>
        </div>

        <div class="grid__cell size--t-5-12 size--3-12">
            <div class="b-contact b-contact--map b-contact--primary holder holder--md">
                <h2 class="b-contact__title">Kde nás najdete</h2>
                <div class="b-contact__text">
                    <p>
                        <strong>Fakulta informačních technologií VUT <br />
                            Božetěchova 1/2, 612 00 Brno-Královo Pole</strong>
                    </p>
                </div>
            </div>

            <div class="b-contact b-contact--map holder holder--md">
                <h2 class="b-contact__title">Kontaktujte nás</h2>
                <div class="b-contact__text">
                    <p>
                        <a href="tel:+42054114144" class="link-icon">
						<span class="icon-svg icon-svg--phone link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-phone" x="0" y="0" width="100%" height="100%"></use>
                            </svg>
                        </span>

                            +420 541 14 144
                        </a><br>
                        <a href="mailto:info@fit.vutbr.cz" class="link-icon">
						<span class="icon-svg icon-svg--message link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-message" x="0" y="0" width="100%" height="100%"></use>
                            </svg>
                        </span>

                            info@fit.vutbr.cz
                        </a><br>
                        <a href="#" class="link-icon">
						<span class="icon-svg icon-svg--link link-icon__icon">
                            <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <use xlink:href="/img/bg/icons-svg.svg#icon-link" x="0" y="0" width="100%" height="100%"></use>
                            </svg>
                        </span>

                            www.vutbr.cz
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include '../footer.php';
?>