<?php
include '../header.php';

$awards = [
    ['name' => 'Ocenění 1', 'year' => 2015, 'author' => 'Jiří Novák, Ing.', 'text' => 'text o ocenění'],
    ['name' => 'Ocenění 2', 'year' => 2013, 'author' => 'Adam Pstruh, Ing.', 'text' => 'text o ocenění'],
    ['name' => 'Ocenění 3', 'year' => 2014, 'author' => 'Petr Pavel, Ing.', 'text' => 'text o ocenění'],
    ['name' => 'Ocenění 4', 'year' => 2016, 'author' => 'Jana Adamová, Ing.', 'text' => 'text o ocenění'],
]
?>

    <main id="main" class="main" role="main">
        <div class="b-intro border-b holder holder--lg">
            <h1 class="b-intro__title first-title">Ocenění</h1>

            <form action="?" class="f-subjects">
                <div class="f-subjects__filter">
                    <p class="inp inp--multiple">
                        <span class="inp__fix minw160--t minw180">
                            <label for="year" class="inp__label inp__label--inside">Rok</label>
                            <select name="year" id="year" class="select js-select">
                                <option selected disabled placeholder>Rok</option>
                                <option>2018</option>
                                <option>2017</option>
                                <option>2016</option>
                                <option>2015</option>
                                <option>2014</option>
                            </select>
                        </span>
                    </p>
                </div>

                <div class="f-subjects__search">
                    <p class="inp inp--group mb0">
							<span class="inp__fix">
								<label for="f-subjects__search" class="inp__label inp__label--inside">Název ocenění</label>
								<input type="text" class="inp__text" id="f-subjects__search" placeholder="Název, autor, vedoucí, abstrakt&hellip;">
							</span>
                        <span class="inp__btn">
                            <button class="btn btn--secondary btn--block--m" type="submit">
                                <span class="btn__text">Hledat</span>
                            </button>
                        </span>
                    </p>
                </div>
            </form>
        </div>

        <div class="c-subjects holder holder--lg">
            <nav class="pagination " aria-label="Stránkování" role="navigation">
                <ul class="pagination__list">
                    <li class="pagination__item">
                        <a href="#" class="pagination__link" aria-current="page">2018</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2017</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2016</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2015</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2014</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2013</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2012</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2011</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2010</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2009</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2008</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2007</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2006</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2005</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2004</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2003</a>
                    </li>
                    <li class="pagination__item">
                        <a href="#" class="pagination__link">2002</a>
                    </li>
                </ul>
            </nav>
        </div>

        <div class="c-events">
            <ul class="c-events__list grid grid--bd">
                <?php foreach($awards as $award){
                    echo "<li class=\"c-events__item grid__cell grid__cell--grow border-b holder
							size--4-12 c-events__item--sm holder--md
					\">
                <a href=\"../ofakulte/akce_detail.php\" class=\"b-term
								b-term--sm
								b-term--img
						c-events__term\">
                    <div class=\"b-term__img\">
                    </div>
                    <div class=\"b-term__wrap\">
                        <time class=\"b-term__date date font-secondary\" style='width: 100%;'>
                            <span class=\"date__year\">{$award['year']}</span>
                            <span class=\"date__day\" style='font-size:24px;'>{$award['author']}</span>
                        </time>
                        <h2 class=\"b-term__title h3\">{$award['name']}</h2>
                    </div>
                </a>
            </li>";
                }?>

            </ul>

        </div>
    </main>
<?php
include '../footer.php'
?>