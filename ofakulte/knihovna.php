<?php
include '../header.php';

$news = [
    ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 1"],
    ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 2"],
    ["day" => 22, "month" => "červen", "year" => 2018, "title" => "Název aktuality 3"],
]
?>

<div class="b-hero-header b-hero-header--nav">
    <div class="b-hero-header__img" style="background-image: url('/img/illust/b-hero-header--02.jpg');"></div>
    <div class="b-hero-header__content b-hero-header__content--bottom holder holder--lg text-left">
        <p class="mb0">
        </p>
        <h1 class="title title--secondary b-hero-header__title">
            <span class="title__item">Knihovna</span>
        </h1>

        <ul class="m-main__list profile-tablist" role="tablist">
            <li class="m-main__item">
                <a href="#oknihovne" class="m-main__link" role="tab" aria-controls="oknihovne"  aria-selected="true">O knihovně</a>
            </li>
            <li class="m-main__item">
                <a href="#sluzby" class="m-main__link" role="tab" aria-controls="sluzby" >Služby knihovny</a>
            </li>
            <li class="m-main__item">
                <a href="#katalog" class="m-main__link" role="tab" aria-controls="katalog" >Online katalog</a>
            </li>
            <li class="m-main__item">
                <a href="#navody" class="m-main__link" role="tab" aria-controls="navody" >Návody a tipy</a>
            </li>
            <li class="m-main__item">
                <a href="#kontakty" class="m-main__link" role="tab" aria-controls="kontakty" >Kontakty</a>
            </li>
        </ul>
    </div>

</div>

<div id="oknihovne" role="tabpanel">
    <div class="b-detail">
        <div class="grid grid--0">

            <div class="grid__cell size--t-12-12">
                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                    <div class="c-articles-lg ">
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Aktuálně z knihovny</h2>

                            <ul class="c-news__list grid grid--50">
                                <?php foreach($news as $oneNews){
                                    echo "<li class=\"c-news__item grid__cell grid__cell--grow size--t-6-12 size--4-12\">
                                    <article class=\"b-news\" role=\"article\">
                                        <a href=\"#\" class=\"b-news__link\">
                                            <time class=\"b-news__date font-secondary\">{$oneNews['day']}. {$oneNews['month']} {$oneNews['year']}</time>
                                            <h2 class=\"b-news__title h3\">{$oneNews['title']}</h2>
                                        </a>
                                    </article>
                                </li>";
                                }?>

                            </ul>

                            <nav class="pagination text-center" aria-label="Stránkování" role="navigation">
                                <ul class="pagination__list">
                                    <li class="pagination__item">
                                        <a href="#" class="pagination__link" aria-current="page">1</a>
                                    </li>
                                    <li class="pagination__item">
                                        <a href="#" class="pagination__link">2</a>
                                    </li>
                                    <li class="pagination__item">
                                        <a href="#" class="pagination__link">3</a>
                                    </li>
                                    <li class="pagination__item">
                                        <a href="#" class="btn btn--secondary btn--wide btn--icon-only--m">
                                <span class="btn__text">
                                    <span class="hide--m">Další</span>
                                    <span class="icon-svg icon-svg--angle-r btn__icon hide--t hide--d">
                                    <svg class="icon-svg__svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <use xlink:href="/img/bg/icons-svg.svg#icon-angle-r" x="0" y="0" width="100%" height="100%"></use>
                                    </svg>
                                </span>

                                </span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="holder holder--lg border-b pt40">
                                <h2 class="c-highlights__title">Zajímavá čísla</h2>
                            </div>

                            <ul class="c-highlights__list grid grid--bd grid--0">
                                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                                    <div class="b-highlight holder">
                                        <p class="b-highlight__title font-secondary">20000</p>
                                        <div class="b-highlight__text">
                                            <p>knihovních jednotek</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12">
                                    <div class="b-highlight holder">
                                        <p class="b-highlight__title font-secondary">100</p>
                                        <div class="b-highlight__text">
                                            <p>studijních míst</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                                    <div class="b-highlight holder">
                                        <p class="b-highlight__title font-secondary">2000</p>
                                        <div class="b-highlight__text">
                                            <p>aktivních uživatelů</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="c-highlights__item grid__cell size--t-6-12 size--3-12 border-t--t">
                                    <div class="b-highlight holder">
                                        <p class="b-highlight__title font-secondary">688</p>
                                        <div class="b-highlight__text">
                                            <p> metrů čtverečních prostor v nejstarší části kláštera</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                            <h2 class="c-articles-lg__title">Užitečné odkazy</h2>
                            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/dokumenty/Provozni_rad_Knihovna_2017doPDF.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Provozní řád knihovny FIT </h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="https://www.vutbr.cz/login?fdkey=WcXVf8he4K" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Knihovní řád VUT</h2>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="sluzby" role="tabpanel">
    <div class="b-detail">
        <div class="grid grid--0">

            <div class="grid__cell size--t-12-12">
                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                    <div class="c-articles-lg ">
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Služby knihovny</h2>

                            <ul class="c-news__list grid grid--50">
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Prezenční výpůjčky</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                    <ul class="library-ul">
                                                        <li>využívání informačních materiálů pouze v prostorách knihovny</li>
                                                        <li>vybrané dokumenty, označené žlutým kolečkem, kvalifikační práce</li>
                                                        <li>některé dokumenty určené k prezenčnímu studiu je možné si po domluvě půjčit přes noc/přes víkend</li>
                                                        <li>za pozdní vrácení prezenčních knih účtujeme 25,- Kč/knihovní jednotku/den</li>
                                                        <li>starší kvalifikační práce a knihy ze skladu vám připravíme k vypůjčení do druhého dne od zadání požadavku(mailem či osobně v knihovně)</li>
                                                        <li>CD k diplomovým a bakalářským pracím půjčujeme studentům pouze na vyžádání od akademických pracovníků a doktorandů</li>
                                                    </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Absenční výpůjčky</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>zapůjčení knih a informačních materiálů mimo knihovnu</li>
                                                    <li>výpůjční doba knih - 4 týdny</li>
                                                    <li> výpůjční doba skript - 6 týdnů</li>
                                                    <li>výpůjční doba FITkitů - 5 měsíců</li>
                                                    <li>výpůjční doba časopisů - 1 týden (aktuální číslo v knihovně zůstává, zapůjčit si můžete časopis označený zeleným proužkem)</li>
                                                    <li>možnost prodloužení maximálně 2 x, pokud není dokument rezervován jiným čtenářem, časopisy rezervovat ani prodlužovat nelze</li>
                                                    <li>za pozdní vrácení absenčních knih účtujeme 2 Kč/knihovní jednotku/den</li>
                                                    <li>maximální celkový počet výpůjček, které může student mít, je třicet</li>

                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Meziknihovní a mezinárodní výpůjčky (MVS a MMVS)</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>pouze pro vyučující, doktorandy a diplomanty (se souhlasem vedoucího práce)</li>
                                                    <li>pokud není kniha dostupná ve fondu knihovny FIT VUT ani v jiné brněnské knihovně</li>
                                                    <li>MVS se provádí v rámci celé ČR</li>
                                                    <li>MMVS objednáváme ze zahraničí prostřednictvím Virtuální polytechnické knihovny</li>
                                                    <li>knihovny mohou žádat o MVS prostřednictvím emailu: knihovna@fit.vutbr.cz</li>

                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>

                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Nákup knih</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>Pro akademické pracovníky, doktorandy a zaměstnance.</li>
                                                    <li>Otevřete si nové okno v prohlížeči Internet Explorer, Google Chrome, Mozilla Firefox (dle vašich vlastních preferencí).</li>
                                                    <li>Požadavky na nákup se provádějí na stránkách Portálu VUT (je třeba se přihlásit pomocí svého loginu a hesla) --> klikněte na Intraporál --> Nákup --> Nový košík .</li>
                                                    <li>Pro přijetí košíku je nutné uvést jméno osoby, která košík založila, pro koho jsou knihy určeny (Vlastník/Knihovna) a kdo hradí nákup (Vlastník/Knihovna).</li>
                                                    <li>Dále je nutné mít vyplněny v tabulce CO objekty položky "Zakázka" a "Nákladové středisko" NEBO "Zakázka" a "SPP prvek".</li>
                                                    <li>Následně klikněte na "Vložit záznam" pro vytvoření požadavku na knihu. Tento krok opakujte tolikrát, dokud nebudou v tabulce uvedeny všechny Vámi objednávané knihy.</li>
                                                    <li>Pro nákup jsou nejdůležitější údaje o názvu knihy a ISBN (uvádějte pokud možno bez pomlček).</li>
                                                    <li>Pokud budete mít jakékoliv dotazy ohledně nákupu, kontaktujte některou z knihovnic.</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Elektronické služby</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>přístup k elektronickým informačním zdrojům, (návod na připojení k databázím naleznete zde)</li>
                                                    <li>elektronická komunikace s uživateli</li>
                                                    <li>Zeptejte se knihovny on-line - prostřednictvím této služby se můžete zeptat
                                                        na informace související s knihovnou, na faktografické a bibliografické údaje o knihách atd.
                                                        Na Vaše dotazy odpovíme co nejrychleji. V případě velmi složitých dotazů je budeme konzultovat s ostatními knihovnami.</li>

                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Reprografické služby</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>černobílé a barevné kopírování a tisk, plotter, kroužková vazba, termovazba a laminace A4 (tyto služby jsou zpoplatněny, ceny se řídí dle ceníku schváleného děkanem Fakulty informačních technologií)</li>
                                                    <li>skenování studijních materiálů na přístroji Xerox WorkCentre 7335 - tato služba je zdarma</li>
                                                    <li>Prostudujte si, prosím, informace o tisku v knihovně zde.</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Seminární místnosti</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>pro vaši týmovou práci můžete v knihovně využívat dvě seminární místnosti.</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>

                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Doplňkové služby</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>K dispozici je vám řezačka, sešívačka, nůžky, lepidlo a další. Stačí se ozvat. :)</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Informační výchova</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>Pro studenty prvního ročníku organizujeme akreditovaný předmět IVIG (Informační vzdělávání - Informační gramotnost). Předmět poskytuje studentům
                                                    základní informace pro rozvoj informační gramotnosti. Výuka je zaměřena především na praktické využívání informačních zdrojů, institucí a služeb
                                                    jimi poskytovanými. Účelem je naučit studenta pracovat s dostupnými elektronickými informačními zdroji s možností jejich praktického využití při studiu či odborné práci.</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>

                                <li class="c-news__item grid__cell grid__cell--grow size--t-6-12 size--6-12">
                                    <article class="b-news" role="article">
                                        <a href="#" class="b-news__link">
                                            <h3 class="b-news__title">Referenční služby</h3>
                                            <div class="b-news__perex" style="max-width: 100% !important;">
                                                <p>
                                                <ul class="library-ul">
                                                    <li>poskytování informací o fondu knihovny FIT</li>
                                                    <li>poskytování informací o FIT a ostatních fakultách VUT</li>

                                                </ul>
                                                </p>
                                            </div>
                                        </a>
                                    </article>
                                </li>
                            </ul>
                            <h3>Ceník služeb - viz <a href="http://www.fit.vutbr.cz/lib/dokumenty/cenik_sluzeb.pdf">http://www.fit.vutbr.cz/lib/dokumenty/cenik_sluzeb.pdf</a></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="katalog" role="tabpanel">
    <div class="b-detail">
        <div class="grid grid--0">

            <div class="grid__cell size--t-12-12">
                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                    <div class="c-articles-lg ">
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Online katalog</h2>
                            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/online_kat.php.cs " class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Primo </h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/online_kat.php.cs " class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Aleph</h2>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="navody" role="tabpanel">
    <div class="b-detail">
        <div class="grid grid--0">

            <div class="grid__cell size--t-12-12">
                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                    <div class="c-articles-lg ">
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Návody</h2>
                            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/dokumenty/letak_kfit_zakladni_inf.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Základní informace o knihovně FIT VUT</h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/dokumenty/letak_kfit_aleph.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Návod pro práci s online katalogem</h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/dokumenty/letak_kfit_kniha.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Návod jak najít požadovanou knihu v knihovně FIT</h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/dokumenty/letak_kFIT_Primo.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Návod k vyhledávači Primo</h2>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Normy</h2>
                            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="https://cas.fit.vutbr.cz/?cosign-WebFIT&https://www.fit.vutbr.cz/study/courses/knihovna/private/78349.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">ČSN 01 6910 Úprava písemností zpracovaných textovými editory</h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="https://cas.fit.vutbr.cz/?cosign-WebFIT&https://www.fit.vutbr.cz/study/courses/knihovna/private/21060.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">ČSN ISO 7144 Formální úprava disertací a podobných dokumentů</h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="http://www.fit.vutbr.cz/lib/dokumenty/licencni_podminky" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Licenční podmínky k využívání norem</h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="https://www.citace.com/download/CSN-ISO-690.pdf" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Interpretace normy ČSN ISO 690 Bibliografické odkazy a citace dokumentů</h2>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Odkazy</h2>
                            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="https://www.vutbr.cz/knihovny" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Portál knihoven VUT Brno</h2>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Generátory bibiliografických citací</h2>
                            <ul class="c-faculties__list grid grid--80 grid--t-40 js-macy">
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="https://www.citace.com" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Citace.com</h2>
                                    </a>
                                </li>
                                <li class="c-faculties__item grid__cell size--t-6-12 size--4-12">
                                    <a href="https://citace.lib.vutbr.cz" class="b-faculty b-faculty--shadow">
                                        <h2 class="b-faculty__title h3">Citace PRO</h2>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="kontakty" role="tabpanel">
    <div class="b-detail">
        <div class="grid grid--0">

            <div class="grid__cell size--t-12-12">
                <div class="b-detail__content holder holder--lg pt0--m pb40--m pt40--t pb40--t pt0 pb60 mb0">

                    <div class="c-articles-lg ">
                        <div class="holder holder--lg">
                            <h2 class="c-articles-lg__title">Na Vaší návštěvu se těší:</h2>
                            <div class="b-contact b-contact--map b-contact--primary holder holder--md">
                                <h2 class="b-contact__title">Knihovna Fakulty informačních technologií</h2>
                                <div class="b-contact__text">
                                    <p class="library-links">
                                        www: <a href="http://www.fit.vutbr.cz/lib/">http://www.fit.vutbr.cz/lib/ </a><br />
                                        e-mail: <a href="mailto:knihovna@fit.vutbr.cz ">knihovna@fit.vutbr.cz </a><br />
                                        tel: <a href="tel:+420 54114-1204">+420 54114-1204</a><br />
                                        tel: <a href="tel:+420 54114-1204">+420 54114-1204</a><br />
                                        <a href="https://www.facebook.com/knihfit?ref=tn_tnmn">Facebook</a><br />
                                    </p>
                                </div>
                            </div>
                            <div class="c-employees">
                                <div class="holder holder--lg border-t">
                                    <div class="c-employees__wrap">
                                        <ul class="c-employees__list grid grid--60">
                                            <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                                <a href="../ofakulte/profil.php" class="b-employee">
                                                    <div class="b-employee__wrap">
                                                        <div class="b-employee__img">
                                                            <img src="/img/illust/b-employee--01.jpg" width="100" height="100" alt="">
                                                        </div>
                                                        <h3 class="b-employee__name">Anežka Mikésková, DiS.</h3>
                                                        <div class="b-employee__footer">
                                                            <p class="b-employee__position font-secondary">knihovnice</p><br />
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="c-employees__item grid__cell grid__cell--grow size--t-6-12 size--4-12">
                                                <a href="../ofakulte/profil.php" class="b-employee">
                                                    <div class="b-employee__wrap">
                                                        <div class="b-employee__img">
                                                            <img src="/img/illust/b-employee--01.jpg" width="100" height="100" alt="">
                                                        </div>
                                                        <h3 class="b-employee__name">Bc. Petra Obrusníková</h3>
                                                        <div class="b-employee__footer">
                                                            <p class="b-employee__position font-secondary">knihovnice</p><br />

                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="b-contact b-contact--map holder holder--md">
                                    <h2 class="b-contact__title">Provozní doba knihovny</h2>
                                    <div class="b-contact__text">
                                        <p>
                                            <strong>Pondělí, Úterý:</strong> 8:00 - 16:00 (16-20 platí režim Studovna!)<br />
                                            <strong>Středa, Čtvrtek:</strong> 8:00 - 16:00 (16-19 platí režim Studovna!)<br />
                                            <strong>Pátek:</strong> 8:00 - 15:00<br /><br />
                                            <strong>Každý první pátek v měsíci je knihovna z důvodu údržby knihovního fondu zavřená.</strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include '../footer.php';
?>